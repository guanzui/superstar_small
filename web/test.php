﻿<?php
    $host = "https://api.trackingmore.com/v2";
    $path = "/trackings";
    $method = "GET";
    $headers = array(
		'Content-Type: application/json',
		'Trackingmore-Api-Key:298cb48d-8753-491e-a7b6-7878111976d0'
	);
	
	$url = $host . $path . '/4px/1Z5922000299704232';
	
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_FAILONERROR, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    //curl_setopt($curl, CURLOPT_HEADER, true); 如不输出json, 请打开这行代码，打印调试头部状态码。
    //状态码: 200 正常；400 URL无效；401 appCode错误； 403 次数用完； 500 API网管错误
    if (1 == strpos("$".$host, "https://"))
    {
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    }
    $data = json_decode(curl_exec($curl),true);
	print_r( $data );
?>