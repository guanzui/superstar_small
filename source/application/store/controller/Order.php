<?php

namespace app\store\controller;

use app\store\model\Order as OrderModel;
use app\common\model\GetTui as GetTuiModel;

/**
 * 订单管理
 * Class Order
 * @package app\store\controller
 */
class Order extends Controller
{
    /**
     * 待发货订单列表
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function delivery_list()
    {
        return $this->getList('待发货订单列表', [
            'pay_status' => 20,
            'delivery_status' => 10
        ]);
    }

    /**
     * 待收货订单列表
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function receipt_list()
    {
        return $this->getList('待收货订单列表', [
            'pay_status' => 20,
            'delivery_status' => 20,
            'receipt_status' => 10
        ]);
    }

    /**
     * 待付款订单列表
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function pay_list()
    {
        return $this->getList('待付款订单列表', ['pay_status' => 10, 'order_status' => 10]);
    }

    /**
     * 已完成订单列表
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function complete_list()
    {
        return $this->getList('已完成订单列表', ['order_status' => 30]);
    }

    /**
     * 已取消订单列表
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function cancel_list()
    {
        return $this->getList('已取消订单列表', ['order_status' => 20]);
    }

    /**
     * 全部订单列表
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function all_list()
    {
        return $this->getList('全部订单列表');
    }

    /**
     * 订单列表
     * @param $title
     * @param $filter
     * @return mixed
     * @throws \think\exception\DbException
     */
    private function getList($title, $filter = [])
    {
        $model = new OrderModel;
		$order_no = $this->request->param("order_no",'');
		if( $order_no ) {
			$filter['order_no'] = $order_no;
		}
		$type = $this->request->param("type/d",0);
		if( $type > 0 ){
			$filter['type'] = $type;
		}
		$pay_type = $this->request->param("pay_type/d",0);
		if( $pay_type > 0 ) {
			$filter['pay_type'] = $pay_type;
		}
		
        $list = $model->getList($filter);
        return $this->fetch('index', compact('title','list'));
    }

    /**
     * 订单详情
     * @param $order_id
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function detail($order_id)
    {
		$express_list = config("express_name_list");
        $detail = OrderModel::detail($order_id);
        return $this->fetch('detail', compact('detail' , 'express_list'));
    }

    /**
     * 确认发货
     * @param $order_id
     * @return array
     * @throws \think\exception\DbException
     */
    public function delivery($order_id)
    {
        $model = OrderModel::detail($order_id);
        if ($model->delivery($this->postData('order'))) {
			$obj = new  GetTuiModel;
			$info = db("order")->where(['order_id' => $order_id])->find();
			$obj->sendToClient($info['user_id'] , '您购买的产品已发货' , '您的订单【'.$info['order_no'].'】平台已发货，详情请在个人中心查看');
            return $this->renderSuccess('发货成功');
        }
        $error = $model->getError() ?: '发货失败';
        return $this->renderError($error);
    }
	
	/**
	 * 导出
	 */
	public function export() {
		$filter = [];
		$order_types = $this->request->param("order_type_data/s",'');
		switch( $order_types ) {
			case 'delivery_list' :
				$filter = [
					'o.pay_status' => 20,
					'o.delivery_status' => 10
				];
			break;
			case 'receipt_list' :
				$filter = [
					'o.pay_status' => 20,
					'o.delivery_status' => 20,
					'o.receipt_status' => 10
				];
			break;
			case 'pay_list' :
				$filter = ['o.pay_status' => 10, 'o.order_status' => 10];
			break;
			case 'complete_list' :
				$filter = ['o.order_status' => 30];
			break;
			case 'cancel_list' :
				$filter = ['o.order_status' => 20];
			break;
		}
		$order_no = $this->request->param("order_no",'');
		if( $order_no ) {
			$filter['o.order_no'] = $order_no;
		}
		$type = $this->request->param("type/d",0);
		if( $type > 0 ){
			$filter['o.type'] = $type;
		}
		$pay_type = $this->request->param("pay_type/d",0);
		if( $pay_type > 0 ) {
			$filter['o.pay_type'] = $pay_type;
		}
		$order_list = db("order")->alias("o")->join("yoshop_user u",'u.user_id = o.user_id')->join("yoshop_order_address a" , 'a.order_id = o.order_id')
					  ->where($filter)->field("o.order_id,o.type,u.nickname,o.total_price,o.express_price,o.pay_price,o.pay_integral,o.pay_status,o.pay_time,o.pay_type,o.transaction_id,o.express_company,o.express_no,a.name,a.phone,o.create_time")->select()->toArray();
		$field = [
			'订单编号',
			'订单类型',
			'购买用户',
			'订单总金额' ,
			'物流费' , 
			'实际支付金额' , 
			'支付积分' , 
			'支付状态' , 
			'支付时间' , 
			'支付方式' ,
			'支付单号', 
			'物流公司' , 
			'快递单号' , 
			'收货人' ,
			'联系电话',
			'下单时间'
		];
		if( $order_list ) {
			foreach( $order_list as $k => $v ) {
				$order_list[$k]['type'] = $v['type'] == 1 ? '商城订单' : '兑换订单';
				$order_list[$k]['pay_status'] = $v['pay_status'] == 20 ? '已支付' : '未支付';
				$order_list[$k]['pay_time'] = $v['pay_status'] == 20 ? date("Y-m-d H:i:s" , $v['pay_time']) : '--';
				$order_list[$k]['pay_type'] = $v['pay_type'] == 1 ? '微信支付' : ($v['pay_type'] == 1 ? '支付宝支付' : '余额支付');
				$order_list[$k]['create_time'] = date("Y-m-d H:i:s" , $v['create_time']);
			}
		}
		\app\common\extend\Excel::export($order_list , $field);
	}

}
