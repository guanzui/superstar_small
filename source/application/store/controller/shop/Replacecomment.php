<?php
namespace app\store\controller\shop;

use app\store\controller\Controller;
/**
 * 换油评论
 * Class Index
 * @package app\store\controller
 */
class Replacecomment extends Controller
{
    public function index()
    {
		$where = [];
		$newwhere = '';
		$shop_id = $this->request->param("shop_id/d",0);
		if( $shop_id > 0 ) {
			$where['sc.shop_id'] = $shop_id;
		}
		$status = $this->request->param("status/d" , -1);
		if( $status >= 0 ) {
			$where['sc.status'] = $status;
		}
		$type = $this->request->param("type/d" , -1);
		if( $type > 0 ) {
			$where['sc.type'] = $type;
		}
		$merch = $this->request->param("merch" , '');
		if( $merch ) {
			$newwhere = 's.shop_id = "'.$merch.'" OR s.shop_name = "'.$merch.'"';
		}
        $list = db("shop_yuyue_service_comment")->where($where)->alias('sc')->join("shop s" , 's.shop_id = sc.shop_id')->where($newwhere)->join("user u",'u.user_id = sc.user_id')->field("u.nickname,u.mobile,s.shop_name,sc.*")->order(['sc.id' => 'DESC'])->paginate(15, false, ['query' => $this->request->request()]);
        if( request()->isAjax() ) {
			exit(json_encode($list));
		}
		return $this->fetch('index', compact('list'));
    }
    
    
    public function delete() {
        $id = $this->request->post('id' , 0);
        if( $id <= 0 ) $this->error('参数错误，请核对');
        $res = db('shop_yuyue_service_comment')->where(['id' => $id])->delete();
        if( $res ) {
            $this->success('删除成功');
        } else {
            $this->error("操作失败");
        }
    }
	
	public function infosave() {
        $id = $this->request->param('id/d' , 0);
        $field = $this->request->param('field');
        $value = $this->request->param('value/d' , 0);
        if( $id <= 0 || $field == '' ) {
            $this->error('数据不完整，请核对');
        }
        $res = db("shop_yuyue_service_comment")->where(['id' => $id])->update([$field => $value]);
        if( $res ) {
            $this->success('操作成功');
        } else {
            $this->error('操作失败');
        }
    }
	
	public function export()
    {
		$where = [];
		$shop_id = $this->request->param("shop_id/d",0);
		if( $shop_id > 0 ) {
			$where['sc.shop_id'] = $shop_id;
		}
		$status = $this->request->param("status/d" , -1);
		if( $status >= 0 ) {
			$where['sc.status'] = $status;
		}
		$type = $this->request->param("type/d" , -1);
		if( $type > 0 ) {
			$where['sc.type'] = $type;
		}
		$newwhere = '';
		$merch = $this->request->param("merch" , '');
		if( $merch ) {
			$newwhere = 's.shop_id = "'.$merch.'" OR s.shop_name = "'.$merch.'"';
		}
        $list = db("shop_yuyue_service_comment")->where($where)->alias('sc')->where($newwhere)->join("shop s" , 's.shop_id = sc.shop_id')->join("user u",'u.user_id = sc.user_id')->field("u.nickname,u.mobile,s.shop_name,sc.*")->order(['sc.id' => 'DESC'])->select()->toArray();
		$new_data = [];
		if( $list ){
			foreach( $list as $k => $v ) {
				$new_data[] = [
					'id' => $v['id'],
					'shop_name' => $v['shop_name'],
					'nickname' => $v['nickname'],
					'type' => $v['type'] == 1 ? '差评' : ($v['type'] == 2 ? '中评' : '好评'),
					'service_star' => $v['service_star'],
					'skill_star' => $v['skill_star'],
					'content' => $v['content'],
					'status' => $v['status'] == 1 ? '显示' : '隐藏',
					'create_time' => date("Y-m-d H:i:s" , $v['create_time'])
				];
			}
		}
		$field = ['ID' , '服务商' , '用户' , '类型' , '服务态度' , '技术态度' , '评论内容' , '状态' , '创建时间'];
		\app\common\extend\Excel::export($new_data , $field);
	}
}
