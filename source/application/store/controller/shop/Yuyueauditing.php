<?php
namespace app\store\controller\shop;

use app\store\controller\Controller;
use app\common\model\GetTui as GetTuiModel;
/**
 * 预约核算审核
 * Class Index
 * @package app\store\controller
 */
class Yuyueauditing extends Controller
{
    public function index()
    {
		$where = ['sc.is_settlement' => 1];
		$shop_id = $this->request->param("shop_id/d",0);
		if( $shop_id > 0 ) {
			$where['sc.shop_id'] = $shop_id;
		}
		$status = $this->request->param("status/d" , 1);
		if( $status >= 0 ) {
			$where['sc.settlement_status'] = $status;
		}
		
		$newwhere = '';
		$merch = $this->request->param("merch" , '');
		if( $merch ) {
			$newwhere = 's.shop_id = "'.$merch.'" OR s.shop_name LIKE "%'.$merch.'%"';
		}
        $list = db("shop_yuyue_service")->alias('sc')->join("shop s" , 's.shop_id = sc.shop_id')->where($where)->where($newwhere)
				->join("user u",'u.user_id = sc.user_id')->field("u.nickname,u.mobile,s.shop_name,sc.*")->order(['sc.id' => 'DESC'])->paginate(15, false, ['query' => $this->request->request()])->each(function($item,$key){
					switch($item['settlement_status']) {
							case 0 :
								$status_name = "待申请";
							break;
							case 1 :
								$status_name = "待审核";
							break;
							case 2 :
								$status_name = "已驳回";
							break;
							case 3 :
								$status_name = "已通过";
							break;
					}
					$item['status_name'] = $status_name;
					return $item;
				});
        if( request()->isAjax() ) {
			exit(json_encode($list));
		}
		$data = [
			'total' => db("shop_yuyue_service")->alias('sc')->join("shop s" , 's.shop_id = sc.shop_id')->join("user u",'u.user_id = sc.user_id')->where(['sc.is_settlement' => 1])->count() * 100,
			'y_price' => db("shop_yuyue_service")->alias('sc')->join("shop s" , 's.shop_id = sc.shop_id')->join("user u",'u.user_id = sc.user_id')->where(['sc.settlement_status' => 3])->count() * 100,
			'sh_price' => db("shop_yuyue_service")->alias('sc')->join("shop s" , 's.shop_id = sc.shop_id')->join("user u",'u.user_id = sc.user_id')->where(['sc.settlement_status' => 1])->count() * 100,
			'wtg_price' => db("shop_yuyue_service")->alias('sc')->join("shop s" , 's.shop_id = sc.shop_id')->join("user u",'u.user_id = sc.user_id')->where(['sc.settlement_status' => 2])->count() * 100,
			'w_price' => db("shop_yuyue_service")->alias('sc')->join("shop s" , 's.shop_id = sc.shop_id')->join("user u",'u.user_id = sc.user_id')->where(['sc.settlement_status' => 0 , 'sc.is_settlement' => 1])->count() * 100,
		];
		return $this->fetch('index', compact('list','data' , 'status'));
    }
    
    
    public function delete() {
        $id = $this->request->post('clerk_id' , 0);
        if( $id <= 0 ) $this->error('参数错误，请核对');
        $res = db('shop_yuyue_service_comment')->where(['id' => $id])->delete();
        if( $res ) {
            $this->success('删除成功');
        } else {
            $this->error("操作失败");
        }
    }
	
	public function infosave() {
        $id = $this->request->param('id/d' , 0);
        $status = $this->request->param('status/d' , 2);
        $note = $this->request->param('note' , '');
        if( $id <= 0 || !$status || ( $status == 2 && $note == '' ) ) {
            $this->error('数据不完整，请核对');
        }
		$info = db("shop_yuyue_service")->where(['id' => $id])->find();
        $res = db("shop_yuyue_service")->where(['id' => $id])->update(['settlement_status' => $status,'sh_time' => time(),'settlement_note' => $note]);
        if( $res ) {
			//增加积分
			if( $status == 3 ) {
				$member_infos = db("shop")->field("u.*")->alias("s")->join("yoshop_user u " , 'u.user_id = s.user_id')->where(['s.shop_id' => $info['shop_id']])->find();
				if( $member_infos ) {
					//写入用户交易日志
					db("user_integral_log")->insert([
						'user_id' => $member_infos['user_id'],
						'num' => 100,
						'type' => 1,
						'content' => '服务费结算积分增加【100】',
						'create_time' => time()
					]);
					db("user")->where(['user_id' => $member_infos['user_id']])->update(['integral' => $member_infos['integral'] + 100 , 'total_integral' => $member_infos['total_integral'] + 100]);
				}
			}
			$obj = new  GetTuiModel;
			$obj->sendToClient($info['user_id'] , '结算审核通知' , '您申请的结算'.($status == 3?'已通过审核' : '审核失败').($note?',备注：' . $note:'').',详情请在商户中心查看');
            $this->success('操作成功');
        } else {
            $this->error('操作失败');
        }
    }
	
	public function export()
    {
		$where = ['sc.is_settlement' => 1];
		$shop_id = $this->request->param("shop_id/d",0);
		if( $shop_id > 0 ) {
			$where['sc.shop_id'] = $shop_id;
		}
		$status = $this->request->param("status/d" , -1);
		if( $status >= 0 ) {
			$where['sc.status'] = $status;
		}
		
		$newwhere = '';
		$merch = $this->request->param("merch" , '');
		if( $merch ) {
			$newwhere = 's.shop_id = "'.$merch.'" OR s.shop_name LIKE "%'.$merch.'%"';
		}
        $list = db("shop_yuyue_service")->alias('sc')->join("shop s" , 's.shop_id = sc.shop_id')->where($where)->where($where)
				->join("user u",'u.user_id = sc.user_id')->field("u.nickname,u.mobile,s.shop_name,sc.*")->order(['sc.id' => 'DESC'])->select()->each(function($item,$key){
			switch($item['settlement_status']) {
					case 0 :
						$status_name = "待申请";
					break;
					case 1 :
						$status_name = "待审核";
					break;
					case 2 :
						$status_name = "已驳回";
					break;
					case 3 :
						$status_name = "已通过";
					break;
			}
			$item['status_name'] = $status_name;
			return $item;
		})->toArray();
		$new_data = [];
		if( $list ){
			foreach( $list as $k => $v ) {
				$new_data[] = [
					'id' => $v['id'],
					'shop_name' => $v['shop_name'],
					'nickname' => $v['nickname'],
					'contact' => $v['contact'],
					'mobile' => $v['mobile'],
					'cart_brand' => $v['cart_brand'],
					'status_name' => $v['status_name'],
					'sh_time' => $v['status'] > 1 ? date("Y-m-d H:i:s" , $v['sh_time']) : '--',
					'create_time' => date("Y-m-d H:i:s" , $v['create_time'])
				];
			}
		}
		$field = ['ID' , '服务商' , '用户' , '预约人' , '联系电话' , '车辆品牌' , '状态' , '审核时间', '创建时间'];
		\app\common\extend\Excel::export($new_data , $field);
	}
}
