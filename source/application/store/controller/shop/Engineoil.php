<?php
namespace app\store\controller\shop;

use app\store\controller\Controller;
/**
 * 机油列表
 * Class Index
 * @package app\store\controller
 */
class Engineoil extends Controller
{
    public function index()
    {
        $list = db("engineoil")->order(['id' => 'DESC'])->paginate(15, false, ['query' => $this->request->request()]);
        return $this->fetch('index', compact('list'));
    }
    
    public function add() {
        return $this->post();
    }
    
    public function edit() {
        $id = $this->request->param('id/d' , 0);
        if( !$data = db("engineoil")->where(['id' => $id])->find()){
            $this->error("数据不存在");
        }
        return $this->post( $data );
    }
    
    public function delete() {
        $id = $this->request->post('id' , 0);
        if( $id <= 0 ) $this->error('参数错误，请核对');
        $res = db('engineoil')->where(['id' => $id])->delete();
        if( $res ) {
            $this->success('删除成功');
        } else {
            $this->error("操作失败");
        }
    }
    
    public function post( $save_data = [] ) {
        if( $this->request->isAjax()) {
            $params = $this->postData('poster');
            $params['create_time'] = time();
            $id = $this->request->param('id/d',0);
            if( $id > 0 ) {
                $res = db('engineoil')->where(['id' => $id])->update($params);
                if( $res ) {
                    $this->success('机油修改成功');
                } else {
                    $this->error("编辑失败");
                }
            } else {
                $res = db('engineoil')->insert($params);
                if( $res ) {
                    $this->success('机油新增成功');
                } else {
                    $this->error("新增失败");
                }
            }
        }
        return $this->fetch('' , compact('save_data'));
    }
}
