<?php
namespace app\store\controller\shop;

use app\store\controller\Controller;
/**
 * 换油记录
 * Class Index
 * @package app\store\controller
 */
class Replace extends Controller
{
    public function index()
    {
		$where = [];
		$newwhere = '';
		$shop_id = $this->request->param("shop_id/d",0);
		if( $shop_id > 0 ) {
			$where['ys.shop_id'] = $shop_id;
		}
		$type = $this->request->param("type/d" , -1);
		if( $type > 0 ) {
			$where['ys.type'] = $type;
		}
		$status = $this->request->param("status/d" , -1);
		if( $status >= 0 ) {
			$where['ys.status'] = $status;
		}
		$merch = $this->request->param("merch" , '');
		if( $merch ) {
			$newwhere = 's.shop_id = "'.$merch.'" OR s.shop_name LIKE "%'.$merch.'%"';
		}
        $list = db("shop_yuyue_service")->where($where)->alias('ys')->join("shop s" , 's.shop_id = ys.shop_id')->where($newwhere)->join("user u",'u.user_id = ys.user_id')->field("u.nickname,u.mobile,s.shop_name,ys.*")->order(['ys.id' => 'DESC'])->paginate(15, false, ['query' => $this->request->request()])->each(function($item,$key){
			switch( $item['status'] ) {
				case 0 :
					$statu_name = "等待服务商审核";
				break;
				case 1 :
					$statu_name = "服务商已确认";
				break;
				case 2 :
					$statu_name = "服务已开始";
				break;
				case 3 :
					$statu_name = "用户确认完成";
				break;
				case 4 :
					$statu_name = "用户申请取消";
				break;
				case 5 :
					$statu_name = "服务商同意取消";
				break;
				case 6 :
					$statu_name = "拒绝服务";
				break;
			}
			$item['statu_name'] = $statu_name;
			return $item;
		});
		if( request()->isAjax() ) {
			exit(json_encode($list));
		}
		return $this->fetch('index', compact('list'));
    }
	
    public function show() {
        $id = $this->request->param('id/d' , 0);
        if( !$info = db("shop_yuyue_service")->alias('ys')->join("shop s" , 's.shop_id = ys.shop_id')->where(['id' => $id])->join("user u",'u.user_id = ys.user_id')->field("u.nickname,u.mobile,s.shop_name,ys.*")->find()){
            $this->error("数据不存在");
        }
		$info['product'] = db("engineoil")->where(['id' => $info['engineoil_id']])->find();
		return $this->fetch('show', compact('info'));
    }
    
    public function delete() {
        $id = $this->request->post('id' , 0);
        if( $id <= 0 ) $this->error('参数错误，请核对');
        $res = db('shop_yuyue_service')->where(['id' => $id])->delete();
        if( $res ) {
			db('shop_yuyue_service_comment')->where(['yuyue_service_id' => $id])->delete();
            $this->success('删除成功');
        } else {
            $this->error("操作失败");
        }
    }
	
	 public function export()
    {
		$where = [];
		$shop_id = $this->request->param("shop_id/d",0);
		if( $shop_id > 0 ) {
			$where['ys.shop_id'] = $shop_id;
		}
		$type = $this->request->param("type/d" , -1);
		if( $type > 0 ) {
			$where['ys.type'] = $type;
		}
		$status = $this->request->param("status/d" , -1);
		if( $status >= 0 ) {
			$where['ys.status'] = $status;
		}
		$newwhere = '';
		$merch = $this->request->param("merch" , '');
		if( $merch ) {
			$newwhere = 's.shop_id = "'.$merch.'" OR s.shop_name LIKE "%'.$merch.'%"';
		}
        $list = db("shop_yuyue_service")->where($where)->alias('ys')->where($newwhere)->join("shop s" , 's.shop_id = ys.shop_id')->join("user u",'u.user_id = ys.user_id')->field("u.nickname,u.mobile,s.shop_name,ys.*")->order(['ys.id' => 'DESC'])->select()->each(function($item,$key){
			switch( $item['status'] ) {
				case 0 :
					$statu_name = "等待服务商审核";
				break;
				case 1 :
					$statu_name = "服务商已确认";
				break;
				case 2 :
					$statu_name = "服务已开始";
				break;
				case 3 :
					$statu_name = "用户确认完成";
				break;
				case 4 :
					$statu_name = "用户申请取消";
				break;
				case 5 :
					$statu_name = "服务商同意取消";
				break;
				case 6 :
					$statu_name = "拒绝服务";
				break;
			}
			$item['statu_name'] = $statu_name;
			//查询使用机油
			$item['product_name'] = '--';
			if( $item['engineoil_id'] > 0 ) { 
				$item['product_name'] = db("engineoil")->where(['id' => $item['engineoil_id']])->value('title');
			}
			return $item;
		})->toArray();
		$new_data = [];
		if( $list ){
			foreach( $list as $k => $v ) {
				$new_data[] = [
					'id' => $v['id'],
					'shop_name' => $v['shop_name'],
					'nickname' => $v['nickname'],
					'type' => $v['type'] == 1 ? '补油' : '换油',
					'cart_brand' => $v['cart_brand'],
					'yuyue_time' => $v['yuyue_time'] . ' ' . $v['specific_time'],
					'contact' => $v['contact'],
					'mobile' => $v['mobile'],
					'mileage' => $v['mileage'],
					'statu_name' => $v['statu_name'],
					'create_time' => date("Y-m-d H:i:s" , $v['create_time']),
					'shop_confirm_time' => $v['shop_confirm_time'] > 0 ? date("Y-m-d H:i:s" , $v['shop_confirm_time']) : '--',
					'shop_qrcode_time' => $v['shop_qrcode_time'] > 0 ? date("Y-m-d H:i:s" , $v['shop_qrcode_time']) : '--',
					'user_complete_time' => $v['user_complete_time'] > 0 ? date("Y-m-d H:i:s" , $v['user_complete_time'])  : '--',
					'product_name' => $v['product_name'],
					'use_capacity' => $v['use_capacity'] > 0 ? $v['use_capacity'] : '--',
				];
			}
		}
		$field = ['ID' , '服务门店' , '预约用户' , '服务类型' , '车型' , '预约时间' , '联系人' , '联系电话' , '行驶里程' , '状态' , '申请时间' , '商家确认时间' , '商家核销时间', '用户完成时间' , '使用机油' , '用油量'];
		\app\common\extend\Excel::export($new_data , $field);
	}
}
