<?php
namespace app\store\controller\goods;

use app\store\controller\Controller;
use think\Request;

/**
 * 用户点评
 * Class User
 * @package app\store\controller
 */
class Comment extends Controller
{
    /**
     * 用户点评列表
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $request = Request::instance();
		$where = [];
		$user_id = $this->request->param("user_id/d",0);
		if( $user_id > 0 ) {
			$where['user_id'] = $user_id;
		}
		$type = $this->request->param("type/d",0);
		if( $type > 0 ) {
			$where['type'] = $type;
		}
        $list = db("goods_comment")->order(['comment_id' => 'DESC'])->where($where)->paginate(15, false, ['query' => $request->request()])->each(function($item, $key){
            //查询用户 / 商品 等信息
            $item['goods_name'] = db("goods")->where(['goods_id' => $item['goods_id']])->value("goods_name");
            $item['nickname'] = db("user")->where(['user_id' => $item['user_id']])->value("nickname");
            $item['thumb_list'] ? $item['thumb_list'] = json_decode( $item['thumb_list'] ) : '';
            return $item;
        });
        return $this->fetch('index', compact('list'));
    }
    
    /**
     * 用户点评删除
     */
    public function delete() {
        $request = Request::instance();
        $id = $request->post('comment_id' , 0);
        if( $id <= 0 ) $this->error('参数错误，请核对');
        $res = db('goods_comment')->where(['comment_id' => $id])->delete();
        if( $res ) {
            $this->success('删除成功');
        } else {
            $this->error("操作失败");
        }
    }
	
	public function infosave() {
        $comment_id = $this->request->param('comment_id/d' , 0);
        $field = $this->request->param('field');
        $value = $this->request->param('value/d' , 0);
        if( $comment_id <= 0 || $field == '' ) {
            $this->error('数据不完整，请核对');
        }
        $res = db("goods_comment")->where(['comment_id' => $comment_id])->update([$field => $value]);
        if( $res ) {
            $this->success('操作成功');
        } else {
            $this->error('操作失败');
        }
    }
}