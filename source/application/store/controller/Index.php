<?php
namespace app\store\controller;

/**
 * 后台首页
 * Class Index
 * @package app\store\controller
 */
class Index extends Controller
{
    public function index()
    {
		//判断当前是否是默认首页
		if( $this->store['user']['role_data'] != "*" ) {
			$one_level_field = array_keys($this->menu_data)[0];
			$this->redirect(url($this->menu_data[$one_level_field]['index']));
			exit();
		}
		$day_start_time = strtotime( date("Y-m-d" , time() ) );
		$day_end_time = $day_start_time + 3600 * 24 - 1;
		$day7_end_time = $day_start_time - ( 3600 * 24 * 7 - 1 );
		$day15_end_time = $day_start_time - ( 3600 * 24 * 15 - 1 );
		//统计
		$count = [
			'sh' 	=> db("shop_yuyue_service")->where(['is_settlement' => 1, 'settlement_status' => 1])->count(),
			'tx'	=> db("member_withdrawcash")->where('`status` = 0')->count(),
			'rjff'	=> db("shop_yuyue_service")->where('`status` in(1,2,3) AND `shop_qrcode_time` > ' . $day_start_time . ' AND `shop_qrcode_time` <= ' . $day_end_time)->count(),
			'rjdd'  => db("order")->where('`pay_status` = 20 AND `order_status` != 20 AND `create_time` > ' . $day_start_time . ' AND `create_time` <= ' . $day_end_time)->count(),
			'user_cart' => db("user")->where(['status' => 2 , 'is_open_cart' => 1])->count(),
			'fy' 	=> db("member_cart_order_distribution")->sum("price"),
			'yw'	=> db("user")->where(['identity' => 10 , 'status' => 2])->count(),
			'xsjl'	=> db("user")->where(['identity' => 20 , 'status' => 2])->count(),
			'zjl'	=> db("user")->where(['identity' => 30 , 'status' => 2])->count(),
		];
		//订单数据
		$order = [
			'day7' => db("order")->where('`pay_status` = 20 AND `order_status` != 20 AND `create_time` > ' . $day7_end_time . ' AND `create_time` <= ' . $day_end_time)->count(),
			'day7_price' => db("order")->where('`pay_status` = 20 AND `order_status` != 20 AND `create_time` > ' . $day7_end_time . ' AND `create_time` <= ' . $day_end_time)->sum('total_price'),
			'day15' => db("order")->where('`pay_status` = 20 AND `order_status` != 20 AND `create_time` > ' . $day15_end_time . ' AND `create_time` <= ' . $day_end_time)->count(),
			'day15_price' => db("order")->where('`pay_status` = 20 AND `order_status` != 20 AND `create_time` > ' . $day15_end_time . ' AND `create_time` <= ' . $day_end_time)->sum('total_price'),
			'all' => db("order")->where('`pay_status` = 20 AND `order_status` != 20')->count(),
			'all_price' => db("order")->where('`pay_status` = 20 AND `order_status` != 20')->sum('total_price')
		];
		//预约数据统计
		$yuyue = [
			'day7' => db("shop_yuyue_service")->where('`status` IN(2,3) AND `shop_qrcode_time` > ' . $day7_end_time . ' AND `shop_qrcode_time` <= ' . $day_end_time)->count(),
			'day15' => db("shop_yuyue_service")->where('`status` IN(2,3) AND `shop_qrcode_time` > ' . $day15_end_time . ' AND `shop_qrcode_time` <= ' . $day_end_time)->count(),
			'all' => db("shop_yuyue_service")->where('`status` IN(2,3)')->count(),
			'js' => db("shop_yuyue_service")->where('`status` IN(2,3)')->count()
		];
        return $this->fetch('index' , compact( 'count' ,'order','yuyue'));
    }

    public function demolist()
    {
        return $this->fetch('demo-list');
    }
    
    public function memberList() {
        $key = $this->request->post("key");
        if( $key == '' ) $this->error('搜索关键字不能为空');
        $list = db("user")->where("`mobile` = '{$key}' OR `nickname` LIKE '%{$key}%' OR `user_id` = '{$key}'")->field("nickname,avatar,user_id,mobile")->select();
        if( $list->isEmpty() ) {
            $this->error('未检索到数据');
        } else {
            $this->success('成功' ,'', $list);
        }
    }
	
	public function userinfo() {
		$id = $this->request->param("id/d",0);
		if( $id <= 0 ) $this->error("信息错误");
		$info = thumbImgUrl( db("user")->where(['user_id' => $id])->find() );
		if( !$info ) {
			$this->error("信息不存在");
		}
		$info['login_time'] = $info['login_time'] > 0 ? date("Y-m-d H:i:s" , $info['login_time']) : '暂无';
		$info['sex'] = $info['sex'] == 1 ? '男' : '女';
		$info['address'] = $info['province_name'] . ' - ' .$info['city_name'] . ' - ' . $info['area_name'];
		$this->success('成功' ,'', $info);
	}
	
	public function shopinfo() {
		$id = $this->request->param("id/d" , 0);
		if( $id <= 0 ) $this->error("信息错误");
		$info = thumbImgUrl( db("shop")->where(['shop_id' => $id])->find() );
		if( !$info ) $this->error("信息不存在");
		$info['address'] = $info['province_name'].$info['city_name'].$info['area_name'].$info['address'];
		$info['type'] = $info['type'] == 1 ? '机油' : '柴油';
		$this->success('成功' ,'', $info);
	}

}
