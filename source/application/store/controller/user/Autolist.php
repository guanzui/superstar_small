<?php

namespace app\store\controller\user;

use app\store\controller\Controller;

/**
 * 车辆管理
 * Class autolits
 * @package app\store\controller
 */
class Autolist extends Controller
{
    public function index() {
        $where = [];
		$carowner = $this->request->param("carowner/s" ,'');
		if( $carowner ) {
			$where['carowner'] = $carowner;
		}
		$user_id = $this->request->param("user_id/d" ,0);
		if( $user_id ) {
			$where['user_id'] = $user_id;
		}
		$carowner_mobile = $this->request->param("carowner_mobile/s" ,'');
		if( $carowner_mobile ) {
			$where['carowner_mobile'] = $carowner_mobile;
		}
        $list = db("member_auto_info")->order(['id' => 'DESC'])->where($where)->paginate(15, false, ['query' => $this->request->request()])->each(function($item , $key){
            $item['nickname'] = db("user")->where(['user_id' => $item['user_id']])->value('nickname');
            return $item;
        });
        return $this->fetch('', compact('list'));
    }
    
    public function show() {
        $id = $this->request->param('id/d' , 0);
        if( $id <= 0 ) $this->error('参数错误，请核对');
        $info = db('member_auto_info')->where(['id' => $id])->find();
        $info['nickname'] = db("user")->where(['user_id' => $info['user_id']])->value('nickname');
        return $this->fetch('', compact('info'));
    }
    
    public function delete() {
        $id = $this->request->post('id/d' , 0);
        if( $id <= 0 ) $this->error('参数错误，请核对');
        $res = db('member_auto_info')->where(['id' => $id])->delete();
        if( $res ) {
            $this->success('删除成功');
        } else {
            $this->error("操作失败");
        }
    }
}