<?php

namespace app\store\controller\user;

use app\store\controller\Controller;
use app\common\model\GetTui as GetTuiModel;


/**
 * 团长申请管理
 * Class items
 * @package app\store\controller
 */
class Items extends Controller
{
    public function index() {
        $where = [];
		$name = $this->request->param("name/s" ,'');
		if( $name ) {
			$where['i.name'] = $name;
		}
		$idcard = $this->request->param("idcard/s" ,'');
		if( $idcard ) {
			$where['i.idcard'] = $idcard;
		}
		$status = $this->request->param("status" ,'');
		if( $status != '' ) {
			$where['i.status'] = $status;
		}
        $list = db("member_items")->alias("i")->join("yoshop_user u",'u.user_id = i.user_id')->field("i.*,u.nickname,u.avatar")->order(['i.status' => 'DESC'])
				->where($where)->paginate(15, false, ['query' => $this->request->request()])->each(function($item , $key){
					$item['create_time'] = date("Y-m-d H:i:s" , $item['create_time']);
					return $item;
				});
        return $this->fetch('', compact('list'));
    }
	
	
	public function infosave() {
        $id = $this->request->param('id/d' , 0);
		$info = db("member_items")->where(['id' => $id])->find();
		if( !$info ) $this->error("信息错误");
		if( $info['status'] == 2 ) $this->error("已审核通过，请勿重复审核！");
        $status = $this->request->param('status/d' , 2);
        $note = $this->request->param('note' , '');
        if( $id <= 0 || !$status || ( $status == 2 && $note == '' ) ) {
            $this->error('数据不完整，请核对');
        }
        $res = db("member_items")->where(['id' => $id])->update(['status' => $status,'note' => $note]);
        if( $res ) {
			if( $status == 2 ) {
				db("user")->where(['user_id' => $info['user_id']])->update(["is_item" => 1]);
				//写入系统日志
				$msg = '您申请的【团长】已通过审核，请在个人中心查看';
			} else {
				$msg = '很抱歉，您申请的【团长】未能通过审核，原因：' . $note . '，详情请在个人中心查看。';
			}
			$obj = new  GetTuiModel;
			$obj->sendToClient($info['user_id'] , '团长审核消息提示' , $msg);
            $this->success('操作成功');
        } else {
            $this->error('操作失败');
        }
    }
    
    public function delete() {
        $id = $this->request->post('id/d' , 0);
        if( $id <= 0 ) $this->error('参数错误，请核对');
        $res = db('member_items')->where(['id' => $id])->delete();
        if( $res ) {
            $this->success('删除成功');
        } else {
            $this->error("操作失败");
        }
    }
}