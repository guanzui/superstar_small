<?php
namespace app\store\controller\setting;

use app\store\controller\Controller;
use think\Request;

/**
 * 首页菜单管理
 * Class Ads
 * @package app\store\controller\setting
 */
class Menu extends Controller
{
    /**
     * 列表
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $request = Request::instance();
        $list = db("menu")->order(['sort' => 'asc'])->paginate(15, false, ['query' => $request->request()])->each(function($item, $key){
            //查询用户 / 商品 等信息
            switch( $item['target_type'] ) {
                case  1 : //跳转商品
                    $item['target_data'] = "商品名称：" . db("goods")->where(['goods_id' => $item['target_data']])->value("goods_name");
                break;
                case  2 : //跳转分类
                    $item['target_data'] = "分类名称：" . db("category")->where(['category_id' => $item['target_data']])->value("name");
                break;
                case 3 : //跳转文章
                    $item['target_data'] = "文档名称：" . db("article")->where(['article_id' => $item['target_data']])->value("title");
                break;
            }
            return $item;
        });
        return $this->fetch('index', compact('list'));
    }
    
    /**
     * 新增
     */
    public function add() {
        return $this->post();
    }
    
    /**
     * 修改
     */
    public function edit() {
        $id = $this->request->param('menu_id/d' , 0);
        if( !$data = db("menu")->where(['menu_id' => $id])->find()){
            $this->error("数据不存在");
        }
        return $this->post($data);
    }
    
    /**
     * 删除
     */
    public function delete() {
        $request = Request::instance();
        $id = $request->post('menu_id' , 0);
        if( $id <= 0 ) $this->error('参数错误，请核对');
        $res = db('menu')->where(['menu_id' => $id])->delete();
        if( $res ) {
            $this->success('删除成功');
        } else {
            $this->error("操作失败");
        }
    }
    
    private function post($save_data = array()) {
        if ($this->request->isAjax()) {
            $data = $this->postData('menu');
            $data['target_data'] = $data['target_type']==1?$this->request->post("goods"):($data['target_type']==2?$this->request->post("cat"):($data['target_type']==3?$this->request->post("article"):$this->request->post("url")));
            $menu_id = $this->request->post('id/d' , 0);
            if( $menu_id > 0 ) {
                $res = db("menu")->where(['menu_id' => $menu_id])->update($data);
                if( $res ) {
                    $this->success('菜单修改成功');
                } else {
                    $this->error('菜单修改失败');
                }
            }else {
                $data['create_time'] = time();
                $res = db("menu")->insert($data);
                if( $res ) {
                    $this->success('菜单添加成功');
                } else {
                    $this->error('菜单添加失败');
                }
            }
        }
        $goods_list = db("goods")->where(['goods_status' => 10])->field("goods_id,goods_name")->select();
        $cat_list = db("category")->field("category_id,name")->select();
        $article_list = db("article")->field("article_id,title")->select();
        return $this->fetch('',compact('goods_list','cat_list','article_list' , 'save_data'));
    }
}
