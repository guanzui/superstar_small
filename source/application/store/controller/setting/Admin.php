<?php
namespace app\store\controller\setting;

use app\store\controller\Controller;
use think\Request;

/**
 * 管理员列表
 * Class Admin
 * @package app\store\controller\setting
 */
class Admin extends Controller
{
    /**
     * 配送模板列表
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $request = Request::instance();
        $list = db("store_user")->order(['store_user_id' => 'DESC'])->alias("su")->join("yoshop_store_role sr" , "sr.role_id = su.role_id")->where(['sr.status' => 1])->field("su.*,sr.name as role_name")
				->paginate(15, false, ['query' => $request->request()])->each(function($item, $key){
            $item['create_time'] = date("Y-m-d H:i:s" , $item['create_time']);
			$item['update_time'] = date("Y-m-d H:i:s" , $item['update_time']);
            return $item;
        });
        return $this->fetch('index', compact('list'));
    }
    
    /**
     * 新增
     */
    public function add() {
        return $this->post();
    }
    
    /**
     * 修改
     */
    public function edit() {
        $id = $this->request->param('id/d' , 0);
        if( !$data = db("store_user")->where(['store_user_id' => $id])->find()){
            $this->error("数据不存在");
        }
        return $this->post($data);
    }
    
    /**
     * 删除
     */
    public function delete() {
        $request = Request::instance();
        $id = $request->post('id' , 0);
        if( $id <= 0 ) $this->error('参数错误，请核对');
        $res = db('store_user')->where(['store_user_id' => $id])->delete();
        if( $res ) {
            $this->success('删除成功');
        } else {
            $this->error("操作失败");
        }
    }
    
    private function post($save_data = array()) {
        if ($this->request->isAjax()) {
            $data = $this->postData('admin');
            $admin_id = $this->request->post('id/d' , 0);
            if( $admin_id > 0 ) {
				if( $data['password'] ){ 
					$data['password'] = yoshop_hash($data['password']);
				} else {
					unset( $data['password'] );
				}
				$data['update_time'] = time();
                $res = db("store_user")->where(['store_user_id' => $admin_id])->update($data);
                if( $res ) {
                    $this->success('修改成功');
                } else {
                    $this->error('修改失败');
                }
            }else {
                $data['create_time'] = time();
				$data['password'] = yoshop_hash($data['password']);
                $res = db("store_user")->insert($data);
                if( $res ) {
                    $this->success('添加成功');
                } else {
                    $this->error('添加失败');
                }
            }
        }
		$role_list = db("store_role")->where(['status' => 1])->field("role_id,name")->select()->toArray();
        return $this->fetch('',compact('role_list' , 'save_data'));
    }
}
