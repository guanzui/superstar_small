<?php
namespace app\store\controller\setting;

use app\store\controller\Controller;
use think\Request;

/**
 * 角色列表
 * Class Role
 * @package app\store\controller\setting
 */
class Role extends Controller
{
    /**
     * 配送模板列表
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $request = Request::instance();
        $list = db("store_role")->order(['role_id' => 'DESC'])->paginate(15, false, ['query' => $request->request()])->each(function($item, $key){
            $item['create_time'] = date("Y-m-d H:i:s" , $item['create_time']);
            return $item;
        });
        return $this->fetch('index', compact('list'));
    }
    
    /**
     * 新增
     */
    public function add() {
        return $this->post();
    }
    
    /**
     * 修改
     */
    public function edit() {
        $id = $this->request->param('id/d' , 0);
        if( !$data = db("store_role")->where(['role_id' => $id])->find()){
            $this->error("数据不存在");
        }
        return $this->post($data);
    }
    
    /**
     * 删除
     */
    public function delete() {
        $request = Request::instance();
        $id = $request->post('id' , 0);
        if( $id <= 0 ) $this->error('参数错误，请核对');
        $res = db('store_role')->where(['role_id' => $id])->delete();
        if( $res ) {
            $this->success('删除成功');
        } else {
            $this->error("操作失败");
        }
    }
    
    private function post($save_data = array()) {
		$menus = include(APP_PATH . "store/extra/menus.php");
		$menu_data = [];
		foreach( $menus as $k => $v ) {
			if( isset( $v['role'] ) ) continue;
			$menu_data[] = ['id' => $v['id'] , 'parent' => '#','text' => $v['name'] , 'state' => ['selected' => false]];
			if( !isset( $v['submenu'] ) ) continue;
			foreach( $v['submenu'] as $k1 => $v1 ) {
				$menu_data[] = ['id' => $v1['id'] , 'parent' => $v1['pid'],'text' => $v1['name'] , 'state' => ['selected' => !$save_data ? false : ( strpos($save_data['role_data'] , ','.$v1['id'].',') !== false ? true  : false )]];
			}
		}
        if ($this->request->isAjax()) {
            $data = $this->postData('role');
			if( $data['type'] == 1 && ( !isset( $data['role_data'] ) || !$data['role_data'] || !is_array($data['role_data']))){
				$this->error("请勾选权限列表");
			}
			$data['role_data'] = $data['type'] == 1 ? ',' . implode("," , $data['role_data'] ) . ',' : '*';
            $role_id = $this->request->post('id/d' , 0);
            if( $role_id > 0 ) {
                $res = db("store_role")->where(['role_id' => $role_id])->update($data);
                if( $res ) {
                    $this->success('修改成功');
                } else {
                    $this->error('修改失败');
                }
            }else {
                $data['create_time'] = time();
                $res = db("store_role")->insert($data);
                if( $res ) {
                    $this->success('添加成功');
                } else {
                    $this->error('添加失败');
                }
            }
        }
        return $this->fetch('',compact('save_data' , 'menu_data'));
    }
}
