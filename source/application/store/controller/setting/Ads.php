<?php
namespace app\store\controller\setting;

use app\store\controller\Controller;
use think\Request;

/**
 * 广告管理
 * Class Ads
 * @package app\store\controller\setting
 */
class Ads extends Controller
{
    private $type_list = [
        'index_banner' => '首页banner',
        'index_center1' => '首页热卖横屏广告',
        'index_center2' => '首页兑换横屏广告',
        //'start_up_list' => 'APP启动广告',
        //'member_center_ads' => '会员中心横条广告'
    ];
    /**
     * 配送模板列表
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $request = Request::instance();
        $list = db("ads")->order(['sort' => 'asc'])->paginate(15, false, ['query' => $request->request()])->each(function($item, $key){
            //查询用户 / 商品 等信息
            switch( $item['target_type'] ) {
                case  1 : //跳转商品
                    $item['target_data'] = "商品名称：" . db("goods")->where(['goods_id' => $item['target_data']])->value("goods_name");
                break;
                case  2 : //跳转分类
                    $item['target_data'] = "分类名称：" . db("category")->where(['category_id' => $item['target_data']])->value("name");
                break;
                case 3 : //跳转文章
                    $item['target_data'] = "文档名称：" . db("article")->where(['article_id' => $item['target_data']])->value("title");
                break;
            }
            return $item;
        });
        $type_list = $this->type_list;
        return $this->fetch('index', compact('list','type_list'));
    }
    
    /**
     * 新增
     */
    public function add() {
        return $this->post();
    }
    
    /**
     * 修改
     */
    public function edit() {
        $id = $this->request->param('ads_id/d' , 0);
        if( !$data = db("ads")->where(['ads_id' => $id])->find()){
            $this->error("数据不存在");
        }
        return $this->post($data);
    }
    
    /**
     * 删除
     */
    public function delete() {
        $request = Request::instance();
        $id = $request->post('ads_id' , 0);
        if( $id <= 0 ) $this->error('参数错误，请核对');
        $res = db('ads')->where(['ads_id' => $id])->delete();
        if( $res ) {
            $this->success('删除成功');
        } else {
            $this->error("操作失败");
        }
    }
    
    private function post($save_data = array()) {
        if ($this->request->isAjax()) {
            $data = $this->postData('ads');
            $data['target_data'] = $data['target_type']==1?$this->request->post("goods"):($data['target_type']==2?$this->request->post("cat"):($data['target_type']==3?$this->request->post("article"):$this->request->post("url")));
            $ads_id = $this->request->post('id/d' , 0);
            if( $ads_id > 0 ) {
                $res = db("ads")->where(['ads_id' => $ads_id])->update($data);
                if( $res ) {
                    $this->success('广告修改成功');
                } else {
                    $this->error('广告修改失败');
                }
            }else {
                $data['create_time'] = time();
                $res = db("ads")->insert($data);
                if( $res ) {
                    $this->success('广告添加成功');
                } else {
                    $this->error('广告添加失败');
                }
            }
        }
        $goods_list = db("goods")->where(['goods_status' => 10])->field("goods_id,goods_name")->select();
        $cat_list = db("category")->field("category_id,name")->select();
        $article_list = db("article")->field("article_id,title")->select();
        $type_list = $this->type_list;
        return $this->fetch('',compact('type_list','goods_list','cat_list','article_list' , 'save_data'));
    }
}
