<?php

namespace app\store\controller\setting;

use app\store\controller\Controller;

/**
 * 汽车品牌管理
 * Class User
 * @package app\store\controller
 */
class Autobrand extends Controller
{
    public function index() {
        $where = [];
        $list = db("auto_brand")->order(['sort' => 'DESC' , 'id' => 'DESC'])->where($where)->paginate(15, false, ['query' => $this->request->request()])->each(function($item, $key){
            $item['total'] = db("auto_type")->where(['auto_brand_id' => $item['id']])->count("id");
            return $item;
        });
        return $this->fetch('', compact('list'));
    }
    
    public function add() {
        return $this->post();
    }
    
    public function edit(){
        $id = $this->request->param('id/d' , 0);
        if( $id <= 0 ) $this->error('数据格式错误');
        $data = db("auto_brand")->where(['id' => $id])->find();
        if( !$data ) $this->error('数据不存在，请核对');
        return $this->post( $data );
    }
    
    private function post( $save_data = '' ) {
        if( $this->request->isAjax() ) {
            $params = $this->postData('brand');
            $id = $this->request->post('id/d' , 0);
            if( $id <= 0 ) {
                $params['create_time'] = time();
                $res = db("auto_brand")->insert($params);
                if( $res ) {
                    $this->success('品牌新增成功');
                } else {
                    $this->error('添加失败，请重试');
                }
            } else {
                $res = db("auto_brand")->where(['id' => $id])->update($params);
                if( $res ) {
                    $this->success('品牌修改成功');
                } else {
                    $this->error('修改失败，请重试');
                }
            }
        }
        return $this->fetch('', compact('save_data'));
    }
    
    public function delete() {
        $id = $this->request->post('id/d' , 0);
        if( $id <= 0 ) $this->error('参数错误，请核对');
        $res = db('auto_brand')->where(['id' => $id])->delete();
        if( $res ) {
            $this->success('删除成功');
        } else {
            $this->error("操作失败");
        }
    }
}