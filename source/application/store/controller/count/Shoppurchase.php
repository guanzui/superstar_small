<?php

namespace app\store\controller\count;

use app\store\controller\Controller;
/**
 * 数据统计
 * Class Count
 * @package app\store\controller
 */
class Shoppurchase extends Controller
{
    /**
     * 商家进销记录
     */
    public function index() {
        $where = [];
		$type = $this->request->param("type/d" , 0 );
		if( $type > 0 ) {
			$where['sl.type'] = $type;
		}
		$id = $this->request->param("id/d" , 0 );
		if( $id > 0 ) {
			$where['sl.engineoil_id'] = $id;
		}
		$shop_id = $this->request->param("shop_id/d" , 0 );
		if( $shop_id > 0 ) {
			$where['sl.shop_id'] = $shop_id;
		}
		$newwhere = '';
		$merch = $this->request->param("merch" , '');
		if( $merch ) {
			$newwhere = 's.shop_id = "'.$merch.'" OR s.shop_name = "'.$merch.'"';
		}
        $list = db("engineoil_shop_log")->alias('sl')->join("yoshop_shop s" , 's.shop_id = sl.shop_id')->where($newwhere)->join("yoshop_engineoil e",'e.id = sl.engineoil_id')->order(['sl.id' => 'DESC'])->where($where)->field("sl.*,s.shop_name,e.title")->paginate(15, false, ['query' => $this->request->request()]);
        $data = [
			'in' => db("engineoil_shop_log")->alias('sl')->join("yoshop_shop s" , 's.shop_id = sl.shop_id')->join("yoshop_engineoil e",'e.id = sl.engineoil_id')->where(array_merge($where,['sl.type' => 1]))->sum("num"),
			'out' => db("engineoil_shop_log")->alias('sl')->join("yoshop_shop s" , 's.shop_id = sl.shop_id')->join("yoshop_engineoil e",'e.id = sl.engineoil_id')->where(array_merge($where,['sl.type' => ['in','2,3']]))->sum("num")
		];
		$data['sy'] = $data['in'] - $data['out'];
		return $this->fetch('', compact('list' , 'data'));
    }
    
    /**
     * 删除
     */
    public function delete() {
        $id = $this->request->post('id' , 0);
        if( $id <= 0 ) $this->error('参数错误，请核对');
        $res = db('engineoil_shop_log')->where(['id' => $id])->delete();
        if( $res ) {
            $this->success('删除成功');
        } else {
            $this->error("操作失败");
        }
    }
	
	public function export()
    {
		$where = [];
		$type = $this->request->param("type/d" , 0 );
		if( $type > 0 ) {
			$where['sl.type'] = $type;
		}
		$id = $this->request->param("id/d" , 0 );
		if( $id > 0 ) {
			$where['sl.engineoil_id'] = $id;
		}
		$shop_id = $this->request->param("shop_id/d" , 0 );
		if( $shop_id > 0 ) {
			$where['sl.shop_id'] = $shop_id;
		}
		$newwhere = '';
		$merch = $this->request->param("merch" , '');
		if( $merch ) {
			$newwhere = 's.shop_id = "'.$merch.'" OR s.shop_name = "'.$merch.'"';
		}
        $list = db("engineoil_shop_log")->alias('sl')->join("yoshop_shop s" , 's.shop_id = sl.shop_id')->where($newwhere)->join("yoshop_engineoil e",'e.id = sl.engineoil_id')->order(['sl.id' => 'DESC'])->where($where)->field("sl.*,s.shop_name,e.title")->select()->toArray();
		$new_data = [];
		if( $list ){
			foreach( $list as $k => $v ) {
				$new_data[] = [
					'id' => $v['id'],
					'shop_name' => $v['shop_name'],
					'title' => $v['title'],
					'num' => $v['num'],
					'type' => $v['type'] == 1 ? '增加' : '减少',
					'content' => $v['content'],
					'create_time' => date("Y-m-d H:i:s" , $v['create_time'])
				];
			}
		}
		$field = ['ID' , '商家' , '机油' , '数量' , '类型' , '详情' , '时间'];
		\app\common\extend\Excel::export($new_data , $field);
	}
}