<?php

namespace app\store\controller\count;

use app\store\controller\Controller;
/**
 * 预约次数
 * Class Count
 * @package app\store\controller
 */
class Yuyuenum extends Controller
{
    /**
     * 商家进销记录
     */
    public function index() {
        $list = db("user")->where(['is_open_cart' => 1])->paginate(15, false, ['query' => $this->request->request()])->each(function($item,$key){
			$yuyue_info = db("shop_yuyue_service")->alias("ys")->join("yoshop_shop s" , "s.shop_id = ys.shop_id")->where(['ys.user_id' => $item['user_id']])->field("ys.yuyue_time,s.shop_name,s.shop_id")->order("id DESC")->find();
			if( !$yuyue_info ) {
				$yuyue_info = [
					'yuyue_time' => '暂无',
					'shop_name' => '暂无',
					'shop_id' => 0,
				];
			}
			$item = array_merge( $yuyue_info , $item );
			return $item;
		});
        $data = [
			'sy' => db("user")->where(['is_open_cart' => 1])->sum("service_num"),
			'xf' => db("user")->where(['is_open_cart' => 1])->sum("baoyang_num"),
		];
		$data['total'] = $data['sy'] + $data['xf'];
		return $this->fetch('', compact('list' , 'data'));
    }
	
	public function export()
    {
		$list = db("user")->where(['is_open_cart' => 1])->select()->each(function($item,$key){
			$yuyue_info = db("shop_yuyue_service")->alias("ys")->join("yoshop_shop s" , "s.shop_id = ys.shop_id")->where(['ys.user_id' => $item['user_id']])->field("ys.yuyue_time,s.shop_name,s.shop_id")->order("id DESC")->find();
			if( !$yuyue_info ) {
				$yuyue_info = [
					'yuyue_time' => '暂无',
					'shop_name' => '暂无',
					'shop_id' => 0,
				];
			}
			$item = array_merge( $yuyue_info , $item );
			return $item;
		})->toArray();
		$new_data = [];
		if( $list ){
			foreach( $list as $k => $v ) {
				$new_data[] = [
					'id' => $v['user_id'],
					'nickname' => $v['nickname'],
					'total' => $v['service_num'] + $v['baoyang_num'],
					'service_num' => $v['service_num'],
					'baoyang_num' => $v['baoyang_num'],
					'yuyue_time' => $v['yuyue_time'],
					'shop_name' => $v['shop_name'],
					'status' => $v['status'] == 2 ? '正常' : '禁止'
				];
			}
		}
		$field = ['ID' , '用户' , '服务总次数' , '已使用次数' , '未使用次数' , '上次预约时间' , '上次预约服务商' , '状态'];
		\app\common\extend\Excel::export($new_data , $field);
	}
}