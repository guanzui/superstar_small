<?php

namespace app\store\controller\finance;

use app\store\controller\Controller;
/**
 * 数据统计
 * Class Count
 * @package app\store\controller
 */
class Commission extends Controller
{
    /**
     * 分佣记录
     */
    public function index() {
        $where = [];
		$user_id = $this->request->param("user_id/d",0);
        if( $user_id > 0 ) {
			$where['od.user_id'] = $user_id;
		}
		$type = $this->request->param("type/d",0);
        if( $type > 0 ) {
			$where['od.type'] = $type;
		}
		$newwhere = '';
		$name = $this->request->param("name/s" , '');
		if( $name ) {
			$newwhere = "u.nickname = '{$name}' OR u.user_id = '{$name}' OR u.mobile = '{$name}'";
		}
		$list = db("member_cart_order_distribution")->alias('od')->join("yoshop_user u" , 'od.user_id = u.user_id')->where($newwhere)->order(['od.id' => 'DESC'])->where($where)->field("od.*,u.nickname,u.avatar")->paginate(15, false, ['query' => $this->request->request()]);
		return $this->fetch('', compact('list'));
    }
    
    /**
     * 删除
     */
    public function delete() {
        $id = $this->request->post('id' , 0);
        if( $id <= 0 ) $this->error('参数错误，请核对');
        $res = db('member_cart_order_distribution')->where(['id' => $id])->delete();
        if( $res ) {
            $this->success('删除成功');
        } else {
            $this->error("操作失败");
        }
    }
	
	public function export()
    {
		$where = [];
		$user_id = $this->request->param("user_id/d",0);
        if( $user_id > 0 ) {
			$where['od.user_id'] = $user_id;
		}
		$type = $this->request->param("type/d",0);
        if( $type > 0 ) {
			$where['od.type'] = $type;
		}
		$newwhere = '';
		$name = $this->request->param("name/s" , '');
		if( $name ) {
			$newwhere = "u.nickname = '{$name}' OR u.user_id = '{$name}' OR u.mobile = '{$name}'";
		}
		$list = db("member_cart_order_distribution")->alias('od')->join("yoshop_user u" , 'od.user_id = u.user_id')->where($newwhere)->order(['od.id' => 'DESC'])->where($where)->field("od.*,u.nickname,u.avatar")->select()->toArray();
		$new_data = [];
		if( $list ){
			foreach( $list as $k => $v ) {
				$new_data[] = [
					'id' => $v['id'],
					'nickname' => $v['nickname'],
					'num' => $v['ordersn'],
					'type' => $v['type'] == 1 ? '直推' : ($v['type'] == 2 ? '间推' : ($v['type'] == 3 ? '复购奖' : ($v['type'] == 3 ? '团长提成' : '感恩奖')) ),
					'num' => $v['price'],
					'create_time' => date("Y-m-d H:i:s" , $v['create_time'])
				];
			}
		}
		$field = ['ID' , '用户' , '订单号' , '类型' , '积分' , '创建时间'];
		\app\common\extend\Excel::export($new_data , $field);
	}
}