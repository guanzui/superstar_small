<?php

namespace app\store\controller\finance;

use app\store\controller\Controller;
/**
 * 奖金拨比
 * Class Count
 * @package app\store\controller
 */
class Allocate extends Controller
{
    /**
     * 余额消费记录
     */
    public function index() {
		$type = $this->request->param("type/d" , 0);
		$start = $this->request->param("start/s" , date("Y-m-d H:i:s" , strtotime('-1day')));
		$end = $this->request->param("end/s" , date("Y-m-d H:i:s" , time()));
		$sale_price = db("member_cart_order")->where('`status` = 1 AND `create_time` >= ' . strtotime($start) . ' AND `create_time` <= ' . strtotime($end))->sum("price");
        $where = '';
		if( $type > 0 ) {
			$where = '`type` = ' . $type . ' AND ';
		}
		$reward_price = db("member_cart_order_distribution")->where($where . '`create_time` >= ' . strtotime($start) . ' AND `create_time` <= ' . strtotime($end))->sum("price");
		$data = [
			'sale_price' => $sale_price,
			'reward_price' => $reward_price,
			'ratio' => $reward_price == 0 ? 0 : round( $reward_price / $sale_price , 4 )
		];
		return $this->fetch('', compact('data' , 'start' , 'end' , 'type'));
    }
}