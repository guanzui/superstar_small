<?php

namespace app\store\controller\finance;

use app\store\controller\Controller;
/**
 * 积分消费日志
 * Class Count
 * @package app\store\controller
 */
class Userintegral extends Controller
{
    /**
     * 余额消费记录
     */
    public function index() {
        $type = $this->request->param('type/d' , 0);
        $where = [];
        if( $type > 0 ) {
            $where['um.type'] = $type;
        }
		$newwhere = '';
		$name = $this->request->param("name/s" , '');
		if( $name ) {
			$newwhere = "u.nickname = '{$name}' OR u.user_id = '{$name}'";
		}
        $list = db("user_integral_log")->alias('um')->join("user u" , 'u.user_id = um.user_id')->where($newwhere)->order(['um.id' => 'DESC'])->where($where)->field("um.*,u.nickname")->paginate(15, false, ['query' => $this->request->request()]);
        return $this->fetch('', compact('list'));
    }
    
    /**
     * 删除
     */
    public function delete() {
        $id = $this->request->post('id' , 0);
        if( $id <= 0 ) $this->error('参数错误，请核对');
        $res = db('user_integral_log')->where(['id' => $id])->delete();
        if( $res ) {
            $this->success('删除成功');
        } else {
            $this->error("操作失败");
        }
    }
	
	public function export()
    {
		$type = $this->request->param('type/d' , 0);
        $where = [];
        if( $type > 0 ) {
            $where['um.type'] = $type;
        }
		$newwhere = '';
		$name = $this->request->param("name/s" , '');
		if( $name ) {
			$newwhere = "u.nickname = '{$name}' OR u.user_id = '{$name}'";
		}
        $list = db("user_integral_log")->alias('um')->join("user u" , 'u.user_id = um.user_id')->where($newwhere)->order(['um.id' => 'DESC'])->where($where)->field("um.*,u.nickname")->select()->toArray();
		$new_data = [];
		if( $list ){
			foreach( $list as $k => $v ) {
				$new_data[] = [
					'id' => $v['id'],
					'nickname' => $v['nickname'],
					'type' => $v['type'] == 1 ? '增加' : '减少',
					'num' => $v['num'],
					'content' => $v['content'],
					'create_time' => date("Y-m-d H:i:s" , $v['create_time'])
				];
			}
		}
		$field = ['ID' , '用户' , '类型' , '积分' , '详情' , '创建时间'];
		\app\common\extend\Excel::export($new_data , $field);
	}
}