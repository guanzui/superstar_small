<?php

namespace app\store\controller;

use app\store\model\Category;
use app\store\model\Delivery;
use app\store\model\Goods as GoodsModel;

/**
 * 商品管理控制器
 * Class Goods
 * @package app\store\controller
 */
class Goods extends Controller
{
    /**
     * 商品列表(出售中)
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $model = new GoodsModel;
		$where  = [];
		$title = $this->request->param('title/s' , '');
		if( $title ) {
			$where['goods_name'] = ['LIKE' , '%'.$title.'%'];
		}
		$goods_type = $this->request->param('goods_type/s' , '');
		if( $goods_type > 0 ) {
			$where['goods_type'] = $goods_type;
		}
        $list = $model->getList($status = null, $category_id = 0, $search = '', $sortType = 'all', $sortPrice = false , $where);
        return $this->fetch('index', compact('list'));
    }

    /**
     * 添加商品
     * @return array|mixed
     */
    public function add()
    {
        if (!$this->request->isAjax()) {
            // 商品分类
            $catgory = Category::getCacheTree();
            // 配送模板
            $delivery = Delivery::getAll();
            return $this->fetch('add', compact('catgory', 'delivery'));
        }
        $model = new GoodsModel;
        if ($model->add($this->postData('goods'))) {
            return $this->renderSuccess('添加成功', url('goods/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 删除商品
     * @param $goods_id
     * @return array
     * @throws \think\exception\DbException
     */
    public function delete($goods_id)
    {
		//删除商品主体
        db("goods")->where(['goods_id' => $goods_id])->delete();
        db("goods_spec")->where(['goods_id' => $goods_id])->delete();
		$data = db("goods_spec_rel")->where(['goods_id' => $goods_id])->select()->toArray();
		if( $data ) {
			$spec_id = [];
			$spec_value_id = [];
			foreach( $data as $k => $v ) {
				$spec_id[$v['spec_id']] = $v['spec_id'];
				$spec_value_id[$v['spec_value_id']] = $v['spec_value_id'];
			}
			//print_r( $data );
			//print_r( $spec_id );
			//print_r( $spec_value_id );
			db("spec")->where(['spec_id' => ['IN' , implode(',' , $spec_id)]])->delete();
			db("spec_value")->where(['spec_value_id' => ['IN' , implode(',' , $spec_value_id)]])->delete();
		}
        db("goods_spec_rel")->where(['goods_id' => $goods_id])->delete();
        db("goods_comment")->where(['goods_id' => $goods_id])->delete();
        db("goods_cart")->where(['goods_id' => $goods_id])->delete();
        db("goods_collection")->where(['goods_id' => $goods_id])->delete();
        return $this->renderSuccess('删除成功');
    }

    /**
     * 商品编辑
     * @param $goods_id
     * @return array|mixed
     * @throws \think\exception\DbException
     */
    public function edit($goods_id)
    {
        // 商品详情
        $model = GoodsModel::detail($goods_id);
        if (!$this->request->isAjax()) {
            // 商品分类
            $catgory = Category::getCacheTree();
            // 配送模板
            $delivery = Delivery::getAll();
            // 多规格信息
            $specData = 'null';
            if ($model['spec_type'] === 20)
                $specData = json_encode($model->getManySpecData($model['spec_rel'], $model['spec']));
            return $this->fetch('edit', compact('model', 'catgory', 'delivery', 'specData'));
        }
        // 更新记录
        if ($model->edit($this->postData('goods'))) {
            return $this->renderSuccess('更新成功', url('goods/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }
    
    public function infosave() {
        $goods_id = $this->request->param('goods_id/d' , 0);
        $field = $this->request->param('field');
        $value = $this->request->param('value/d' , 0);
        if( $goods_id <= 0 || $field == '' ) {
            $this->error('数据不完整，请核对');
        }
        $res = db("goods")->where(['goods_id' => $goods_id])->update([$field => $value]);
        if( $res ) {
            $this->success('操作成功');
        } else {
            $this->error('操作失败');
        }
    }

}
