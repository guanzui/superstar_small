<style>
    .layui-layer-content{ height: auto !important}
</style>
<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">修改用户</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">手机号 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <?=$user['mobile']?>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">登录密码 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="user[password]" value="">
                                    <div class="help-block am-margin-top-sm">
                                        <small>可不填写，需要修改密码则填写</small>
                                    </div>
                                </div>
                            </div>
							<div class="am-form-group select_user_info" data-name="user[parent_id]">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">上级用户 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" placeholder="请选择用户" value="<?php if( $user['parent_id'] > 0) { echo $user['user_info']['nickname']; }?>"  style="width:50%;display: inline-block;" readonly="">
                                    <button type="button" style="padding:7px 15px; border:0px;display: inline-block " class="am-btn-secondary"><i class="iconfont sidebar-nav-link-logo icon-account" style=""></i> 选择用户</button>
                                    <div class="uploader-list am-cf">
                                        <?php if( $user['parent_id'] > 0) :?>
                                        <div class="file-item">
                                            <img src="<?=$user['user_info']['avatar']?>">
                                            <input type="hidden" value="<?=$user['parent_id']?>" name="shop[parent_id]" />
                                            <i class="iconfont icon-shanchu file-item-user-delete"></i>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">昵称 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="user[nickname]" value="<?=$user['nickname']?>" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require">
                                    性别
                                </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="user[sex]" value="1" data-am-ucheck required <?php if( $user['sex'] == 1 ) echo "checked"; ?>>男
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="user[sex]" value="2" data-am-ucheck required <?php if( $user['sex'] == 2 ) echo "checked"; ?>>女
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">头像 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <button type="button" id="upload_logo" class="upload-file am-btn am-btn-secondary am-radius"><i class="am-icon-cloud-upload"></i> 请选择头像</button>
                                        <div class="uploader-list am-cf">
                                            <?php if( $user['avatar']) : ?>
                                                <div class="file-item">
                                                    <img src="<?=$user['avatar']?>">
                                                    <input type="hidden" name="shop[logo]"
                                                           value="<?= $user['avatar'] ?>">
                                                    <i class="iconfont icon-shanchu file-item-delete"></i>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="help-block am-margin-top-sm">
                                        <small>尺寸200x200像素以上，大小1M以下</small>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group am-form-select" id="area_select_div">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">地区 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline" style="padding-left: 0px;">
                                        <select name="user[province]" id="province" data-id="<?=$user['province']?>" style="width:180px;">
                                            <option value="1">请选择省</option>
                                        </select>
                                    </label>
                                    <label class="am-radio-inline" style="padding-left: 0px;">
                                        <select name="user[city]" id="city" data-id="<?=$user['city']?>" style="width:180px;">
                                            <option value="1">请选择城市</option>
                                        </select>
                                    </label>
                                    <label class="am-radio-inline" style="padding-left: 0px;">
                                        <select name="user[area]" id="area" data-id="<?=$user['area']?>" style="width:180px;">
                                            <option value="1">请选择区域</option>
                                        </select>
                                    </label>
                                </div>
                            </div>
							<div class="am-form-group am-form-select" id="area_select_div">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">用户等级 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline" style="padding-left: 0px;">
                                        <select name="user[identity]" style="width:280px;">
                                            <option value="0">无等级</option>
                                            <option value="10" <?php if( $user['identity'] == 10) echo "selected"; ?>>业务员</option>
                                            <option value="20" <?php if( $user['identity'] == 20) echo "selected"; ?>>销售经理</option>
                                            <option value="30" <?php if( $user['identity'] == 30) echo "selected"; ?>>总经理</option>
                                        </select>
                                    </label>
                                </div>
                            </div>
							<div class="am-form-group am-form-select" id="area_select_div">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">团队长 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline" style="padding-left: 0px;">
                                        <select name="user[is_item]" style="width:280px;">
                                            <option value="0" <?php if( $user['is_item'] == 0) echo "selected"; ?>>暂不开通</option>
                                            <option value="1" <?php if( $user['is_item'] == 1) echo "selected"; ?>>开通</option>
                                        </select>
                                    </label>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">换油次数 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="user[service_num]" value="<?php echo $user['service_num']; ?>">
                                    <div class="help-block am-margin-top-sm">
                                        <small>请谨慎修改</small>
                                    </div>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">积分资产 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="user[sales_achievement]" value="<?php echo $user['sales_achievement']; ?>">
                                    <div class="help-block am-margin-top-sm">
                                        <small>请谨慎修改</small>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require">
                                    用户状态
                                </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="user[status]" value="2" data-am-ucheck required <?php if($user['status'] == 2 ) echo "checked"; ?>>启用
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="user[status]" value="1" data-am-ucheck required <?php if($user['status'] == 1 ) echo "checked"; ?>>禁用
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <input type="hidden" value="<?=$user['user_id']?>" name="id" />
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- 图片文件列表模板 -->
{{include file="layouts/_template/tpl_file_item" /}}

<!-- 文件库弹窗 -->
{{include file="layouts/_template/file_library" /}}
<script src="assets/store/js/ddsort.js"></script>
<script>
    var json_area = <?php echo ( $area); ?>;
    $(function () {
        $('#upload_logo').selectImages({
            name: 'user[avatar]',
            type : 2
        });
        /**
         * 表单验证提交
         * @type {*}
         */
        $('#my-form').superForm();
    });
</script>