<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">车辆信息查看</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">所属用户 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock"><?=$info['nickname']?> / <a href="javascript:;" class="user_info_show" data-id="<?=$info['user_id']?>">查看</a></span> 
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">车主 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock"><?=$info['carowner']?></span> 
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">联系方式 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock"><?=$info['carowner_mobile']?></span> 
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">品牌/型号 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock"><?=$info['auto_brand_name']?>( <?=$info['auto_type_name']?> )</span> 
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">车架号 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock"><?=$info['frame_number']?></span> 
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">补油/换油次数 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock"><?=$info['maintain_total']?> 次 / <?=$info['replaceoil_total']?> 次</span> 
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">行驶公里数 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock"><?=$info['travel_mileage']?></span> 
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">上次预约时间 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock"><?=$info['last_yuyue_time']?></span> 
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">行驶证 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock" style="height:auto;"><a href="<?=$info['driving_license']?>" target="_blank" title="点击查看图片"><img src="<?=$info['driving_license']?>" width="100" /></a></span> 
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">创建时间 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock"><?=date('Y-m-d H:i:s' , $info['create_time'])?></span> 
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="button" onclick="history.go(-1)" class="j-submit am-btn am-btn-secondary">返回
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- 图片文件列表模板 -->
{{include file="layouts/_template/tpl_file_item" /}}

<!-- 文件库弹窗 -->
{{include file="layouts/_template/file_library" /}}

<script>
    $(function () {

        // 选择图片
        $('.upload-file').selectImages({
            name: 'brand[thumb]',type : 2
        });

        /**
         * 表单验证提交
         * @type {*}
         */
        $('#my-form').superForm();
    });
</script>
