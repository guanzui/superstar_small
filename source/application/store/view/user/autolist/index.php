<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">用户车辆管理</div>
                </div>
                <div class="widget-body am-fr">
					<form action="" style="height:50px;" class="am-form tpl-form-line-form submit_forms" enctype="multipart/form-data" method="get">
                    <div class="am-u-sm-12 am-u-md-2 am-u-lg-2"> </div>
					<div class="am-u-sm-12 am-u-md-10 am-u-lg-10" style="text-align: right;">
						<div class="am-btn-group am-btn-group-xs" >
							<input type="text" class="tpl-form-input" name="carowner" value="<?=input('param.carowner')?>" placeholder="产权人">
						</div>
						<div class="am-btn-group am-btn-group-xs" >
							<input type="text" class="tpl-form-input" name="carowner_mobile" value="<?=input('param.carowner_mobile')?>" placeholder="手机号">
						</div>
						<button type="submit" class="j-submit am-btn am-btn-secondary fontsize15rem">搜索</button>
					</div>
					</form>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>所属用户</th>
                                <th>产权人</th>
                                <th>联系电话</th>
                                <th>品牌/型号</th>
                                <th>驾驶公里数</th>
                                <th>上次预约时间</th>
                                <th>创建时间</th>
                                <th width="160">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['id'] ?></td>
                                    <td class="am-text-middle"><?= $item['nickname'] ?></td>
                                    <td class="am-text-middle"><?= $item['carowner'] ?></td>
                                    <td class="am-text-middle" ><?= $item['carowner_mobile']?></td>
                                    <td class="am-text-middle" ><?= $item['auto_brand_name']?>\<?= $item['auto_type_name']?></td>
                                    <td class="am-text-middle" ><?= $item['travel_mileage']?></td>
                                    <td class="am-text-middle" ><?= $item['last_yuyue_time']?></td>
                                    <td class="am-text-middle"><?php echo date("Y-m-d H:i:s" , $item['create_time']);?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <a href="<?= url('user.autolist/show',
                                                ['id' => $item['id']]) ?>">
                                                <i class="am-icon-pencil"></i> 查看
                                            </a>
                                            <a href="javascript:;" class="item-delete tpl-table-black-operation-del"
                                                data-id="<?= $item['id'] ?>">
                                                 <i class="am-icon-trash"></i> 删除
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="9" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        // 删除元素
        var url = "<?= url('user.autolist/delete') ?>";
        $('.item-delete').delete('id', url);

    });
</script>

