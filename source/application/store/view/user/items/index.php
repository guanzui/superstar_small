<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">团长申请审核</div>
                </div>
                <div class="widget-body am-fr">
					<form action="" style="height:50px;" class="am-form tpl-form-line-form submit_forms" enctype="multipart/form-data" method="get">
                    <div class="am-u-sm-12 am-u-md-2 am-u-lg-2"> </div>
					<div class="am-u-sm-12 am-u-md-10 am-u-lg-10" style="text-align: right;">
						<div class="am-btn-group am-btn-group-xs" >
							<input type="text" class="tpl-form-input" name="'name"" value="<?=input('param.name')?>" placeholder="申请人">
						</div>
						<div class="am-btn-group am-btn-group-xs" >
							<input type="text" class="tpl-form-input" name="idcard" value="<?=input('param.idcard')?>" placeholder="身份证号">
						</div>
						<div class="am-btn-group am-btn-group-xs" >
							<select name="status"  class="tpl-form-input">
								<option value="" >状态</option>
								<option value="0" <?php if( input('param.status') != '' && input('param.status') == 0 ) echo "selected" ?>>未审核</option>
								<option value="1" <?php if( input('param.status') == 1 ) echo "selected" ?>>审核未通过</option>
								<option value="2" <?php if( input('param.status') == 2 ) echo "selected" ?>>已通过</option>
							</select>
						</div>
						<button type="submit" class="j-submit am-btn am-btn-secondary fontsize15rem">搜索</button>
					</div>
					</form>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>用户</th>
                                <th>姓名</th>
                                <th>联系方式</th>
                                <th>身份证号</th>
                                <th>状态</th>
                                <th>创建时间</th>
                                <th width="160">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['id'] ?></td>
                                    <td class="am-text-middle"><?= $item['nickname'] ?> / <a href="javascript:;" class="user_info_show" data-id="<?=$item['user_id']?>">查看</a></td>
                                    <td class="am-text-middle" ><?= $item['name']?></td>
                                    <td class="am-text-middle"><?= $item['mobile'] ?></td>
                                    <td class="am-text-middle" ><?= $item['idcard']?></td>
                                    <td class="am-text-middle" ><?=$item['status'] == 0 ? '未审核' : ($item['status'] == 1 ? '未通过' : '已通过');?></td>
                                    <td class="am-text-middle"><?=$item['create_time']?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
											<?php if( $item['status'] != 2 ) : ?>
                                            <a href="javascript:;" data-id="<?=$item['id'];?>" class="chognzhi_but">
                                                <i class="am-icon-pencil"></i> 审核
                                            </a>
											<?php endif; ?>
                                            <a href="javascript:;" class="item-delete tpl-table-black-operation-del"
                                                data-id="<?= $item['id'] ?>">
                                                 <i class="am-icon-trash"></i> 删除
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="10" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
	.chongzhi_body{ padding:20px; width: 600px; height:230px;}
</style>
<script id="chongzhi_template">
<div class="chongzhi_body">
        <div class="am-form tpl-form-line-form">
            <div class="am-form-group">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label form-require">审核状态 </label>
                <div class="am-u-sm-6 am-u-end">
					<select id="status_data" class="tpl-form-input">
						<option value="">请选择状态</option>
						<option value="1">驳回</option>
						<option value="2">审核通过</option>
					</select>
				</div>
            </div>
            <div class="am-form-group">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label">操作说明 </label>
                <div class="am-u-sm-9 am-u-end">
                    <textarea class="tpl-form-input  am-active" id="note_data" required="" placeholder="驳回申请必填"></textarea>
                </div>
            </div>
            <input type="hidden" value="1" name="id">
            <div class="am-form-group">
                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                    <button type="button" class="but-submit-data am-btn am-btn-secondary"  data-id="{{id}}">确认审核</button>
                </div>
            </div>
        </div>
</div>
</script>
<script>
	$("a.chognzhi_but").click(function(){
		var id = $(this).data("id");
		layer.open({
			type: 1,
			title : '团长资质审核',
			skin: 'layui-layer-rim',
			area: ['650px', '290px'],
			content: $("#chongzhi_template").html().replace("{{id}}" , id)
		});
	});
	$(document).on( "click" , ".but-submit-data" , function(){
		var status = $("#status_data").val();
		var note = $("#note_data").val();
		if( status == '' ) {
			layer.msg("请选择审核状态");
			return false;
		}
		if( note == '' && status == 1 ) {
			layer.msg("请输入驳回的原因");
			return false;
		}
		layer.msg('正在请求', {
			icon:16,
			shade:[0.1, '#fff'],
			time:false
		});
		$.post("<?=url('store/user.items/infosave');?>" , { id : $(this).data("id") , status : status , note : note } , function(data){
			if( data.code == 1 ) {
				layer.alert(data.msg , function(){
					window.location.reload();
				});
			} else {
				layer.msg(data.msg);
			}
		},'json');
	});
</script>

<script>
    $(function () {
        // 删除元素
        var url = "<?= url('user.items/delete') ?>";
        $('.item-delete').delete('id', url);

    });
</script>

