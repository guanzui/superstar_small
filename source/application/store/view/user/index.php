<style>
    .chongzhi_body{ padding:20px; width: 600px; height:300px;}
	.jiyouliebiao{ margin:20px; height:300px; overflow-y:scroll}
</style>
<script src="assets/layer/laydate/laydate.js"></script>
<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">用户列表</div>
                </div>
                <div class="widget-body am-fr">
					<form action="<?=url("store/user/index");?>" style="height:50px;" class="am-form tpl-form-line-form submit_forms" enctype="multipart/form-data" method="get">
                    <div class="am-u-sm-12 am-u-md-2 am-u-lg-2">
                        <div class="am-form-group">
                            <div class="am-btn-toolbar">
                                <div class="am-btn-group am-btn-group-xs">
                                    <a class="am-btn am-btn-default am-btn-success am-radius"
                                       href="<?= url('user/add') ?>">
                                        <span class="am-icon-plus"></span> 新增
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="am-u-sm-12 am-u-md-10 am-u-lg-10" style="text-align: right;">
						<div class="am-btn-group am-btn-group-xs" style="width:120px;">
							<input type="text" class="tpl-form-input" name="string" value="<?=input('param.string')?>" placeholder="姓名/手机/卡号">
						</div>
                      	<div class="am-btn-group am-btn-group-xs" style="width:180px;">
							<input type="text" class="tpl-form-input" name="datetime" id="datetime" value="<?=input('param.datetime')?>" placeholder="日期筛选">
						</div>
						<!--<div class="am-btn-group am-btn-group-xs" style="width:120px;">
							<input type="text" class="tpl-form-input" name="cart_sn" value="<?=input('param.cart_sn')?>" placeholder="会员卡号">
						</div>-->
						<div class="am-btn-group am-btn-group-xs" >
							<select name="is_cart"  class="tpl-form-input">
								<option value="">是否开卡</option>
								<option value="2" <?php if( input('param.is_cart') == 2 ) echo "selected" ?>>已开卡</option>
								<option value="1" <?php if( input('param.is_cart') == 1 ) echo "selected" ?>>未开卡</option>
							</select>
						</div>
						<div class="am-btn-group am-btn-group-xs" style="width:40%;" id="area_select_div">
							<select name="province" id="province" data-id="<?=input('param.province')?>" style="width:calc(33.3% - 10px); float:left; margin-right:10px;">
								<option value="">请选择省</option>
							</select>
							<select name="city" id="city" data-id="<?=input('param.city')?>" style="width:calc(33.3% - 10px); float:left; margin-right:10px;">
								<option value="">请选择城市</option>
							</select>
							<select name="area" id="area" data-id="<?=input('param.area')?>" style="width:calc(33.3% - 10px); float:left;">
								<option value="">请选择区域</option>
							</select>
						</div>
						<button type="submit" class="j-submit am-btn am-btn-secondary fontsize15rem">搜索</button>
						<button type="button" class="export_submit am-btn am-btn-secondary fontsize15rem">导出</button>
					</div>
					</form>
					<style>
					.user_data_count{ width:100%; height:120px; margin-bottom:20px;}
					.user_data_count_item{ width:calc(20% - 10px); height:120px; float:left;margin-right:10px; border:1px solid #efefef; border-top:3px solid #ccc}
					.user_data_count_item p{ margin:0px; text-align:center; font-size:14px;}
					.user_data_count_item p.item_ti{ margin:25px 0 5px 0; font-size:20px; font-weight:bold}
					.tanchukuang{ width:200px; height:80px; position:absolute; background:#fff;border:1px solid #ccc; padding:10px; margin:-50px 0px 0px 60px;display:none;}
					.tanchukuang div{ padding:5px 0px}
					.tanchukuang_hover{ cursor:pointer}
					.tanchukuang_hover:hover .tanchukuang{ display:block }
					</style>
					<div class="user_data_count">
						<div class="user_data_count_item">
							<p class="item_ti"><?=$data['open_man_num']?>人</p>
							<p>开卡人数</p>
						</div>
						<div class="user_data_count_item">
							<p class="item_ti">￥<?=$data['open_man_price']?></p>
							<p>销售金额</p>
						</div>
						<div class="user_data_count_item">
							<p class="item_ti"><?=$data['open_rest_man_num']?>人</p>
							<p>复购人数</p>
						</div>
						<div class="user_data_count_item">
							<p class="item_ti">￥<?=$data['open_rest_man_price']?></p>
							<p>复购金额</p>
						</div>
						<div class="user_data_count_item">
							<p class="item_ti">￥<?=$data['total_price']?></p>
							<p>总提成金额</p>
						</div>
					</div>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>用户ID</th>
                                <th>手机号</th>
                                <th>头像</th>
                                <th>昵称</th>
                                <th>性别</th>
                                <th>团队长</th>
                                <th>会员卡</th>
                                <th>开卡时间</th>
                                <th>地区</th>
                                <th>等级</th>
                                <th>上级</th>
                                <th>推荐码</th>
                                <th>积分资产</th>
                                <th>状态</th>
                                <th>注册时间</th>
                                <th width="150">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['user_id'] ?></td>
                                    <td class="am-text-middle"><?= $item['mobile'] ?></td>
                                    <td class="am-text-middle">
                                        <a href="<?= $item['avatar'] ?>" title="点击查看大图" target="_blank">
                                            <img src="<?= $item['avatar'] ?>" width="40" height="40" alt="">
                                        </a>
                                    </td>
                                    <td class="am-text-middle"><?= $item['nickname'] ?></td>
                                    <td class="am-text-middle"><?= $item['sex'] ==1 ? '男' : '女'?></td>
                                    <td class="am-text-middle"><?= $item['is_item'] == 1 ? '已开通' : '未开通'?></td>
                                    <td class="am-text-middle tanchukuang_hover"><?= $item['is_open_cart'] == 1 ? '已开通' : '未开通'?><div class="tanchukuang"><div>剩余换油次数：<?php echo $item['service_num']?></div><div>已服务换油次数：<?php echo $item['baoyang_num']?></div></div></td>
                                    <td class="am-text-middle"><?= $item['open_cart_time'] > 0 ? date("Y-m-d H:i" , $item['open_cart_time']) : '--' ?></td>
									<td class="am-text-middle"><?php echo $item['province_name'] .'-'.$item['city_name'].'-'.$item['area_name'];?></td>
                                    <td class="am-text-middle"><?php echo $item['identity']==10?"业务员":($item['identity']==20?"销售经理":($item['identity']==30?"总经理":"普通会员"));?></td>
                                    <td class="am-text-middle">
										<?php if( $item['parent_nickname'] ) :?>
										<?php echo $item['parent_nickname']; ?><br>电话：<?php echo $item['parent_mobile']; ?>
										<?php else : ?>
										--
										<?php endif; ?>
									</td>
									<td class="am-text-middle"><?= $item['share_code']?$item['share_code']:'******' ?></td>
									<td class="am-text-middle"><?= $item['sales_achievement'] ?></td>
                                    <td class="am-text-middle"><a href="<?php echo url("user/savestatus" , ['user_id' => $item['user_id'],'status' => $item['status']==1?2:1])?>" menu="ajax_request" data-id="<?=$item['user_id']?>" style="color:<?php if( $item['status'] == 2 ) :?>green<?php else: ?>red<?php endif;?>"><?= $item['status'] == 2 ? "正常" : '禁用' ?></a></td>
                                    
									<td class="am-text-middle"><?= $item['create_time']?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <div style="margin-bottom:10px;">
											<a href="<?=url("store/finance.commission/index" , array("user_id" => $item['user_id']));?>" class="logList_data" data-name="<?=$item['nickname']?>" data-id="<?=$item['user_id']?>">
                                                <i class="iconfont sidebar-nav-link-logo icon-stock " style=""></i> 提成记录
                                            </a>
                                            <a href="<?= url('user/savepass' , array('user_id' => $item['user_id'])) ?>" title="密码将会被重置为【888888】，确定要操作？" class="item-ajax-request">
                                                <i class="iconfont sidebar-nav-link-logo icon-redo" style=""></i> 密码重置
                                            </a>
											</div>
                                            <div style="margin-bottom:10px;">
											<a href="<?= url('user/itemshow' , array('user_id' => $item['user_id'])) ?>" data-nickname="<?=$item['nickname']?>" title="查看团队" class="item-show-list">
                                                查看团队
                                            </a>
											<a href="<?= url('store/user.autolist/index' , array('user_id' => $item['user_id'])) ?>">
                                                用户车辆
                                            </a>
											</div>
                                            <a href="<?= url('user/edit' , array('id' => $item['user_id'])) ?>">
                                                <i class="am-icon-pencil"></i> 编辑
                                            </a>
                                            <a href="javascript:;" class="item-delete tpl-table-black-operation-del" data-id="<?=$item['user_id']?>">
                                                <i class="am-icon-trash"></i> 删除
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="9" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--
<script id="chongzhi_template">
<div class="chongzhi_body">
        <div class="am-form tpl-form-line-form">
            <div class="am-form-group">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label ">{{name}} </label>
                <div class="am-u-sm-9 am-u-end">
                    <span style="height:3rem; line-height:3rem; color:red; display: inline-block;font-size: 1.8rem;">{{value}}</span>
                </div>
            </div>
            <div class="am-form-group">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label form-require">充值数量 </label>
                <div class="am-u-sm-6 am-u-end">
                    <input type="text" class="tpl-form-input" id="num" value="" required="" pattern="^-?(?:\d+|\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$">
                </div>
            </div>
            <div class="am-form-group">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label">备注 </label>
                <div class="am-u-sm-9 am-u-end">
                    <textarea class="tpl-form-input  am-active" id="note" required="" placeholder="请输入此次操作的原因"></textarea>
                </div>
            </div>
            <input type="hidden" value="1" name="id">
            <div class="am-form-group">
                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                    <button type="button" class="but-submit-data am-btn am-btn-secondary " data-type="{{type}}" data-id="{{id}}">确定充值</button>
                </div>
            </div>
        </div>
</div>
</script>-->
<script>
	var json_area = <?php echo ( $area); ?>;
    $(function () {
        $('.item-ajax-request').request_ajax();
        /*$("a.chognzhi_but").click(function(){
            var value = $(this).data("value");
            var type = $(this).data("type");
            var id = $(this).data("id");
            layer.open({
                type: 1,
                title : $(this).attr("title"),
                skin: 'layui-layer-rim',
                area: ['650px', '380px'],
                content: $("#chongzhi_template").html().replace("{{name}}" , (type == 1 ? '当前余额' : '当前积分')).replace("{{id}}" , id).replace("{{type}}" , type).replace("{{value}}" , ( type == 1 ? "￥" : '' ) + value)
            });
        })
        $(document).on( 'click',".but-submit-data"  , function(){
            var type = $(this).data("type");
            var id = $(this).data("id");
            var num = $("#num").val();
            var note = $("#note").val();
            if( num == '' ) {
                layer.msg("请输入需要充值的数量");
                return false;
            }
            $.post('<?=url('user/recharge');?>' , { id :id , num :num , type:type , note:note} , function(data){
                if( data.code == 1 ) {
                    layer.alert(data.msg , function(){
                        window.location.reload();
                    })
                } else {
                    layer.msg(data.msg);
                }
            },'json');
        })*/
		
		// 删除元素
		var url = "<?= url('user/delete') ?>";
		$('.item-delete').delete('id', url);
    });
	laydate.render({
	  elem: '#datetime',
	  type: 'date',
	  range: '~'
	});
</script>
<script>
$(".export_submit").click(function(){
	var url = decodeURIComponent(window.location.href).split("?s=");
	var url_uri = url[1].substr(1).split('/');
	var url_data = $('form').serialize().split("&");
	$.each(url_data , function(k,v){
		var uri_data = v.split("=");
		var floag = false;
		$.each(url_uri , function(m,n){
			if( m > 2 && m % 2 == 1 && uri_data[0] == n ) {
				floag = true;
				url_uri[m + 1] = uri_data[1];
			}
		});
		if( floag === false ) {
			url_uri.push( uri_data[0] + '/' + uri_data[1] );
		}
		
	});
	location.href = url[0] + '?s=' + '/'+url_uri.join("/").replace("<?php echo request()->action();?>" , 'export') + '/order_type_data/<?php echo request()->action();?>';
	return false;
});

$('a.item-show-list').click(function(){
	layer.open({
      type: 2,
      title: '用户【'+$(this).data('nickname')+'】团队查看',
      shadeClose: true,
      shade: [0.5,'#000'],
      maxmin: false,
      area: ['1000px', '620px'],
      content: $(this).attr("href")
    });
	return false;
})
</script>
