<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf"><?= $title ?></div>
                </div>
                <div class="widget-body am-fr">
					<form action="<?=url("store/goods.comment/index");?>" class="am-form tpl-form-line-form submit_forms" enctype="multipart/form-data" method="post">
                    <div class="am-u-sm-12 am-u-md-1 am-u-lg-1"></div>
					<div class="am-u-sm-12 am-u-md-11 am-u-lg-11" style="text-align: right;">
						<div class="am-btn-group am-btn-group-xs">
							<select name="pay_type"  data-am-selected="{searchBox: 1, btnSize: 'sm',  placeholder:'请选择类型'}">
								<option value="0">支付方式</option>
								<option value="1" <?php if( input('param.pay_type') == 1 ) echo "selected" ?>>微信</option>
								<option value="2" <?php if( input('param.pay_type') == 2 ) echo "selected" ?>>支付宝</option>
								<option value="3" <?php if( input('param.pay_type') == 3 ) echo "selected" ?>>余额</option>
							</select>
						</div>
						<div class="am-btn-group am-btn-group-xs">
							<select name="type"  data-am-selected="{searchBox: 1, btnSize: 'sm',  placeholder:'请选择类型'}">
								<option value="0">订单类型</option>
								<option value="1" <?php if( input('param.type') == 1 ) echo "selected" ?>>商城订单</option>
								<option value="2" <?php if( input('param.type') == 2 ) echo "selected" ?>>兑换订单</option>
							</select>
						</div>
						<div class="am-btn-group am-btn-group-xs">
							<input type="text" class="tpl-form-input" name="order_no" value="<?=input('param.order_no')?>" placeholder="订单编号">
						</div>
						<button type="submit" class="j-submit am-btn am-btn-secondary fontsize15rem">搜索</button>
						<button type="button" class="am-btn export_submit am-btn-secondary fontsize15rem">导出</button>
					</div>
					</form>
                    <div class="order-list am-scrollable-horizontal am-u-sm-12 am-margin-top-xs">
                        <table width="100%" class="am-table am-table-centered
                        am-text-nowrap am-margin-bottom-xs">
                            <thead>
                            <tr>
                                <th width="30%" class="goods-detail">商品信息</th>
                                <th width="10%">单价/数量</th>
                                <th width="15%">实付款</th>
                                <th>买家</th>
                                <th>交易状态</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $order): ?>
                                <tr class="order-empty">
                                    <td colspan="6"></td>
                                </tr>
                                <tr>
                                    <td class="am-text-middle am-text-left" colspan="6">
                                        <span class="am-margin-right-lg"> <?= $order['create_time'] ?></span>
                                        <span class="am-margin-right-lg">订单号：<?= $order['order_no'] ?></span>
                                    </td>
                                </tr>
                                <?php $i = 0;
                                foreach ($order['goods'] as $goods): $i++; ?>
                                    <tr>
                                        <td class="goods-detail am-text-middle">
                                            <div class="goods-image">
                                                <img src="<?= $goods['thumb'] ?>" alt="">
                                            </div>
                                            <div class="goods-info">
                                                <p class="goods-title"><?= $goods['goods_name'] ?></p>
                                                <p class="goods-spec am-link-muted">
                                                    <?= $goods['goods_attr'] ?>
                                                </p>
                                            </div>
                                        </td>
                                        <td class="am-text-middle">
                                            <p>￥<?= $goods['goods_price'] ?></p>
                                            <p>×<?= $goods['total_num'] ?></p>
                                        </td>
                                        <?php if ($i === 1) : $goodsCount = count($order['goods']); ?>
                                            <td class="am-text-middle" rowspan="<?= $goodsCount ?>">
                                                <p>￥<?= $order['pay_price'] ?></p>
                                                <p class="am-link-muted">(含运费：￥<?= $order['express_price'] ?>)</p>
                                            </td>
                                            <td class="am-text-middle" rowspan="<?= $goodsCount ?>">
                                                <p><?= $order['user']['nickname'] ?></p>
                                                <p class="am-link-muted">(用户id：<?= $order['user']['user_id'] ?>)</p>
                                            </td>
                                            <td class="am-text-middle" rowspan="<?= $goodsCount ?>">
                                                <p>付款状态：
                                                    <span class="am-badge
                                                <?= $order['pay_status']['value'] === 20 ? 'am-badge-success' : '' ?>">
                                                        <?= $order['pay_status']['text'] ?></span>
                                                </p>
                                                <p>发货状态：
                                                    <span class="am-badge
                                                <?= $order['delivery_status']['value'] === 20 ? 'am-badge-success' : '' ?>">
                                                        <?= $order['delivery_status']['text'] ?></span>
                                                </p>
                                                <p>收货状态：
                                                    <span class="am-badge
                                                <?= $order['receipt_status']['value'] === 20 ? 'am-badge-success' : '' ?>">
                                                        <?= $order['receipt_status']['text'] ?></span>
                                                </p>
                                            </td>
                                            <td class="am-text-middle" rowspan="<?= $goodsCount ?>">
                                                <div class="tpl-table-black-operation">
                                                    <a class="tpl-table-black-operation-green"
                                                       href="<?= url('order/detail', ['order_id' => $order['order_id']]) ?>">
                                                        订单详情</a>
                                                    <?php if ($order['pay_status']['value'] === 20
                                                        && $order['delivery_status']['value'] === 10): ?>
                                                        <a class="tpl-table-black-operation"
                                                           href="<?= url('order/detail#delivery',
                                                               ['order_id' => $order['order_id']]) ?>">
                                                            去发货</a>
                                                    <?php endif; ?>
                                                </div>
                                            </td>
                                        <?php endif; ?>
                                    </tr>
									<?php if( $order['note'] ) :?>
									<tr>
										<td class="am-text-middle am-text-left" colspan="6">
											<span class="am-margin-right-lg">买家备注：<?= $order['note'] ?></span>
										</td>
									</tr>
									<?php endif;?>
                                <?php endforeach; ?>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="6" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(".export_submit").click(function(){
	var url = decodeURIComponent(window.location.href).split("?s=");
	var url_uri = url[1].substr(1).split('/');
	var url_data = $('form').serialize().split("&");
	$.each(url_data , function(k,v){
		var uri_data = v.split("=");
		var floag = false;
		$.each(url_uri , function(m,n){
			if( m > 2 && m % 2 == 1 && uri_data[0] == n ) {
				floag = true;
				url_uri[m + 1] = uri_data[1];
			}
		});
		if( floag === false ) {
			url_uri.push( uri_data[0] + '/' + uri_data[1] );
		}
		
	});
	location.href = url[0] + '?s=' + '/'+url_uri.join("/").replace("<?php echo request()->action();?>" , 'export') + '/order_type_data/<?php echo request()->action();?>';
	return false;
})
</script>


