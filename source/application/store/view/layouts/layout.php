<?php if( isset( $item_show ) && $item_show == 1 ) : ?>
{__CONTENT__}
<?php else : ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title><?= $setting['store']['values']['name'] ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="renderer" content="webkit"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="icon" type="image/png" href="assets/store/i/favicon.ico"/>
    <meta name="apple-mobile-web-app-title" content="<?= $setting['store']['values']['name'] ?>"/>
    <link rel="stylesheet" href="assets/store/css/amazeui.min.css"/>
    <link rel="stylesheet" href="assets/store/css/app.css"/>
    <link rel="stylesheet" href="//at.alicdn.com/t/font_783249_t6knt0guzo.css">
    <script src="assets/store/js/jquery.min.js"></script>
    <script src="//at.alicdn.com/t/font_783249_e5yrsf08rap.js"></script>
    <script>
        BASE_URL = '<?= isset($base_url) ? $base_url : '' ?>';
        STORE_URL = '<?= isset($store_url) ? $store_url : '' ?>';
    </script>
</head>

<body data-type="">
<div class="am-g tpl-g">
    <?php if( !isset( $template_type ) || $template_type != 'tips') : ?>
    <!-- 头部 -->
    <header class="tpl-header">
        <!-- 右侧内容 -->
        <div class="tpl-header-fluid">
            <!-- 侧边切换 -->
            <div class="am-fl tpl-header-button switch-button">
                <i class="iconfont icon-menufold"></i>
            </div>
            <!-- 刷新页面 -->
            <div class="am-fl tpl-header-button refresh-button">
                <i class="iconfont icon-refresh"></i>
            </div>
            <!-- 其它功能-->
            <div class="am-fr tpl-header-navbar">
                <ul>
                    <!-- 欢迎语 -->
                    <li class="am-text-sm tpl-header-navbar-welcome">
                        <a href="<?= url('store.user/renew') ?>">欢迎你，<span><?= $store['user']['user_name'] ?></span>
                        </a>
                    </li>
                    <!-- 退出 -->
                    <li class="am-text-sm">
                        <a href="<?= url('passport/logout') ?>">
                            <i class="iconfont icon-tuichu"></i> 退出
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <!-- 侧边导航栏 -->
    <div class="left-sidebar">
        <?php $menus = $menus ?: []; ?>
        <?php $group = $group ?: 0; ?>
        <!-- 一级菜单 -->
        <ul class="sidebar-nav">
            <li class="sidebar-nav-heading"><?= $setting['store']['values']['name'] ?></li>
            <?php foreach ($menus as $key => $item): ?>
                <li class="sidebar-nav-link">
                    <a href="<?= isset($item['index']) ? url($item['index']) : 'javascript:void(0);' ?>"
                       class="<?= $item['active'] ? 'active' : '' ?>">
                        <?php if (isset($item['is_svg']) && $item['is_svg'] === true): ?>
                            <svg class="icon sidebar-nav-link-logo" aria-hidden="true">
                                <use xlink:href="#<?= $item['icon'] ?>"></use>
                            </svg>
                        <?php else: ?>
                            <i class="iconfont sidebar-nav-link-logo <?= $item['icon'] ?>"
                               style="<?= isset($item['color']) ? "color:{$item['color']};" : '' ?>"></i>
                        <?php endif; ?>
                        <?= $item['name'] ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
        <!-- 子级菜单-->
        <?php $second = isset($menus[$group]['submenu']) ? $menus[$group]['submenu'] : []; ?>
        <?php if (!empty($second)) : ?>
            <ul class="left-sidebar-second">
                <li class="sidebar-second-title"><?= $menus[$group]['name'] ?></li>
                <li class="sidebar-second-item">
                    <?php foreach ($second as $item) : ?>
                        <?php if (!isset($item['submenu'])): ?>
                            <!-- 二级菜单-->
                            <a href="<?= url($item['index']) ?>" class="<?= $item['active'] ? 'active' : '' ?>">
                                <?= $item['name']; ?>
                            </a>
                        <?php else: ?>
                            <!-- 三级菜单-->
                            <div class="sidebar-third-item">
                                <a href="javascript:void(0);"
                                   class="sidebar-nav-sub-title <?= $item['active'] ? 'active' : '' ?>">
                                    <i class="iconfont icon-caret"></i>
                                    <?= $item['name']; ?>
                                </a>
                                <ul class="sidebar-third-nav-sub">
                                    <?php foreach ($item['submenu'] as $third) : ?>
                                        <li>
                                            <a class="<?= $third['active'] ? 'active' : '' ?>"
                                               href="<?= url($third['index']) ?>">
                                                <?= $third['name']; ?></a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </li>
            </ul>
        <?php endif; ?>
    </div>
    <?php endif; ?>
    <!-- 内容区域 start -->
    <div class="tpl-content-wrapper" <?php if( isset( $template_type ) && $template_type == 'tips') : ?> style="margin-top:0px;" <?php endif; ?>>
        {__CONTENT__}
    </div>
    <!-- 内容区域 end -->

</div>
<script src="assets/layer/layer.js"></script>
<script src="assets/store/js/jquery.form.min.js"></script>
<script src="assets/store/js/amazeui.min.js"></script>
<script src="assets/store/js/webuploader.html5only.js"></script>
<script src="assets/store/js/art-template.js"></script>
<script src="assets/store/js/app.js"></script>
<script src="assets/store/js/file.library.js"></script>
<script>
    //ajax异步请求
    $("a[menu='ajax_request']").click(function(){
        var _self = $(this);
        var confirm_index = layer.confirm('确定要操作吗' , function(){
            layer.close(confirm_index);
            var loadingIndex = layer.load(2, { 
                shade: [0.5, 'gray'],
                content: '加载中...',
                success: function (layero) {
                    layero.find('.layui-layer-content').css({
                        'padding-top': '39px',
                        'width': '60px'
                    });
                }
            });
            $.get(_self.attr("href") ,function(data){
                layer.close(loadingIndex);
                if( data.code == 1 ) {
                    window.location.reload();
                }else {
                    layer.alert(data.msg);
                }
            },'json');
        });
        return false;
    });    
    //三级联动菜单
    $('[id="area_select_div"]').each(function(){
        var _self = $(this);
        var province_html = '<option value="">请选择省</option>',city_html = '<option value="">请选择城市</option>',area_html = '<option value="">请选择区域</option>';
        var province_id = parseInt(_self.find("#province").data('id')),city_id = parseInt(_self.find("#city").data('id')),area_id = parseInt(_self.find("#area").data('id'));
        var province_obj = new Array(),city_obj = new Array();
        $.each(json_area , function(k,v){
            if( v.pid == 0 ) {
                province_html += '<option value="'+v.id+'" '+(parseInt(province_id)==v.id?'selected':'')+'>'+v.name+'</option>';
            }
        });
        _self.find("#province").html(province_html);
        if( city_id > 0 && province_id > 0) {
            $.each(json_area , function(k,v){
                if( v.id == province_id ) {
                    $.each(v.city,function(k,m){
                        city_obj.push(m);
                        city_html += '<option value="'+m.id+'" '+(parseInt(city_id)==m.id?'selected':'')+'>'+m.name+'</option>';
                    });
                }
            });
            _self.find("#city").html(city_html);
        }
        if( area_id > 0 && city_id > 0) {
            $.each(city_obj , function(k,v){
                if( v.id == city_id ) {
                     $.each(v.region,function(k,m){
                        area_html += '<option value="'+m.id+'" '+(parseInt(area_id)==m.id?'selected':'')+'>'+m.name+'</option>';
                    });
                }
            });
            _self.find("#area").html(area_html);
        }
        _self.find("#province").change(function(){
            var p_id = $(this).val();
            city_obj = new Array();
            city_html = '<option value="">请选择城市</option>';
            area_html = '<option value="">请选择区域</option>';
            _self.find("#city").html(city_html);
            _self.find("#area").html(area_html);
             $.each(json_area , function(k,v){
                if( v.id == parseInt(p_id) ) {
                    $.each(v.city,function(k,m){
                        city_obj.push(m);
                        city_html += '<option value="'+m.id+'">'+m.name+'</option>';
                    });
                }
            });
            _self.find("#city").html(city_html);
        });
        _self.find("#city").change(function(){
            var c_id = $(this).val();
            area_html = '<option value="">请选择区域</option>';
            _self.find("#area").html(area_html);
             $.each(city_obj , function(k,v){
                if( v.id == c_id ) {
                     $.each(v.region,function(k,m){
                        area_html += '<option value="'+m.id+'">'+m.name+'</option>';
                    });
                }
            });
            _self.find("#area").html(area_html);
        });
    });
</script>

<!-- ----------------------------    用户选择   ------------------------------------->
<script id="user_select_append_tempalte">
    <dl>
        <dt><img src="{{avatar}}"></dt>
        <dd><span>{{nickname}}</span><span>{{mobile}}</span></dd>
        <dd><a href="javascript:void(0)" data-id="{{user_id}}" class="am-btn-secondary user_select_gx">选择</a></dd>
    </dl>
</script>
<script id="user_select_template">
<div class="user_select_data">
    <div class="row-content am-cf">
        <div class="row">
            <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
                <div class="widget-body am-form tpl-form-line-form">
                    <div class="am-form-group">
                        <label class="am-u-sm-4 am-u-lg-2 am-form-label form-require">用户筛选 </label>
                        <div class="am-u-sm-7 am-u-end">
                            <input type="text" class="tpl-form-input" id="user_select_key" value="">
                        </div>
                        <div class="am-u-sm-3 am-u-end"><button type="button" style="padding:7px 15px; border:0px;display: inline-block; font-size:14px;" class="user_select_but am-btn-secondary">确认检索</button></div>
                    </div>
                    <div class="am-form-group">
                        <div class="user_select_loading">数据检索中...</div>
                        <div class="user_select_back_data"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</script>
<script>
$('div.select_user_info').each(function(){
    var _self = $(this);
    //删除用户信息
    _self.find('i.file-item-user-delete').bind('click' , function(){
        $(this).closest('.file-item').remove();
        _self.find("input[type='text']").val('');
    });
    _self.find('button').bind('click' , function(){
        var htmls = $("#user_select_template").html();
        layer.open({
            title: "用户筛选",
            type: 1,offset: 'top',
            area: ['auto', 'auto'],
            shadeClose: true,
            content: htmls
        });
        //查询用户
        $(".user_select_but").bind('click' , function(){
            var key = $('#user_select_key').val();
            if( key == '' ) {
                layer.msg("请输入关键字");
                return false;
            }
            $(".user_select_loading").show();
            $(".user_select_back_data").hide();
            var template_htmls = '';
            $.post('<?php echo url("store/index/memberList"); ?>' , { key : key } , function(data){
                if( data.code == 0 ) {
                    $(".user_select_loading").html(data.msg);
                } else {
                    $(".user_select_loading").hide();
                    $.each(data.data , function(key,value){
                        var list_template = $("#user_select_append_tempalte").html();
                        $.each(value,function(k,v){
                            list_template = list_template.replace("{{"+k+"}}" , v);
                        });
                        template_htmls += list_template;
                    });
                    $(".user_select_back_data").html(template_htmls);
                    $(".user_select_back_data").show();
                    //选择用户
                    $(".user_select_gx").bind('click' , function(){
                        var nickname = $(this).closest("dl").find("span:first").html();
                        var user_id = $(this).data('id');
                        var avatar = $(this).closest("dl").find("img").attr('src');
                        _self.find("input[type='text']").val(nickname);
                        _self.find('.uploader-list').html('<div class="file-item"><img src="'+avatar+'"><input type="hidden" value="'+user_id+'" name="'+_self.attr("data-name")+'" /><i class="iconfont icon-shanchu file-item-user-delete"></i></div>');
                        layer.closeAll();
                    });
                }
            },'json');
        });
    });
});
</script>

<!------------------------       会员信息查看       -------------------------->
<script id="user_info_template">
<div style="padding:20px; width: 600px; height:330px;">
        <div class="am-form tpl-form-line-form">
            <div class="am-form-group" style="margin-bottom:.3rem">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label">头像 </label>
                <div class="am-u-sm-6 am-u-end">
					<div style="width:60px; height:60px; overflow:hidden;border-radius:60px; border:1px solid #ccc;"><img src="{{avatar}}" style="width:60px; height:60px;"></div>
				</div>
            </div>
            <div class="am-form-group" style="margin-bottom:.3rem">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label form-require">用户昵称 </label>
                <div class="am-u-sm-6 am-u-end">
					<span style="height:3rem; line-height:3.4rem; color:red; display: inline-block;font-size: 1.2rem;">{{nickname}}</span>
				</div>
            </div>
            <div class="am-form-group" style="margin-bottom:.3rem">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label">手机号 </label>
                <div class="am-u-sm-6 am-u-end">
					<span style="height:3rem; line-height:3.4rem; color:red; display: inline-block;font-size: 1.2rem;">{{mobile}}</span>
				</div>
            </div>
            <div class="am-form-group" style="margin-bottom:.3rem">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label">性别 </label>
                <div class="am-u-sm-6 am-u-end">
					<span style="height:3rem; line-height:3.4rem; color:red; display: inline-block;font-size: 1.2rem;">{{sex}}</span>
				</div>
            </div>
            <div class="am-form-group" style="margin-bottom:.3rem">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label">地址 </label>
                <div class="am-u-sm-6 am-u-end">
					<span style="height:3rem; line-height:3.4rem; color:red; display: inline-block;font-size: 1.2rem;">{{address}}</span>
				</div>
            </div>
            <div class="am-form-group" style="margin-bottom:.3rem">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label">余额/积分 </label>
                <div class="am-u-sm-6 am-u-end">
					<span style="height:3rem; line-height:3.4rem; color:red; display: inline-block;font-size: 1.2rem;">{{money}} 元 / {{integral}} 分</span>
				</div>
            </div>
            <div class="am-form-group" style="margin-bottom:.3rem">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label">登录信息 </label>
                <div class="am-u-sm-6 am-u-end">
					<span style="height:3rem; line-height:3.4rem; color:red; display: inline-block;font-size: 1.2rem;">{{login_ip}} / {{login_time}}</span>
				</div>
            </div>
        </div>
</div>
</script>
<script>
	$("a.user_info_show").click(function(){
		var id = $(this).data("id");
		$.get("<?=url("store/index/userinfo")?>" , {id:id} , function(data){
			if( data.code == 1 ) {
				layer.open({
					type: 1,
					title : '信息查看',
					skin: 'layui-layer-rim',
					area: ['650px', '400px'],
					content: $("#user_info_template").html().replace("{{login_ip}}" , data.data.login_ip).replace("{{login_time}}" , data.data.login_time).replace("{{integral}}" , data.data.integral).replace("{{money}}" , data.data.money).replace("{{sex}}" , data.data.sex).replace("{{address}}" , data.data.address).replace("{{avatar}}" , data.data.avatar).replace("{{nickname}}" , data.data.nickname).replace("{{mobile}}" , data.data.mobile)
				});
			} else {
				layer.msg(data.msg);
			}
		},'json');
	});
</script>

<!-------------------  4s服务商详情  --------------------->
<script id="shop_info_template">
<div style="padding:20px; width: 600px; height:330px;">
        <div class="am-form tpl-form-line-form">
            <div class="am-form-group" style="margin-bottom:.3rem">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label">商户LOGO </label>
                <div class="am-u-sm-6 am-u-end">
					<div style="width:60px; height:60px; overflow:hidden;border-radius:60px; border:1px solid #ccc;"><img src="{{logo}}" style="width:60px; height:60px;"></div>
				</div>
            </div>
            <div class="am-form-group" style="margin-bottom:.3rem">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label form-require">商户名称 </label>
                <div class="am-u-sm-6 am-u-end">
					<span style="height:3rem; line-height:3.4rem;  display: inline-block;font-size: 1.2rem;">{{shop_name}}</span>
				</div>
            </div>
            <div class="am-form-group" style="margin-bottom:.3rem">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label">店面类型 </label>
                <div class="am-u-sm-6 am-u-end">
					<span style="height:3rem; line-height:3.4rem;  display: inline-block;font-size: 1.2rem;">{{type}}</span>
				</div>
            </div>
			
            <div class="am-form-group" style="margin-bottom:.3rem">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label">联系人信息 </label>
                <div class="am-u-sm-6 am-u-end">
					<span style="height:3rem; line-height:3.4rem;  display: inline-block;font-size: 1.2rem;">{{contact}} / {{tel}}</span>
				</div>
            </div>
            <div class="am-form-group" style="margin-bottom:.3rem">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label">法人信息 </label>
                <div class="am-u-sm-6 am-u-end">
					<span style="height:3rem; line-height:3.4rem;  display: inline-block;font-size: 1.2rem;">{{legal_person}} / {{legal_person_mobile}}</span>
				</div>
            </div>
            <div class="am-form-group" style="margin-bottom:.3rem">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label">营业时间 </label>
                <div class="am-u-sm-6 am-u-end">
					<span style="height:3rem; line-height:3.4rem;  display: inline-block;font-size: 1.2rem;">{{saletime}}</span>
				</div>
            </div>
            <div class="am-form-group" style="margin-bottom:.3rem">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label">地址 </label>
                <div class="am-u-sm-6 am-u-end">
					<span style="height:3rem; line-height:3.4rem;  display: inline-block;font-size: 1.2rem;">{{address}}</span>
				</div>
            </div>
        </div>
</div>
</script>
<script>
	$("a.shop_info_show").click(function(){
		var id = $(this).data("id");
		$.get("<?=url("store/index/shopinfo")?>" , {id:id} , function(data){
			if( data.code == 1 ) {
				var htmlstr = $("#shop_info_template").html();
				$.each(data.data , function(k , v){
					htmlstr = htmlstr.replace("{{" + k + "}}" , v);
				});
				layer.open({
					type: 1,
					title : '商户信息查看',
					skin: 'layui-layer-rim',
					area: ['650px', '400px'],
					content: htmlstr
				});
			} else {
				layer.msg(data.msg);
			}
		},'json');
	});
	
	$(".submit_forms").submit(function(){
		var url = decodeURIComponent(window.location.href).split("?s=");
		var url_uri = url[1].substr(1).split('/');
		var url_data = $(this).serialize().split("&");
		$.each(url_data , function(k,v){
			var uri_data = v.split("=");
			var floag = false;
			$.each(url_uri , function(m,n){
				if( m > 2 && m % 2 == 1 && uri_data[0] == n ) {
					floag = true;
					url_uri[m + 1] = uri_data[1];
				}
			});
			if( floag === false ) {
				url_uri.push( uri_data[0] + '/' + uri_data[1] );
			}
			
		});
		location.href = url[0] + '?s=' + '/'+url_uri.join("/");
		return false;
	})
</script>
</body>

</html>
<?php endif; ?>
