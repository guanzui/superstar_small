<script id="tpl-file-item" type="text/template">
    {{ each list }}
    <div class="file-item">
        <img src="{{ $value.file_path }}">
		{{ if type == 1 }}
        <input type="hidden" name="{{ name }}" value="{{ $value.file_id }}">
		{{ else }}
		<input type="hidden" name="{{ name }}" value="/{{ $value.file_path }}">
		{{ /if }}
        <i class="iconfont icon-shanchu file-item-delete"></i>
    </div>
    {{ /each }}
</script>