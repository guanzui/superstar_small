<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">预约核算审核列表</div>
                </div>
                <div class="widget-body am-fr">
					<form action="" class="am-form tpl-form-line-form submit_forms" style="height:30px;" enctype="multipart/form-data" method="post">
                    <div class="am-u-sm-12 am-u-md-2 am-u-lg-2"></div>
					<div class="am-u-sm-12 am-u-md-10 am-u-lg-10" style="text-align: right;">
						<div class="am-btn-group am-btn-group-xs">
							<input type="text" class="tpl-form-input" name="merch" value="<?php echo input("param.merch");?>" placeholder="商家名称/商家ID">
						</div>
						<div class="am-btn-group am-btn-group-xs">
							<select name="status" required  data-am-selected="{searchBox: 1, btnSize: 'sm',  placeholder:'核算审核状态'}">
								<option value="-1">核算审核状态</option>
								<option value="0" <?php if( $status == "0" ) echo "selected" ?>>待申请</option>
								<option value="1" <?php if( $status == 1 ) echo "selected" ?>>待审核</option>
								<option value="2" <?php if( $status == 2 ) echo "selected" ?>>已驳回</option>
								<option value="3" <?php if( $status == 3 ) echo "selected" ?>>已通过</option>
							</select>
						</div>
						<button type="submit" class="j-submit am-btn am-btn-secondary fontsize15rem">搜索</button>
						<button type="button" class="export_submit am-btn am-btn-secondary fontsize15rem">导出</button>
					</div>
					</form>
					<style>
					.user_data_count{ width:100%; height:120px; margin:20px 0px;}
					.user_data_count_item{ width:calc(20% - 10px); height:120px; float:left;margin-right:10px; border:1px solid #efefef; border-top:3px solid #ccc}
					.user_data_count_item p{ margin:0px; text-align:center; font-size:14px;}
					.user_data_count_item p.item_ti{ margin:25px 0 5px 0; font-size:20px; font-weight:bold}
					</style>
					<div class="user_data_count">
						<div class="user_data_count_item">
							<p class="item_ti">￥<?=$data['total']?>元</p>
							<p>核算总额</p>
						</div>
						<div class="user_data_count_item">
							<p class="item_ti">￥<?=$data['y_price']?>元</p>
							<p>已核算</p>
						</div>
						<div class="user_data_count_item">
							<p class="item_ti">￥<?=$data['sh_price']?>元</p>
							<p>已申请核算</p>
						</div>
						<div class="user_data_count_item">
							<p class="item_ti">￥<?=$data['wtg_price']?>元</p>
							<p>被驳回核算</p>
						</div>
						<div class="user_data_count_item">
							<p class="item_ti">￥<?=$data['w_price']?>元</p>
							<p>未申请核算</p>
						</div>
					</div>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>服务商</th>
                                <th>服务用户</th>
                                <th>预约人</th>
                                <th>联系电话</th>
                                <th>服务类型</th>
                                <th>车辆品牌</th>
                                <th>汽车排量</th>
                                <th>用油量</th>
                                <th>状态</th>
								<?php if( $status > 1 ) :?>
                                <th>审核时间</th>
								<?php endif; ?>
                                <th>创建时间</th>
                                <th width="160">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['id'] ?></td>
                                    <td class="am-text-middle"><?= $item['shop_name'] ?> / <a href="">查看</a></td>
                                    <td class="am-text-middle"><?= $item['nickname'] ?> / <a href="javascript:;" class="user_info_show" data-id="<?=$item['user_id']?>">查看</a></td>
                                    <td class="am-text-middle"><?= $item['contact'] ?></td>
                                    <td class="am-text-middle"><?= $item['mobile'] ?></td>
                                    <td class="am-text-middle"><?= $item['type']==1?'换油':'补油' ?></td>
                                    <td class="am-text-middle"><?= $item['cart_brand'] ?></td>
                                    <td class="am-text-middle"><?= $item['displacement'] ?> L</td>
                                    <td class="am-text-middle"><?= $item['use_capacity'] ?> 升</td>
                                    <td class="am-text-middle" style="width:150px; white-space: initial !important">
										<?php switch($item['settlement_status']) {
												case 0 :
													echo "待申请";
												break;
												case 1 :
													echo "待审核";
												break;
												case 2 :
													echo "已驳回";
												break;
												case 3 :
													echo "已通过";
												break;
										}
										?>
										<?php if( $item['settlement_note'] ) :?>
										<div style="width:100%; display:block; color:red !important; font-size:.4rem;">原因：<?=$item['settlement_note']?></div>
										<?php endif; ?>
									</td>
									<?php if( $status > 1 ) :?>
									<td class="am-text-middle"><?php echo $item['sh_time'] > 0 ? date("Y-m-d H:i:s" , $item['sh_time']) : '--';?></td>
									<?php endif; ?>
                                    <td class="am-text-middle"><?php echo date("Y-m-d H:i:s" , $item['create_time']);?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
											<?php if( $item['settlement_status'] == 1 ) :?>
                                            <a href="javascript:;" class="chognzhi_but tpl-table-black-operation-del"
                                                data-id="<?= $item['id'] ?>">
                                                 <i class="iconfont sidebar-nav-link-logo icon-tag"></i> 审核
                                            </a>
											<?php endif; ?>
											<a href="<?= url('shop.replace/show',
                                                ['id' => $item['id']]) ?>">
                                                <i class="am-icon-pencil"></i> 预约详情
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="<?php echo $status > 1 ? 10 : 9;?>" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
	.chongzhi_body{ padding:20px; width: 600px; height:230px;}
</style>
<script id="chongzhi_template">
<div class="chongzhi_body">
        <div class="am-form tpl-form-line-form">
            <div class="am-form-group">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label form-require">审核状态 </label>
                <div class="am-u-sm-6 am-u-end">
					<select id="status_data" class="tpl-form-input">
						<option value="">请选择状态</option>
						<option value="2">驳回</option>
						<option value="3">审核通过</option>
					</select>
				</div>
            </div>
            <div class="am-form-group">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label">操作说明 </label>
                <div class="am-u-sm-9 am-u-end">
                    <textarea class="tpl-form-input  am-active" id="note_data" required="" placeholder="驳回申请必填"></textarea>
                </div>
            </div>
            <input type="hidden" value="1" name="id">
            <div class="am-form-group">
                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                    <button type="button" class="but-submit-data am-btn am-btn-secondary"  data-id="{{id}}">确认审核</button>
                </div>
            </div>
        </div>
</div>
</script>
<script>
	$("a.chognzhi_but").click(function(){
		var id = $(this).data("id");
		layer.open({
			type: 1,
			title : '预约核算审核',
			skin: 'layui-layer-rim',
			area: ['650px', '290px'],
			content: $("#chongzhi_template").html().replace("{{id}}" , id)
		});
	});
	$(document).on( "click" , ".but-submit-data" , function(){
		var status = $("#status_data").val();
		var note = $("#note_data").val();
		if( status == '' ) {
			layer.msg("请选择审核状态");
			return false;
		}
		if( note == '' && status == 2 ) {
			layer.msg("请输入驳回的原因");
			return false;
		}
		layer.msg('正在请求', {
			icon:16,
			shade:[0.1, '#fff'],
			time:false
		});
		$.post("<?=url("store/shop.yuyueauditing/infosave")?>" , { id : $(this).data("id") , status : status , note : note } , function(data){
			if( data.code == 1 ) {
				layer.alert(data.msg , function(){
					window.location.reload();
				});
			} else {
				layer.msg(data.msg);
			}
		},'json');
	});
</script>
<script>
$(".export_submit").click(function(){
	var url = decodeURIComponent(window.location.href).split("?s=");
	var url_uri = url[1].substr(1).split('/');
	var url_data = $('form').serialize().split("&");
	$.each(url_data , function(k,v){
		var uri_data = v.split("=");
		var floag = false;
		$.each(url_uri , function(m,n){
			if( m > 2 && m % 2 == 1 && uri_data[0] == n ) {
				floag = true;
				url_uri[m + 1] = uri_data[1];
			}
		});
		if( floag === false ) {
			url_uri.push( uri_data[0] + '/' + uri_data[1] );
		}
		
	});
	location.href = url[0] + '?s=' + '/'+url_uri.join("/").replace("<?php echo request()->action();?>" , 'export');
	return false;
});
</script>