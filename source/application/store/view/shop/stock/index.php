<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">机油【<?=$info['title']?>】库存管理</div>
                </div>
                <div class="row  am-cf">
                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-4">
                        <div class="widget widget-primary am-cf">
                            <div class="widget-statistic-header">
                                剩余库存
                            </div>
                            <div class="widget-statistic-body">
                                <div class="widget-statistic-value">
                                    <?=$info['stock']?> / 件
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-4">
                        <div class="widget widget-primary am-cf">
                            <div class="widget-statistic-header">
                                入库
                            </div>
                            <div class="widget-statistic-body">
                                <div class="widget-statistic-value">
                                    <?=$count_data['go']?> / 件
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-4">
                        <div class="widget widget-primary am-cf">
                            <div class="widget-statistic-header">
                                出库
                            </div>
                            <div class="widget-statistic-body">
                                <div class="widget-statistic-value">
                                    <?=$count_data['out']?> / 件
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="widget-body am-fr">
                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-6">
                        <div class="am-form-group">
                            <div class="am-btn-toolbar">
                                <div class="am-btn-group am-btn-group-xs">
                                    <a class="am-btn am-btn-default am-btn-success am-radius am-open-new-window" href="<?php echo url('shop.stock/add' , array('id' => $info['id'])); ?>" title="机油库存调整">
                                        <span class="am-icon-plus"></span> 库存调整
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>类型</th>
                                <th>详情</th>
                                <th>数量</th>
                                <th>目标</th>
                                <th>时间</th>
                                <th width="100">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle">
                                        <?php if(in_array($item['type'],array(1,4))) :?><span style="color:green"> + <?php else : ?><span style="color:red"> - <?php endif; ?><?php echo $item['type'] == 1 ? '系统入库' : ( $item['type'] == 2 ? '系统出库' : ( $item['type'] == 3 ? '分配出库' : '商户退回' ) ); ?></span>
                                    </td>
                                    <td class="am-text-middle"><?=$item['content']?></td>
                                    <td class="am-text-middle"><?= $item['stock'] ?></td>
                                    <td class="am-text-middle">
                                        <?php if( $item['shop_id'] <= 0 ) : ?>
                                            系统操作
                                        <?php else : ?>
                                            <?=$item['shop_name']; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td class="am-text-middle"><?php echo date("Y-m-d H:i:s" , $item['create_time']);?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <a href="javascript:;" class="item-delete tpl-table-black-operation-del"
                                                data-id="<?= $item['id'] ?>">
                                                 <i class="am-icon-trash"></i> 删除
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="9" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('.am-open-new-window').openWindow();
        var url = "<?= url('shop.stock/delete') ?>";
        $('.item-delete').delete('id', url);
    });
</script>

