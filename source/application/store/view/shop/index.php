<style>
	.kucun{width:500px;background:#fff;height:150px; padding:20px 0px;}
	.kucun dl{width:86%; margin-left:7%;}
	.kucun dl dt,.kucun dl dd{float:left; font-size:14px; padding:10px 0px; line-height:30px;}
	.kucun dl dt{ width:80px;}
	.kucun dl dd{ width:330px; margin-top:0px}
	.kucun dl dd input{ padding:5px 10px; border:1px solid #ccc;}
</style>
<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">服务中心管理</div>
                </div>
                <div class="widget-body am-fr">
					<form action="" style="height:50px;" class="am-form tpl-form-line-form submit_forms" enctype="multipart/form-data" method="get">
                    <div class="am-u-sm-12 am-u-md-2 am-u-lg-2">
                        <div class="am-form-group">
                            <div class="am-btn-toolbar">
                                <div class="am-btn-group am-btn-group-xs">
                                    <a class="am-btn am-btn-default am-btn-success am-radius"
                                       href="<?= url('shop/add') ?>">
                                        <span class="am-icon-plus"></span> 新增
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="am-u-sm-12 am-u-md-10 am-u-lg-10" style="text-align: right;">
						<div class="am-btn-group am-btn-group-xs" >
							<input type="text" class="tpl-form-input" name="tel" value="<?=input('param.tel')?>" placeholder="联系电话">
						</div>
						<div class="am-btn-group am-btn-group-xs" >
							<input type="text" class="tpl-form-input" name="shop_name" value="<?=input('param.shop_name')?>" placeholder="服务商名称">
						</div>
						<div class="am-btn-group am-btn-group-xs" style="width:40%;" id="area_select_div">
							<select name="province" id="province" data-id="<?=input('param.province')?>" style="width:calc(33.3% - 10px); float:left; margin-right:10px;">
								<option value="">请选择省</option>
							</select>
							<select name="city" id="city" data-id="<?=input('param.city')?>" style="width:calc(33.3% - 10px); float:left; margin-right:10px;">
								<option value="">请选择城市</option>
							</select>
							<select name="area" id="area" data-id="<?=input('param.area')?>" style="width:calc(33.3% - 10px); float:left;">
								<option value="">请选择区域</option>
							</select>
						</div>
						<button type="submit" class="j-submit am-btn am-btn-secondary fontsize15rem">搜索</button>
						<button type="button" class="export_submit am-btn am-btn-secondary fontsize15rem">导出</button>
					</div>
					</form>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>名称</th>
                                <th>LOGO</th>
                                <th>联系人</th>
                                <th>电话</th>
                                <th>地区</th>
                                <th>总共分配</th>
                                <th>状态</th>
                                <th>创建时间</th>
                                <th width="160">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['shop_id'] ?></td>
                                    <td class="am-text-middle"><?= $item['shop_name'] ?></td>
                                    <td class="am-text-middle"><?php if($item['logo']):?><a href=""><img src="<?= $item['logo']?>" width="80"></a><?php else: ?>无<?php endif;?></td>
                                    <td class="am-text-middle"><?= $item['contact']?></td>
                                    <td class="am-text-middle"><?= $item['tel'] ?></td>
                                    <td class="am-text-middle"><?= $item['province_name'] ?>-<?= $item['city_name'] ?>-<?= $item['area_name'] ?></td>
                                    <td class="am-text-middle"><?=$item['stock']?> / 件</td>
                                    <td class="am-text-middle"><?php echo $item['status'] == 1?'显示':'禁用';?></td>
                                    <td class="am-text-middle"><?php echo date("Y-m-d H:i:s" , $item['create_time']);?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
											<a href="javascript:;" data-id="<?=$item['shop_id']?>" data-name="<?=$item['shop_name']?>" id="data_show">
                                                <i class="iconfont sidebar-nav-link-logo icon-linechart" style=""></i> 数据查看
                                            </a>
											<a href="javascript:;" data-id="<?=$item['shop_id']?>" data-name="<?=$item['shop_name']?>" class="chognzhi_but">
                                                <i class="iconfont sidebar-nav-link-logo icon-shopping"></i> 机油分配
                                            </a>
											
                                            <a href="<?= url('shop/edit',
                                                ['shop_id' => $item['shop_id']]) ?>">
                                                <i class="am-icon-pencil"></i> 编辑
                                            </a>
                                            <a href="javascript:;" class="item-delete tpl-table-black-operation-del"
                                                data-id="<?= $item['shop_id'] ?>">
                                                 <i class="am-icon-trash"></i> 删除
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="9" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
	.show_data_b{ width:900px; height:410px; background:#fff; border:1px solid #ccc; position:absolute; left:50%; margin-left:-450px; top:20%;}
	.tongji_1{ margin:20px;  border:1px solid #efefef; height:100px;}
	.tongji_1 .item{ width:25%; height:100px; text-align:center; float:left; }
	.tongji_1 .item p{ margin:0px; padding:0px; font-size:14px;}
	.tongji_1 .item p.item_t{ margin:20px 0px 5px; font-size:18px;}
	.tongji_2{ margin:0px 20px 20px 20px; width:calc(50% - 40px); float:left; border:1px solid #efefef;}
	.tongji_2 dl dt{ width:100%; background:#efefef;  height:40px; float:left font-size:14px;font-weight:100; line-height:40px; text-indent:1rem;}
	.tongji_2 dl dd{ width:33.3%; float:left;height:120px; margin:0px; text-align:center;}
	.tongji_2 dl dd p{ margin:0px; padding:0px; font-size:14px;}
	.tongji_2 dl dd p.item_t{ margin:35px 0px 3px; font-size:18px;}
	.meu_list{ width:100%; height:45px; line-height:45px; border-bottom:1px solid #ccc; margin-top:10px;}
	.meu_list a{ position:; padding:0px 20px; height:45px; display:inline-block; font-size:14px;}
	.meu_list a.avtives{ border:1px solid #ccc; background:#fff; border-bottom:1px solid #fff;}
	.jiyouliebiao{ margin:20px; height:405px; overflow-y:scroll}
	.jiyouliebiao dl{ width:100%; padding:10px 0px; margin:0px;float:left; border-bottom:1px solid #efefef;}
	.jiyouliebiao dl dd{ width:100%; height:100px;}
	.jiyouliebiao dl dt,.jiyouliebiao dl dt img{ width:100px; height:100px; float:left;}
	.jiyouliebiao dl dd{ width:calc(100% - 120px); float:right; height:100px;margin-top:0px;}
	.jiyouliebiao dl dd p{font-size:14px;margin:0px;margin-bottom:5px;}
	#show_log_ck,#stock_zhao{ padding:5px 10px;background:red; color:#fff; display:inline-block; font-size:12px; margin-left:30px;}
</style>
<script id="template_data">
	<div class="meu_list">
		<a href="javascript:;" data-type="0" class="avtives" style="margin-left:10px;">数据统计</a>
		<a href="javascript:;" data-type="1">库存列表</a>
		<a href="javascript:;" data-url="<?=url('store/shop.replace/index')?>/shop_id/{#shop_id}" data-type="2">预约列表</a>
		<a href="javascript:;" data-url="<?=url('store/shop.yuyueauditing/index')?>/shop_id/{#shop_id}/status/-1" data-type="3">核算列表</a>
		<a href="javascript:;" data-url="<?=url('store/shop.replenishment/index')?>/shop_id/{#shop_id}" data-type="4">申请库存列表</a>
		<a href="javascript:;" data-url="<?=url('store/shop.replacecomment/index')?>/shop_id/{#shop_id}" data-type="5">评论列表</a>
	</div>
	<div class="jiyouliebiao" id="menuList_1" style="display:none;">
	</div>
	<div id="menuList_0">
		<div class="tongji_1">
			<div class="item">
				<p class="item_t" id="sh">--</p>
				<p>待审核</p>
			</div>
			<div class="item">
				<p class="item_t" id="ok">--</p>
				<p>已预约</p>
			</div>
			<div class="item" >
				<p class="item_t" id="start">--</p>
				<p>进行中</p>
			</div>
			<div class="item">
				<p class="item_t" id="over">--</p>
				<p>已完成</p>
			</div>
		</div>
		
		<div class="tongji_1">
			<div class="item">
				<p class="item_t" id="y_sy">--</p>
				<p>剩余油量</p>
			</div>
			<div class="item">
				<p class="item_t" id="y_use">--</p>
				<p>使用油量</p>
			</div>
			<div class="item" >
				<p class="item_t" id="y_use_h">--</p>
				<p>换油量</p>
			</div>
			<div class="item">
				<p class="item_t" id="y_use_b">--</p>
				<p>补油量</p>
			</div>
		</div>
		<div class="tongji_2">
			<dl>
				<dt>
					<span style="float:right;margin-right:10px;">总计：<span id="hsz">--</span>单</span>预约核算
				</dt>
				<dd>
					<p class="item_t" id="hsd">--</p>
					<p>待核算</p>
				</dd>
				<dd>
					<p class="item_t" id="hsy">--</p>
					<p>已核算</p>
				</dd>
				<dd>
					<p class="item_t" id="hsw">--</p>
					<p>已完成</p>
				</dd>
			</dl>
		</div>
		<div class="tongji_2" style="margin-left:0px;width:calc(50% - 20px);">
			<dl>
				<dt>
					<span style="float:right;margin-right:10px;">共计补油：<span id="jyz">--</span>升</span>机油库存
				</dt>
				<dd>
					<p class="item_t" id="jys">--</p>
					<p>正在申请</p>
				</dd>
				<dd>
					<p class="item_t" id="jyb">--</p>
					<p>审核失败</p>
				</dd>
				<dd>
					<p class="item_t" id="jyc">--</p>
					<p>已通过补充</p>
				</dd>
			</dl>
		</div>
	</div>
</script>

<style>
	.chongzhi_body{ padding:20px; width: 600px; height:230px;}
</style>
<script id="chongzhi_template">
<div class="chongzhi_body">
        <div class="am-form tpl-form-line-form">
            <div class="am-form-group">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label form-require">机油 </label>
                <div class="am-u-sm-6 am-u-end">
					<select id="data" class="tpl-form-input">
						<option value="">请选择机油</option>
						<?php foreach( $engineoil_list as $k => $v ) : ?>
						<option value="<?=$v['id']?>"><?=$v['title']?> - 库存：<?=$v['stock']?></option>
						<?php endforeach; ?>
					</select>
				</div>
            </div>
            <div class="am-form-group">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label form-require">分配库存 </label>
                <div class="am-u-sm-9 am-u-end">
                    <input type="number" class="tpl-form-input" id="stock" value="" required="" placeholder="请输入分配的库存数量" />
					<span>
                </div>
            </div>
            <div class="am-form-group">
                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                    <button type="button" class="but-submit-data am-btn am-btn-secondary"  data-id="{{id}}">确认分配</button>
                </div>
            </div>
        </div>
</div>
</script>
<script id="stock_template">
	<div class="kucun">
		<dl>
			<dt>当前库存：</dt>
			<dd style="font-size:20px; color:#F39C12">{{stock}}升</dd>
		</dl>
		<dl>
			<dt>调整：</dt>
			<dd><input type="text" id="stock_tz"><span style="width:90%; font-size:12px; color:#999; display:block; line-height:180%;">注意：这里输入必须为数字，输入负数" -10 "则代表减  输入正数 " 10 "代表增加</span></dd>
		</dl>
		<dl>
			<dt></dt>
			<dd><input type="button" data-id="{{id}}" id="stock_zt_but" value="确定调整" style="padding:8px 15px; background:#E74C3C; border:none; color:#fff; font-size:14px;"/></dd>
		</dl>
	</div>
</script>
<script>
	var stock_tc_div = null;
	$("a.chognzhi_but").click(function(){
		var id = $(this).data("id");
		layer.open({
			type: 1,
			title : '给【'+$(this).data("name")+'】分配机油',
			skin: 'layui-layer-rim',
			area: ['650px', '290px'],
			content: $("#chongzhi_template").html().replace("{{id}}" , id)
		});
	});
	$(document).on('click',"a#stock_zhao",function(){
		var id = $(this).data("id");
		var stock = $(this).data("stock");
		stock_tc_div = layer.open({
			type: 1,
			title : '库存调整',
			skin: 'layui-layer-rim',
			area: ['515px', '300px'],
			content: $("#stock_template").html().replace("{{id}}" , id).replace("{{stock}}" , stock)
		});
	});
	$(document).on("click" , "#stock_zt_but" , function(){
		var id = $(this).data("id");
		var stock = $(this).closest(".kucun").find("input[type='text']").val();
		if( stock == '' ){
			layer.msg("请输入要调整的数字");
			return false;
		}
		$.post("<?=url("shop/stockupdate")?>" , {id:id , stock:stock},function(data){
			if( data.code == 1 ) {
				var layer_alert_index = layer.alert(data.msg,function(){
					layer.close(stock_tc_div);
					layer.close(layer_alert_index);
					gettwo();
					return true;
				});
			} else {
				layer.msg(data.msg)
			}
		},'json');
	});
	$(document).on( "click" , ".but-submit-data" , function(){
		var data = $("#data").val();
		var stock = $("#stock").val();
		if( data == '' ) {
			layer.msg("请选择分配的机油");
			return false;
		}
		if( stock == '' ) {
			layer.msg("请输入分配的数量");
			return false;
		}
		layer.msg('正在请求', {
			icon:16,
			shade:[0.1, '#fff'],
			time:false
		});
		$.post("<?=url('store/shop/fenpei');?>" , { id : $(this).data("id") , data : data , stock : stock } , function(data){
			if( data.code == 1 ) {
				layer.alert(data.msg , function(){
					window.location.reload();
				});
			} else {
				layer.msg(data.msg);
			}
		},'json');
	});
</script>
<script>
	var json_area = <?php echo ( $area); ?>;
	var shop_id = 0;
	var layerMsg;
    $(function () {
        // 删除元素
        var url = "<?= url('shop/delete') ?>";
        $('.item-delete').delete('shop_id', url);

    });
	$("a#data_show").click(function(){
		shop_id = $(this).data("id");
		layer.open({
		  type: 1,
		  title:'【'+$(this).data('name')+'】商家数据信息',
		  skin: 'layui-layer-rim',
		  area: ['800px', '560px'],
		  content: $('#template_data').html().replace(/\{\#shop\_id\}/g , shop_id),
		});
		loading()
		getone();
	})
	function loading(){
		layerMsg = layer.msg("加载中", {
			title: false,
			time: false,
			shade: [0.5,'black'],
			icon: 16
		});
	}
	$(document).on('click' ,'.meu_list a' , function(){
		$('.meu_list a.avtives').removeClass("avtives");
		$(this).addClass("avtives");
		var type = $(this).data("type");
		$("#menuList_" + (type > 0 ? 1 : 0)).show();
		$("#menuList_" + (type > 0 ? 0 : 1)).hide();
		loading();
		switch( type ) {
			case 0 :
				getone();
			break;
			case 1 :
				gettwo();
			break;
			default : 
				getthree( this );
			break;
		}
	});
	
	function getone(){
		$.get('<?=url('store/shop/dataCount')?>' , {shop_id : shop_id} , function(data){
			layer.close(layerMsg)
			$.each(data.data , function(k,v){
				$("#menuList_0 #" + k).html(v);
			})
		},'json');
	}
	
	function gettwo(){
		$.get('<?=url('store/shop/getStockList')?>' , {shop_id : shop_id} , function(data){
			layer.close(layerMsg)
			var strhtml = '';
			$.each(data.data , function(k,v){
				strhtml += '<dl><dt><img src="'+v.thumb+'" /></dt><dd><p style="margin-bottom:20px;">'+v.title+'</p><p>品牌：'+v.brand+' '+v.model_number+'</p><p>剩余油量：'+v.stock_num+' 升 <a href="/index.php?s=/store/count.shoppurchase/index?id='+v.id+'&shop_id='+v.shop_id+'" id="show_log_ck" data-id="">查看出入库日志</a> <a id="stock_zhao" data-stock="'+v.stock_num+'" data-id="'+v.esid+'">调整</a></p></dd></dl>'
			})
			$("#menuList_1").html(strhtml + '<div style="clear:both"></div>');
		},'json');
	}
	
	function getthree( obj ) {
		var url = $(obj).data("url");
		$.get(url , function(data){
			layer.close(layerMsg)
			switch( $(obj).data("type") ) {
				case 2 :
				var strhtml = '<table width="100%" class="am-table am-table-compact am-table-striped tpl-table-black am-text-nowrap"><thead><tr><th>预约用户</th><th>服务类型</th><th>车型</th><th>预约时间</th><th>联系人</th><th>联系电话</th><th>状态</th></tr></thead><tbody>';
				if( data.data.length <= 0 ) {
					strhtml += '<tr><td colspan="7" class="am-text-center">暂无记录</td></tr>';
				} else {
					$.each(data.data , function(k,v){
						strhtml += '<tr><td>'+v.nickname+'</td><td>'+(v.type==1?'换油':'补油')+'</td><td>'+v.cart_brand+'</td><td>'+v.yuyue_time+' '+v.specific_time+'</td><td>'+v.contact+'</td><td>'+v.mobile+'</td><td>'+v.statu_name+'</td></tr>'
					})
				}
				strhtml += '</tbody></table>';
				break;
				case 3 :
				var strhtml = '<table width="100%" class="am-table am-table-compact am-table-striped tpl-table-black am-text-nowrap"><thead><tr><th>服务用户</th><th>预约人</th><th>联系电话</th><th>车辆品牌</th><th>汽车排量</th><th>用油量</th><th>状态</th></tr></thead><tbody>';
				if( data.data.length <= 0 ) {
					strhtml += '<tr><td colspan="5" class="am-text-center">暂无记录</td></tr>';
				} else {
					$.each(data.data , function(k,v){
						strhtml += '<tr><td>'+v.nickname+'</td><td>'+v.contact+'</td><td>'+v.mobile+'</td><td>'+v.cart_brand+'</td><td>'+v.displacement+'L</td><td>'+v.use_capacity+'升</td><td>'+v.status_name+'</td></tr>'
					})
				}
				strhtml += '</tbody></table>';
				break;
				case 4 :
				var strhtml = '<table width="100%" class="am-table am-table-compact am-table-striped tpl-table-black am-text-nowrap"><thead><tr><th>商品信息</th><th>补充数量</th><th>说明</th><th>状态</th></tr></thead><tbody>';
				if( data.data.length <= 0 ) {
					strhtml += '<tr><td colspan="4" class="am-text-center">暂无记录</td></tr>';
				} else {
					$.each(data.data , function(k,v){
						strhtml += '<tr><td><img src="'+v.thumb+ '" width="60" /><span style="display: inline-block; margin-left:10px;">'+v.title+'</span></td><td>'+v.stock+'</td><td>'+v.content+'</td><td>'+v.status_name+'</td></tr>'
					})
				}
				strhtml += '</tbody></table>';
				break;
				case 5 :
				var strhtml = '<table width="100%" class="am-table am-table-compact am-table-striped tpl-table-black am-text-nowrap"><thead><tr><th>评论用户</th><th>评论类型</th><th>服务态度</th><th>技术态度</th><th>评论内容</th><th>状态</th></tr></thead><tbody>';
				if( data.data.length <= 0 ) {
					strhtml += '<tr><td colspan="6" class="am-text-center">暂无记录</td></tr>';
				} else {
					$.each(data.data , function(k,v){
						strhtml += '<tr><td>'+v.nickname+'</td><td>'+(v.type==1?'差评':(v.type==2?"中评":"好评"))+'</td><td>'+v.service_star+'</td><td>'+v.skill_star+'</td><td>'+v.content+'</td><td>'+(v.status == 1 ? "显示" : '隐藏')+'</td></tr>'
					})
				}
				strhtml += '</tbody></table>';
				break;
			}
			$("#menuList_1").html(strhtml);
		},'json');
	}
</script>
<script>
$(".export_submit").click(function(){
	var url = decodeURIComponent(window.location.href).split("?s=");
	var url_uri = url[1].substr(1).split('/');
	var url_data = $('form').serialize().split("&");
	$.each(url_data , function(k,v){
		var uri_data = v.split("=");
		var floag = false;
		$.each(url_uri , function(m,n){
			if( m > 2 && m % 2 == 1 && uri_data[0] == n ) {
				floag = true;
				url_uri[m + 1] = uri_data[1];
			}
		});
		if( floag === false ) {
			url_uri.push( uri_data[0] + '/' + uri_data[1] );
		}
		
	});
	location.href = url[0] + '?s=' + '/'+url_uri.join("/").replace("<?php echo request()->action();?>" , 'export');
	return false;
});
</script>