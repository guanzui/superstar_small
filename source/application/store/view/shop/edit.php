<style>
    .layui-layer-content{ height: auto !important}
</style>
<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">编辑服务中心</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">店面名称 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="shop[shop_name]"  value="<?=$save_data['shop_name']?>" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require">
                                    店面类型
                                </label>
                                <div class="am-u-sm-10">
                                    <label class="am-checkbox-inline">
                                        <input type="checkbox" name="shop[type][]" value="1" data-am-ucheck required <?php if( strpos( $save_data['type'] , ',1,' ) !== false ) echo "checked"; ?>>汽油中心
                                    </label>
                                    <label class="am-checkbox-inline">
                                        <input type="checkbox" name="shop[type][]" value="2" data-am-ucheck required <?php if( strpos( $save_data['type'] , ',2,' ) !== false ) echo "checked"; ?>>柴油中心
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group select_user_info" data-name="shop[user_id]">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">管理员 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" placeholder="请选择用户" value="<?php if( $save_data['user_id'] > 0) { echo $save_data['user_info']['nickname']; }?>" required style="width:50%;display: inline-block;" readonly="">
                                    <button type="button" style="padding:7px 15px; border:0px;display: inline-block " class="am-btn-secondary"><i class="iconfont sidebar-nav-link-logo icon-account" style=""></i> 选择用户</button>
                                    <div class="uploader-list am-cf">
                                        <?php if( $save_data['user_id'] > 0) :?>
                                        <div class="file-item">
                                            <img src="<?=$save_data['user_info']['avatar']?>">
                                            <input type="hidden" value="<?=$save_data['user_id']?>" name="shop[user_id]" />
                                            <i class="iconfont icon-shanchu file-item-user-delete"></i>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">LOGO上传 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <button type="button" id="upload_logo" class="upload-file am-btn am-btn-secondary am-radius"><i class="am-icon-cloud-upload"></i> 选择LOGO</button>
                                        <div class="uploader-list am-cf">
                                            <?php if( $save_data['logo']) : ?>
                                                <div class="file-item">
                                                    <img src="<?= $save_data['logo']?>">
                                                    <input type="hidden" name="shop[logo]"
                                                           value="<?= $save_data['logo']?>">
                                                    <i class="iconfont icon-shanchu file-item-delete"></i>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="help-block am-margin-top-sm">
                                        <small>尺寸200x200像素以上，大小1M以下</small>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group am-form-select" id="area_select_div">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">地区 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline" style="padding-left: 0px;">
                                        <select name="shop[province]" id="province" data-id="<?=$save_data['province']?>" style="width:180px;">
                                            <option value="1">请选择省</option>
                                        </select>
                                    </label>
                                    <label class="am-radio-inline" style="padding-left: 0px;">
                                        <select name="shop[city]" id="city" data-id="<?=$save_data['city']?>" style="width:180px;">
                                            <option value="1">请选择城市</option>
                                        </select>
                                    </label>
                                    <label class="am-radio-inline" style="padding-left: 0px;">
                                        <select name="shop[area]" id="area" data-id="<?=$save_data['area']?>" style="width:180px;">
                                            <option value="1">请选择区域</option>
                                        </select>
                                    </label>
                                </div>
                            </div>
                            
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">地理坐标 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline" style="padding-left: 0px;">
                                        <input type="text" class="tpl-form-input" name="shop[lng]" id="lng" value="<?=$save_data['lng']?>" placeholder="请选择经度" readonly="" style="background: #efefef" required>
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="text" class="tpl-form-input" name="shop[lat]" id="lat" value="<?=$save_data['lat']?>" placeholder="请选择纬度" readonly="" style="background: #efefef" required>
                                    </label>
                                    <label class="am-radio-inline">
                                        <button type="button" id="location_map" style="padding:7px 15px; border:0px; border-radius: 5px;" class="am-btn-secondary"><i class="iconfont sidebar-nav-link-logo icon-dingwei" style=""></i> 选择坐标</button>
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">详细地址 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="shop[address]" value="<?=$save_data['address']?>" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">联系人 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="shop[contact]" value="<?=$save_data['contact']?>" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">联系电话 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="shop[tel]" value="<?=$save_data['tel']?>" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">法人姓名 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="shop[legal_person]" value="<?=$save_data['legal_person']?>" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">法人电话 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="shop[legal_person_mobile]" value="<?=$save_data['legal_person_mobile']?>" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">营业时间 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="shop[saletime]" value="<?=$save_data['saletime']?>" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">4s店简介 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <textarea class="tpl-form-input" name="shop[info]" required placeholder="请输入4s店介绍"><?=$save_data['info']?></textarea>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">营业执照 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <div class="am-form-file">
                                            <button type="button"
                                                    class="upload-file am-btn am-btn-secondary am-radius" id="upload_business_license">
                                                <i class="am-icon-cloud-upload"></i> 选择图片
                                            </button>
                                            <div class="uploader-list am-cf">
                                                <?php if( $save_data['business_license']) : ?>
                                                    <div class="file-item">
                                                        <img src="<?= $save_data['business_license']?>">
                                                        <input type="hidden" name="shop[business_license]"
                                                               value="<?= $save_data['business_license']?>">
                                                        <i class="iconfont icon-shanchu file-item-delete"></i>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="help-block am-margin-top-sm">
                                            <small>大小2M以下 (可拖拽图片调整显示顺序 )</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">道路运输执照 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <div class="am-form-file">
                                            <button type="button"
                                                    class="upload-file am-btn am-btn-secondary am-radius" id="upload_transport_license">
                                                <i class="am-icon-cloud-upload"></i> 选择图片
                                            </button>
                                            <div class="uploader-list am-cf">
                                                <?php if( $save_data['transport_license']) : ?>
                                                    <?php foreach( $save_data['transport_license'] as $k => $v ) :?>
                                                    <div class="file-item">
                                                        <img src="<?= $v?>">
                                                        <input type="hidden" name="shop[transport_license][]"
                                                               value="<?= $v ?>">
                                                        <i class="iconfont icon-shanchu file-item-delete"></i>
                                                    </div>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="help-block am-margin-top-sm">
                                            <small>大小2M以下 (可拖拽图片调整显示顺序 )</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">店面照片 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <div class="am-form-file">
                                            <button type="button"
                                                    class="upload-file am-btn am-btn-secondary am-radius" id="upload_shop_image">
                                                <i class="am-icon-cloud-upload"></i> 选择图片
                                            </button>
                                            <div class="uploader-list am-cf">
                                                <?php if( $save_data['shop_image']) : ?>
                                                    <?php foreach( $save_data['shop_image'] as $k => $v ) :?>
                                                    <div class="file-item">
                                                        <img src="<?= $v ?>">
                                                        <input type="hidden" name="shop[shop_image][]"
                                                               value="<?= $v ?>">
                                                        <i class="iconfont icon-shanchu file-item-delete"></i>
                                                    </div>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="help-block am-margin-top-sm">
                                            <small>大小2M以下 (可拖拽图片调整显示顺序 )</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">车间照片 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <div class="am-form-file">
                                            <button type="button"
                                                    class="upload-file am-btn am-btn-secondary am-radius" id="upload_workshop_image">
                                                <i class="am-icon-cloud-upload"></i> 选择图片
                                            </button>
                                            <div class="uploader-list am-cf">
                                                <?php if( $save_data['workshop_image']) : ?>
                                                    <?php foreach( $save_data['workshop_image'] as $k => $v ) :?>
                                                    <div class="file-item">
                                                        <img src="<?= $v?>">
                                                        <input type="hidden" name="shop[workshop_image][]"
                                                               value="<?= $v ?>">
                                                        <i class="iconfont icon-shanchu file-item-delete"></i>
                                                    </div>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="help-block am-margin-top-sm">
                                            <small>大小2M以下 (可拖拽图片调整显示顺序 )</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">接待室照片 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <div class="am-form-file">
                                            <button type="button"
                                                    class="upload-file am-btn am-btn-secondary am-radius" id="upload_reception_image">
                                                <i class="am-icon-cloud-upload"></i> 选择图片
                                            </button>
                                            <div class="uploader-list am-cf">
                                                <?php if( $save_data['reception_image']) : ?>
                                                    <?php foreach( $save_data['reception_image'] as $k => $v ) :?>
                                                    <div class="file-item">
                                                        <img src="<?= $v?>">
                                                        <input type="hidden" name="shop[reception_image][]"
                                                               value="<?= $v ?>">
                                                        <i class="iconfont icon-shanchu file-item-delete"></i>
                                                    </div>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="help-block am-margin-top-sm">
                                            <small>大小2M以下 (可拖拽图片调整显示顺序 )</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">办公室照片 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <div class="am-form-file">
                                            <button type="button"
                                                    class="upload-file am-btn am-btn-secondary am-radius" id="upload_office_image">
                                                <i class="am-icon-cloud-upload"></i> 选择图片
                                            </button>
                                            <div class="uploader-list am-cf">
                                                <?php if( $save_data['office_image']) : ?>
                                                    <?php foreach( $save_data['office_image'] as $k => $v ) :?>
                                                    <div class="file-item">
                                                        <img src="<?= $v ?>">
                                                        <input type="hidden" name="shop[office_image][]"
                                                               value="<?= $v ?>">
                                                        <i class="iconfont icon-shanchu file-item-delete"></i>
                                                    </div>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="help-block am-margin-top-sm">
                                            <small>大小2M以下 (可拖拽图片调整显示顺序 )</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require">
                                    4s店状态
                                </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="shop[status]" value="1" data-am-ucheck required checked>启用
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="shop[status]" value="0" data-am-ucheck required>禁用
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <input type="hidden" value="<?= $save_data['shop_id']?>" name="shop_id" />
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- 图片文件列表模板 -->
{{include file="layouts/_template/tpl_file_item" /}}

<!-- 文件库弹窗 -->
{{include file="layouts/_template/file_library" /}}
<script src="assets/store/js/ddsort.js"></script>
<script>
    var json_area = <?php echo ( $area); ?>;
    $(function () {

        $('#upload_logo').selectImages({
            name: 'shop[logo]',type:2
        });
        $('#upload_business_license').selectImages({
            name: 'shop[business_license]',type:2
        });
        $('#upload_transport_license').selectImages({
            name: 'shop[transport_license][]',type:2
            , multiple: true
        });
        $('#upload_shop_image').selectImages({
            name: 'shop[shop_image][]',type:2
            , multiple: true
        });
        $('#upload_workshop_image').selectImages({
            name: 'shop[workshop_image][]',type:2
            , multiple: true
        });
        $('#upload_reception_image').selectImages({
            name: 'shop[reception_image][]',type:2
            , multiple: true
        });
        $('#upload_office_image').selectImages({
            name: 'shop[office_image][]',type:2
            , multiple: true
        });
        $('.uploader-list').DDSort({
            target: '.file-item',
            delay: 100, // 延时处理，默认为 50 ms，防止手抖点击 A 链接无效
            floatStyle: {
                'border': '1px solid #ccc',
                'background-color': '#fff'
            }
        });
        $("#location_map").click(function(){
            layer.open({
                type: 2,
                title: '坐标选择',
                shadeClose: true,
                shade: false,
                maxmin: true, //开启最大化最小化按钮
                area: ['893px', '600px'],
                content: '/map_select.html'
            });
        });
        /**
         * 表单验证提交
         * @type {*}
         */
        $('#my-form').superForm();
    });
    function backLocationMapFunc(lat,lng){
        layer.closeAll();
        $("#lng").val(lng);
        $("#lat").val(lat);
    }
</script>
