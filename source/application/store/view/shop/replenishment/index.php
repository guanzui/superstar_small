<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">补油申请列表</div>
                </div>
                <div class="widget-body am-fr">
					<form action="" style="height:50px;" class="am-form tpl-form-line-form submit_forms" enctype="multipart/form-data" method="get">
                    <div class="am-u-sm-12 am-u-md-2 am-u-lg-2"></div>
					<div class="am-u-sm-12 am-u-md-10 am-u-lg-10" style="text-align: right;">
						<div class="am-btn-group am-btn-group-xs">
							<input type="text" class="tpl-form-input" name="merch" value="<?php echo input("param.merch");?>" placeholder="商家名称/商家ID">
						</div>
						<div class="am-btn-group am-btn-group-xs" >
							<select name="status" required  data-am-selected="{searchBox: 1, btnSize: 'sm',  placeholder:'请选择审核类型'}">
								<option value="-1">全部</option>
								<option value="0" <?php if( input('param.status') == "0" ) echo "selected" ?>>未审核</option>
								<option value="1" <?php if( input('param.status') == 1 ) echo "selected" ?>>未通过</option>
								<option value="2" <?php if( input('param.status') == 2 ) echo "selected" ?>>已审核</option>
								<option value="3" <?php if( input('param.status') == 3 ) echo "selected" ?>>已发货</option>
								<option value="4" <?php if( input('param.status') == 4 ) echo "selected" ?>>已收货</option>
							</select>
						</div>
						<button type="submit" class="j-submit am-btn am-btn-secondary fontsize15rem">搜索</button>
						<button type="button" class="export_submit am-btn am-btn-secondary fontsize15rem">导出</button>
					</div>
					</form>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>商品信息</th>
                                <th>补充数量</th>
                                <th>说明</th>
                                <th>店铺信息</th>
                                <th>状态</th>
                                <th>申请时间</th>
                                <th width="160">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><img src="<?= $item['thumb'] ?>" width="60" /><span style="display: inline-block; margin-left:10px;"><?=$item['title']?></span></td>
                                    <td class="am-text-middle"><?= $item['stock'] ?></td>
                                    <td class="am-text-middle"><?=$item['content']?></td>
                                    <td class="am-text-middle"><?= $item['shop_name'] ?> / <a href="javascript:;" class="shop_info_show" data-id="<?=$item['shop_id']?>">查看</a></td>
                                    <td class="am-text-middle">
                                        <?php 
                                            $fields = array('未审核' , '未通过' , '已通过' , '已发货' , '已收货' , '完成');
                                            echo $fields[$item['status']];
                                        ?>
                                        <?php if( $item['status'] >= 3 ) : ?>
                                        <a href="javascript:;" class="express_info_list" data-id="<?=$item['id']?>">查看物流</a>
                                        <?php endif; ?>
                                    </td>
                                    <td class="am-text-middle"><?php echo date("Y-m-d H:i:s" , $item['create_time']);?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
											<a href="<?= url('shop.replenishment/useyoudata',['id' => $item['id']]) ?>" class="data_show">查看用油量</a>
                                            <?php if( $item['status'] == 5 ) :?>
                                            <span style="color:green">已完成</span>
                                            <?php else : ?>
                                            <?php if( $item['status'] == 0 ) :?>
                                            <a class="inspect-but" href="javascript:;" data-id="<?=$item['id']?>"><i class="am-icon-pencil"></i> 审核</a>
                                            <?php endif; ?>
                                            <?php if( $item['status'] == 2 ) :?>
                                            <a href="javascript:;" class="send_express" data-id="<?=$item['id']?>">
                                                <i class="am-icon-pencil"></i> 发货
                                            </a>
                                            <?php endif; ?>
                                            <?php if( in_array($item['status'] , [0,1,5])) :?>
                                            <a href="javascript:;" class="item-delete tpl-table-black-operation-del"
                                                data-id="<?= $item['id'] ?>">
                                                 <i class="am-icon-trash"></i> 删除
                                            </a>
                                            <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="9" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .inspect_boty{ width:650px; height:300px;}
    .inspect_boty .hui_disa{ background: #efefef }
    .express_titles{ width:100%; height:36px;  border-bottom:1px solid #ccc; text-indent: 1em;}
    .am-u-sm-2{ font-size:14px !important; line-height:220% !important;}
    .express_infos_lists{ width:94%; margin-left:3%; margin-top:10px; height:300px;  overflow-y: scroll}
    .express_infos_lists dl dd{ float:left; line-height: 180%; font-size:14px; width: 100%; margin-top:8px; border-bottom: 1px solid #efefef; color:#666}
    .express_infos_lists dl dd span{ width:100%; display: block; }
    .express_infos_lists dl dd:nth-child(1){ color:green}
</style>
<script id="inspect_template">
<div class="inspect_boty">
    <div class="row-content am-cf">
        <div class="row">
            <div class="widget-body">
                <div class="am-form tpl-form-line-form">
                    <div class="am-form-group">
                        <label class="am-u-sm-2 am-u-lg-2">审核类型 </label>
                        <div class="am-u-sm-9 am-u-end" style="margin-top:-10px;">
                            <label class="am-radio-inline">
                                <input type="radio" name="status" value="2" data-am-ucheck required checked>审核通过
                            </label>
                            <label class="am-radio-inline">
                                <input type="radio" name="status" value="1" data-am-ucheck required>驳回请求
                            </label>
                        </div>
                    </div>
                    <div class="am-form-group">
                        <label class="am-u-sm-2 am-u-lg-2">驳回理由 </label>
                        <div class="am-u-sm-9 am-u-end">
                            <textarea class="tpl-form-input  am-active hui_disa" style="height:100px;"  name="note" disabled  placeholder="不用填写"></textarea>
                        </div>
                    </div>
                    <div class="am-form-group">
                        <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                            <button type="button" data-id="{{id}}" class="am-btn am-btn-secondary inspect-submit">确认操作</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</script>
<script id="send_express_template">
<div class="inspect_boty" >
    <div class="row-content am-cf">
        <div class="row">
            <div class="widget-body">
                <div class="am-form tpl-form-line-form">
                    <div class="am-form-group">
                        <label class="am-u-sm-2 am-u-lg-2">快递公司 </label>
                        <div class="am-u-sm-9 am-u-end" style="margin-top:-10px;">
                            <label class="am-radio-inline" style="padding-left: 0px;">
                                <select id="express_name" style="padding:8px 30px 8px 10px">
                                    <option value=''>请选择快递公司</option>
                            <?php foreach( $express_list as $key => $val ) : ?>
                                    <option value='<?=$key?>'><?=$val?></option>
                            <?php endforeach; ?>
                                </select>
                            </lable>
                        </div>
                    </div>
                    <div class="am-form-group">
                        <label class="am-u-sm-2 am-u-lg-2">快递单号 </label>
                        <div class="am-u-sm-9 am-u-end">
                            <input type="text" class="tpl-form-input"  id="express_no" value="" placeholder="请输入快递编号" required>
                        </div>
                    </div>
                    <div class="am-form-group">
                        <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                            <button type="button" data-id="{{id}}" class="am-btn am-btn-secondary send-express-submit">确认发货</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</script>
<script id="express_info_template">
<div class="inspect_boty" style="height:500px;">
    <div class="row-content am-cf">
        <div class="row">
            <div class="widget-body">
                <div class="am-form tpl-form-line-form">
                    <div class="am-form-group">
                        <label class="am-u-sm-2 am-u-lg-2">物流公司 </label>
                        <div class="layui-input-block">{{express_name}}</div>
                    </div>
                    <div class="am-form-group">
                        <label class="am-u-sm-2 am-u-lg-2">快递单号 </label>
                        <div class="layui-input-block">{{express_no}}</div>
                    </div>
                    <div class="am-form-group">
                        <div class="express_titles">物流跟踪列表</div>
                        <div class="express_infos_lists">
                            <dl>{{list}}</dl> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</script>


<style>
	.jiyouliebiao{ margin:20px; height:405px; overflow-y:scroll}
	.jiyouliebiao dl{ width:100%; padding:10px 0px; margin:0px;float:left; border-bottom:1px solid #efefef;}
	.jiyouliebiao dl dd{ width:100%; height:100px;}
	.jiyouliebiao dl dt,.jiyouliebiao dl dt img{ width:100px; height:100px; float:left;}
	.jiyouliebiao dl dd{ width:calc(100% - 120px); float:right; height:100px;margin-top:0px;}
	.jiyouliebiao dl dd p{font-size:14px;margin:0px;margin-bottom:5px;}
	#show_log_ck{ padding:5px 10px;background:red; color:#fff; display:inline-block; font-size:12px; margin-left:30px;}
	.tongji_1{ margin:0px 0px 20px 0px;  border:1px solid #efefef; height:100px;}
	.tongji_1 .item{ width:25%; height:100px; text-align:center; float:left; }
	.tongji_1 .item p{ margin:0px; padding:0px; font-size:14px;}
	.tongji_1 .item p.item_t{ margin:20px 0px 5px; font-size:18px;}
</style>
<script id="template_data">
	<div class="jiyouliebiao">
	<div class="tongji_1">
		<div class="item">
			<p class="item_t" id="total_you">--</p>
			<p>共计用油</p>
		</div>
		<div class="item">
			<p class="item_t" id="huan_you">--</p>
			<p>换油</p>
		</div>
		<div class="item" >
			<p class="item_t" id="bu_you">--</p>
			<p>补油</p>
		</div>
		<div class="item" >
			<p class="item_t" id="shengyu">--</p>
			<p>剩余油量</p>
		</div>
	</div>
	<table width="100%" class="am-table am-table-compact am-table-striped tpl-table-black am-text-nowrap"><thead><tr><th>预约用户</th><th>服务类型</th><th>车型</th><th>排量</th><th>用油量</th><th>预约时间</th><th>联系人</th><th>联系电话</th></tr></thead><tbody>
	<tr><td colspan="8" class="am-text-center">暂无记录</td></tr>
	</tbody></table>
	</div>
</script>
<script>
	$("a.data_show").click(function(){
		layer.open({
		  type: 1,
		  title:'用油量数据统计',
		  skin: 'layui-layer-rim',
		  area: ['800px', '560px'],
		  content: $('#template_data').html()
		});
		var layerMsg = layer.msg("加载中", {
			title: false,
			time: false,
			shade: [0.5,'black'],
			icon: 16
		});
		$.get($(this).attr("href") , function(data){
			layer.close(layerMsg)
			var strhtml = '<div class="tongji_1"><div class="item"><p class="item_t" id="total_you">'+data.data.total_you+'升</p><p>共计用油</p></div><div class="item"><p class="item_t" id="huan_you">'+data.data.huan_you+'升</p><p>换油</p></div><div class="item" ><p class="item_t" id="bu_you">'+data.data.bu_you+'升</p><p>补油</p></div><div class="item" ><p class="item_t" id="bu_you">'+data.data.shengyu_you+'升</p><p>剩余油量</p></div></div>'
			strhtml += '<table width="100%" class="am-table am-table-compact am-table-striped tpl-table-black am-text-nowrap"><thead><tr><th>预约用户</th><th>服务类型</th><th>车型</th><th>排量</th><th>用油量</th><th>预约时间</th><th>联系人</th><th>联系电话</th></tr></thead><tbody>';
			if( data.list.length <= 0 ) {
				strhtml += '<tr><td colspan="8" class="am-text-center">暂无记录</td></tr>';
			} else {
				$.each(data.list , function(k,v){
					strhtml += '<tr><td>'+v.nickname+'</td><td>'+(v.type==1?'换油':'补油')+'</td><td>'+v.cart_brand+'</td><td>'+v.displacement+'L</td><td>'+v.use_capacity+'升</td><td>'+v.yuyue_time+'</td><td>'+v.contact+'</td><td>'+v.mobile+'</td></tr>'
				})
			}
			strhtml += '</tbody></table>';
			$(".jiyouliebiao").html(strhtml);
		},'json');
		return false;
	})
</script>

<script>
    $(function () {
        // 删除元素
        var url = "<?= url('shop.replenishment/datadelete') ?>";
        $('.item-delete').delete('id', url);

    });
    $("a.inspect-but").click(function(){
        layer.open({
            type: 1,
            title : '补油申请审核',
            skin: 'layui-layer-rim', //加上边框
            area: ['auto', 'auto'], //宽高
            anim: 3,
            shadeClose: true, //开启遮罩关闭
            content: $("#inspect_template").html().replace("{{id}}" , $(this).attr('data-id'))
          });
        return false;
    });
    $(document).on("click" , ".inspect-submit" , function(){
            var status = $('[name="status"]:checked').val();
            var note = $('[name="note"]').val();
            if( status == 1 && note == '' ) {
                layer.msg("请填写未审核通过的理由");
                return false;
            }
            $.post("<?php echo url("shop.replenishment/inspect")?>" , { id : $(this).data("id") , status : status , note : note } , function(data){
                if( data.code == 1 ) {
                    layer.alert(data.msg,function(){
                        window.location.reload();
                    });
                } else {
                    layer.msg(data.msg);
                }
            },'json');
    });
    $("a.send_express").click(function(){
        layer.open({
            type: 1,
            title : '快递发货',
            skin: 'layui-layer-rim', //加上边框
            area: ['auto', 'auto'], //宽高
            anim: 3,
            shadeClose: true, //开启遮罩关闭
            content: $("#send_express_template").html().replace("{{id}}" , $(this).attr('data-id'))
          });
        return false;
    });
    $(document).on("click" , ".send-express-submit" , function(){
            var express_name = $('#express_name').val();
            var express_no = $('#express_no').val();
            if( express_name == '' && express_no == '' ) {
                layer.msg("请选择快递公司名称或填写快递单号");
                return false;
            }
            $.post("<?php echo url("shop.replenishment/sendexpress")?>" , { id : $(this).data("id") , express_no : express_no , express_name : express_name } , function(data){
                if( data.code == 1 ) {
                    layer.alert(data.msg,function(){
                        window.location.reload();
                    });
                } else {
                    layer.msg(data.msg);
                }
            },'json');
    });
    $(document).on("click" , '[name="status"]' , function(){
        if( $(this).val() == 1 ) {
            $('[name="note"]').removeClass("hui_disa").removeAttr("disabled").attr('placeholder' , '请填写驳回的理由');
        } else {
            $('[name="note"]').addClass("hui_disa").attr({"disabled" : true , 'placeholder' : '不用填写'});
        }
    });
    
    $("a.express_info_list").click(function(){
        var loadingIndex = layer.load(2, { 
            shade: [0.5, 'gray'], 
            content: '加载中...'
        });
        $.get("<?php echo url('shop.replenishment/expressinfolist'); ?>" , { id : $(this).attr('data-id') } , function(data){
            layer.close(loadingIndex);
			var list_html = '';
			if( data.lists.code == 'OK' ) {
				$.each(data.lists.list , function(k,v){
					list_html += '<dd>'+v.content+'<span>'+v.time+'</span></dd>';
				});
			} else {
				list_html = '暂无数据';
			}
            layer.open({
                type: 1,
                title : '物流详情',
                skin: 'layui-layer-rim', //加上边框
                area: ['auto', 'auto'], //宽高
                anim: 3,
                shadeClose: true, //开启遮罩关闭
                content: $("#express_info_template").html().replace("{{express_name}}" , data.data.express_name).replace("{{express_no}}" , data.data.express_no).replace("{{list}}" , list_html)
            });
        },'json');
        return false;
    });
	$(".export_submit").click(function(){
		var url = decodeURIComponent(window.location.href).split("?s=");
		var url_uri = url[1].substr(1).split('/');
		var url_data = $('form').serialize().split("&");
		$.each(url_data , function(k,v){
			var uri_data = v.split("=");
			var floag = false;
			$.each(url_uri , function(m,n){
				if( m > 2 && m % 2 == 1 && uri_data[0] == n ) {
					floag = true;
					url_uri[m + 1] = uri_data[1];
				}
			});
			if( floag === false ) {
				url_uri.push( uri_data[0] + '/' + uri_data[1] );
			}
			
		});
		location.href = url[0] + '?s=' + '/'+url_uri.join("/").replace("<?php echo request()->action();?>" , 'export');
		return false;
	});
</script>

