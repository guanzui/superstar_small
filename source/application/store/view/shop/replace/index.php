<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">预约列表</div>
                </div>
                <div class="widget-body am-fr">
					<form action="" class="am-form tpl-form-line-form submit_forms" enctype="multipart/form-data" method="post">
                    <div class="am-u-sm-12 am-u-md-2 am-u-lg-2"></div>
					<div class="am-u-sm-12 am-u-md-10 am-u-lg-10" style="text-align: right;">
						<div class="am-btn-group am-btn-group-xs">
							<input type="text" class="tpl-form-input" name="merch" value="<?php echo input("param.merch");?>" placeholder="商家名称/商家ID">
						</div>
						<div class="am-btn-group am-btn-group-xs">
							<select name="type" required  data-am-selected="{searchBox: 1, btnSize: 'sm',  placeholder:'请选择服务类型'}">
								<option value="-1">请选择服务类型</option>
								<option value="1" <?php if( input('param.type') == 1 ) echo "selected" ?>>换油</option>
								<option value="2" <?php if( input('param.type') == 2 ) echo "selected" ?>>补油</option>
							</select>
						</div>
						<div class="am-btn-group am-btn-group-xs">
							<select name="status" required  data-am-selected="{searchBox: 1, btnSize: 'sm',  placeholder:'请选择状态'}">
								<option value="-1">请选择状态</option>
								<option value="0" <?php if( input('param.status') == "0" ) echo "selected" ?>>等待服务商审核</option>
								<option value="1" <?php if( input('param.status') == 1 ) echo "selected" ?>>服务商已确认</option>
								<option value="2" <?php if( input('param.status') == 2 ) echo "selected" ?>>服务已开始</option>
								<option value="3" <?php if( input('param.status') == 3 ) echo "selected" ?>>用户确认完成</option>
								<option value="4" <?php if( input('param.status') == 4 ) echo "selected" ?>>用户申请取消</option>
								<option value="5" <?php if( input('param.status') == 5 ) echo "selected" ?>>服务商同意取消</option>
								<option value="6" <?php if( input('param.status') == 6 ) echo "selected" ?>>拒绝服务</option>
							</select>
						</div>
						<button type="submit" class="j-submit am-btn am-btn-secondary fontsize15rem">搜索</button>
						<button type="button" class="export_submit am-btn am-btn-secondary fontsize15rem">导出</button>
					</div>
					</form>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>服务店面</th>
                                <th>预约用户</th>
                                <th>服务类型</th>
                                <th>用油量</th>
                                <th>车型</th>
                                <th>预约时间</th>
                                <th>联系人</th>
                                <th>联系电话</th>
                                <th>状态</th>
                                <th>创建时间</th>
                                <th width="160">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['id'] ?></td>
                                    <td class="am-text-middle"><?= $item['shop_name'] ?></td>
                                    <td class="am-text-middle"><?= $item['nickname'] ?></td>
                                    <td class="am-text-middle"><?php echo $item['type'] == 1?'换油':'补油';?></td>
                                    <td class="am-text-middle"><?php echo in_array($item['status'] , array(2,3)) ? $item['use_capacity'] . '升' : '-';?></td>
                                    <td class="am-text-middle"><?= $item['cart_brand'] ?></td>
                                    <td class="am-text-middle"><?= $item['yuyue_time'] ?> <?= $item['specific_time'] ?></td>
                                    <td class="am-text-middle"><?= $item['contact'] ?></td>
                                    <td class="am-text-middle"><?= $item['mobile'] ?></td>
                                    <td class="am-text-middle">
									<?php
										switch( $item['status'] ) {
											case 0 :
												echo "等待服务商审核";
											break;
											case 1 :
												echo "服务商已确认";
											break;
											case 2 :
												echo "服务已开始";
											break;
											case 3 :
												echo "用户确认完成";
											break;
											case 4 :
												echo "用户申请取消";
											break;
											case 5 :
												echo "服务商同意取消";
											break;
											case 6 :
												echo "拒绝服务";
											break;
										}
									?>
									</td>
                                    <td class="am-text-middle"><?php echo date("Y-m-d H:i:s" , $item['create_time']);?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <a href="<?= url('shop.replace/show',
                                                ['id' => $item['id']]) ?>">
                                                <i class="am-icon-pencil"></i> 查看
                                            </a>
                                            <a href="javascript:;" class="item-delete tpl-table-black-operation-del"
                                                data-id="<?= $item['id'] ?>">
                                                 <i class="am-icon-trash"></i> 删除
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="9" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        // 删除元素
        var url = "<?= url('shop.replace/delete') ?>";
        $('.item-delete').delete('id', url);

    });
</script>
<script>
$(".export_submit").click(function(){
	var url = decodeURIComponent(window.location.href).split("?s=");
	var url_uri = url[1].substr(1).split('/');
	var url_data = $('form').serialize().split("&");
	$.each(url_data , function(k,v){
		var uri_data = v.split("=");
		var floag = false;
		$.each(url_uri , function(m,n){
			if( m > 2 && m % 2 == 1 && uri_data[0] == n ) {
				floag = true;
				url_uri[m + 1] = uri_data[1];
			}
		});
		if( floag === false ) {
			url_uri.push( uri_data[0] + '/' + uri_data[1] );
		}
		
	});
	location.href = url[0] + '?s=' + '/'+url_uri.join("/").replace("<?php echo request()->action();?>" , 'export');
	return false;
});
</script>
