<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">预约信息查看</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">预约服务商 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock"><?=$info['shop_name']?> / <a href="javascript:;" class="shop_info_show" data-id="<?=$info['shop_id']?>">查看</a></span> 
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">预约用户 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock"><?=$info['nickname']?> / <a href="javascript:;" class="user_info_show" data-id="<?=$info['user_id']?>">查看</a></span> 
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">服务类型 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock"><?=$info['type']==1?'换油':'补油'?></span> 
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">车辆品牌 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock"><?=$info['cart_brand']?></span> 
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">联系人/电话 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock"><?=$info['contact']?> / <?=$info['mobile']?></span> 
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">行驶公里数 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock"><?=$info['mileage']?></span> 
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">车辆排量 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock"><?=$info['displacement']?> L</span> 
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">预约时间 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock"><?=$info['yuyue_time']?> <?=$info['specific_time']?> </span> 
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">预约使用产品 </label>
                                <div class="am-u-sm-9 am-u-end">
										<style>
											.yuyue_product{ width:525px; height:100px }
											.yuyue_product img{ width:100px; height:100px; float:left; margin-right:20px;}
											.yy_info{ width:400px; float:right}
											.yy_info_title{ width:100%; height:46px; line-height:160%; font-size:1.4rem; overflow:hidden;}
											.yy_info_attr{ width:100%; margin-top:6px; font-size:1.3rem; color:#666}
										</style>
										<div class="yuyue_product">
											<img src="<?=$info['product']['thumb']?>" />
											<div class="yy_info">
												<div class="yy_info_title"><?=$info['product']['title']?></div>
												<div class="yy_info_attr">品牌/型号：<?=$info['product']['brand']?> - <?=$info['product']['model_number']?></div>
												<div class="yy_info_attr">用油量：<?=$info['use_capacity']?>升</div>
											</div>
										</div>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">信息状态 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock">
										<?php
											switch( $info['status'] ) {
												case 0 :
													echo "等待服务商审核";
												break;
												case 1 :
													echo "服务商已确认";
												break;
												case 2 :
													echo "服务已开始";
												break;
												case 3 :
													echo "用户确认完成";
												break;
												case 4 :
													echo "用户申请取消";
												break;
												case 5 :
													echo "服务商同意取消";
												break;
												case 6 :
													echo "拒绝服务";
												break;
											}
										?> / <?=$info['is_comment'] == 1 ? '已评论' : '未评论'; ?> / <?php if( $info['is_settlement'] == 1 ){
											switch( $info['settlement_status'] ) {
												case 0 :
													echo "可申请结算";
												break;
												case 1 :
													echo "已申请结算";
												break;
												case 2 :
													echo "已拒绝结算";
												break;
												case 3 :
													echo "已完成结算";
												break;
											}
										} else{
											echo "不能结算";
										} ?>
									</span> 
                                </div>
                            </div>
							<?php if($info['status'] == 6 ):?>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">拒绝原因 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock"><?=$info['refuse_note']?></span> 
                                </div>
                            </div>
							<?php endif;?>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">创建时间 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock"><?=date('Y-m-d H:i:s' , $info['create_time'])?></span> 
                                </div>
                            </div>
							<?php if( $info['is_settlement'] == 1) : ?>
							<div class="widget-head am-cf">
                                <div class="widget-title am-fl">核算申请状态</div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">结算状态： </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock" style="width:100%">
										<?php
											switch($info['settlement_status']) {
												case 0:
													echo "等待商户申请";
												break;
												case 1:
													echo "等待平台审核";
												break;
												case 2:
													echo "审核失败已驳回";
												break;
												case 3:
													echo "审核通过";
												break;
											}
										?>
									</span> 
									<?php if( $info['settlement_status'] == 2 ) : ?>
                                    <span class="spanlineblock" style="width:100%;color:red">拒绝原因：</span> 
									<?php endif ;?>
                                </div>
                            </div>
							<?php if( $info['settlement_status'] == 2 ) : ?>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">拒绝原因： </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <span class="spanlineblock" style="width:100%">
										<?=$info['settlement_note']?>
									</span> 
                                </div>
                            </div>
							<?php endif ;?>
							<?php endif; ?>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="button" onclick="history.go(-1)" class="j-submit am-btn am-btn-secondary">返回
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</script>
