<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">换油服务评论</div>
                </div>
                <div class="widget-body am-fr">
					<form action="" class="am-form tpl-form-line-form submit_forms" enctype="multipart/form-data" method="post">
                    <div class="am-u-sm-12 am-u-md-2 am-u-lg-2"></div>
					<div class="am-u-sm-12 am-u-md-10 am-u-lg-10" style="text-align: right;">
						<div class="am-btn-group am-btn-group-xs">
							<input type="text" class="tpl-form-input" name="merch" value="<?php echo input("param.merch");?>" placeholder="商家名称/商家ID">
						</div>
						<div class="am-btn-group am-btn-group-xs">
							<select name="type" required  data-am-selected="{searchBox: 1, btnSize: 'sm',  placeholder:'评价类型'}">
								<option value="-1">评价类型</option>
								<option value="1" <?php if( input('param.type') == 1 ) echo "selected" ?>>差评</option>
								<option value="2" <?php if( input('param.type') == 2 ) echo "selected" ?>>中评</option>
								<option value="3" <?php if( input('param.type') == 3 ) echo "selected" ?>>好评</option>
							</select>
						</div>
						<div class="am-btn-group am-btn-group-xs">
							<select name="status" required  data-am-selected="{searchBox: 1, btnSize: 'sm',  placeholder:'评论状态'}">
								<option value="-1">评论状态</option>
								<option value="1" <?php if( input('param.status') == 1 ) echo "selected" ?>>显示</option>
								<option value="0" <?php if( input('param.status') == "0" ) echo "selected" ?>>屏蔽</option>
							</select>
						</div>
						<button type="submit" class="j-submit am-btn am-btn-secondary fontsize15rem">搜索</button>
						<button type="button" class="export_submit am-btn am-btn-secondary fontsize15rem">导出</button>
					</div>
					</form>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>服务商</th>
                                <th>评论用户</th>
                                <th>评论类型</th>
                                <th>服务态度</th>
                                <th>技术态度</th>
                                <th>评论内容</th>
                                <th>状态</th>
                                <th>创建时间</th>
                                <th width="160">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['id'] ?></td>
                                    <td class="am-text-middle"><?= $item['shop_name'] ?> / <a href="javascript:;" class="shop_info_show" data-id="<?=$item['shop_id']?>">查看</a></td>
                                    <td class="am-text-middle"><?= $item['nickname'] ?> / <a href="javascript:;" class="user_info_show" data-id="<?=$item['user_id']?>">查看</a></td>
                                    <td class="am-text-middle"><?= $item['type']==1?'差评':($item['type']==2?'中评':'好评');?></td>
                                    <td class="am-text-middle"><?= $item['service_star'] ?></td>
                                    <td class="am-text-middle"><?= $item['skill_star'] ?></td>
                                    <td class="am-text-middle" style="width:200px; white-space: initial !important"><?= $item['content'] ?></td>
                                    <td class="am-text-middle"><a href="<?php echo url("store/shop.replacecomment/infosave" , ['id' => $item['id'],'field' => 'status','value' => $item['status']==1?0:1])?>" menu="ajax_request" data-id="<?=$item['id']?>" style="color:<?php if( $item['status'] == 1 ) :?>green<?php else: ?>#999<?php endif;?>"><?php echo $item['status'] == 1?'显示':'禁用';?></a></td>
                                    <td class="am-text-middle"><?php echo date("Y-m-d H:i:s" , $item['create_time']);?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <a href="javascript:;" class="item-delete tpl-table-black-operation-del"
                                                data-id="<?= $item['id'] ?>">
                                                 <i class="am-icon-trash"></i> 删除
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="9" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        // 删除元素
        var url = "<?= url('shop.replacecomment/delete') ?>";
        $('.item-delete').delete('id', url);

    });
</script>
<script>
$(".export_submit").click(function(){
	var url = decodeURIComponent(window.location.href).split("?s=");
	var url_uri = url[1].substr(1).split('/');
	var url_data = $('form').serialize().split("&");
	$.each(url_data , function(k,v){
		var uri_data = v.split("=");
		var floag = false;
		$.each(url_uri , function(m,n){
			if( m > 2 && m % 2 == 1 && uri_data[0] == n ) {
				floag = true;
				url_uri[m + 1] = uri_data[1];
			}
		});
		if( floag === false ) {
			url_uri.push( uri_data[0] + '/' + uri_data[1] );
		}
		
	});
	location.href = url[0] + '?s=' + '/'+url_uri.join("/").replace("<?php echo request()->action();?>" , 'export');
	return false;
});
</script>
