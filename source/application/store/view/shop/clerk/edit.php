<style>
    .layui-layer-content{ height: auto !important}
</style>
<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">编辑店员</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">4s店列表 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <select name="clerk[shop_id]" required  data-am-selected="{searchBox: 1, btnSize: 'sm',  placeholder:'请选择4s店'}">
                                        <option value="">请选择4s店</option>
                                        <?php foreach( $shop as $v) : ?>
                                        <option value="<?=$v['shop_id']?>" <?php echo $v['shop_id'] == $save_data['shop_id'] ? 'selected' : ''; ?>><?=$v['shop_name']?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="am-form-group select_user_info" data-name="clerk[user_id]">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">店员 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" placeholder="请选择用户" value="<?php if( $save_data['user_id'] > 0) { echo $save_data['user_info']['nickname']; }?>" required style="width:50%;display: inline-block;" readonly="">
                                    <button type="button" style="padding:7px 15px; border:0px;display: inline-block " class="am-btn-secondary"><i class="iconfont sidebar-nav-link-logo icon-account" style=""></i> 选择用户</button>
                                    <div class="uploader-list am-cf">
                                        <?php if( $save_data['user_id'] > 0) :?>
                                        <div class="file-item">
                                            <img src="<?=$save_data['user_info']['avatar']?>">
                                            <input type="hidden" value="<?=$save_data['user_id']?>" name="shop[user_id]" />
                                            <i class="iconfont icon-shanchu file-item-user-delete"></i>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require">
                                    店员状态
                                </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="clerk[status]" value="1" data-am-ucheck required <?php echo $save_data['status'] == 1 ? 'checked' : '' ;?>>启用
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="clerk[status]" value="0" data-am-ucheck required <?php echo $save_data['status'] == 0 ? 'checked' : '' ;?>>禁用
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- 图片文件列表模板 -->
{{include file="layouts/_template/tpl_file_item" /}}

<!-- 文件库弹窗 -->
{{include file="layouts/_template/file_library" /}}

<script>
    $(function () {
        /**
         * 表单验证提交
         * @type {*}
         */
        $('#my-form').superForm();
    });
</script>
