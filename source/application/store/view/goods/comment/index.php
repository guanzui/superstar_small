<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">用户点评</div>
                </div>
                <div class="widget-body am-fr">
					<form action="<?=url("store/goods.comment/index");?>" class="am-form tpl-form-line-form submit_forms" enctype="multipart/form-data" method="post">
                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-6"></div>
					<div class="am-u-sm-12 am-u-md-6 am-u-lg-6" style="text-align: right;">
						<div class="am-btn-group am-btn-group-xs">
							<input type="number" class="tpl-form-input" name="user_id" value="<?=input('param.user_id')?>" placeholder="请输入用户ID">
						</div>
						<div class="am-btn-group am-btn-group-xs">
							<select name="type"  data-am-selected="{searchBox: 1, btnSize: 'sm',  placeholder:'请选择评论类型'}">
								<option value="0">全部</option>
								<option value="1" <?php if( input('param.type') == 1 ) echo "selected" ?>>差评</option>
								<option value="2" <?php if( input('param.type') == 2 ) echo "selected" ?>>中评</option>
								<option value="3" <?php if( input('param.type') == 3 ) echo "selected" ?>>好评</option>
							</select>
						</div>
						<button type="submit" class="j-submit am-btn am-btn-secondary fontsize15rem">搜索</button>
					</div>
					</form>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>点评用户</th>
                                <th>评分</th>
                                <th>商品名称</th>
                                <th>点评类型</th>
                                <th>内容</th>
                                <th>点评图片</th>
                                <th>状态</th>
                                <th>点评时间</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['nickname'] ?> <a href="javascript:;" class="user_info_show" data-id="<?=$item['user_id']?>">查看</a></td>
                                    <td class="am-text-middle"><?= $item['star'] ?></td>
                                    <td class="am-text-middle"><?= $item['goods_name'] ?> <a href="<?=url("store/goods/edit" , array("goods_id" => $item['goods_id']))?>">查看</a></td>
                                    <td class="am-text-middle"><?php echo $item['type'] == 3 ? '好评' : ($item['type'] == 2 ? '中评' : '差评'); ?></td>
                                    <td class="am-text-middle"><?= $item['content'] ?></td>
                                    <td class="am-text-middle"><?php if($item['thumb_list']):?><a href=""><img src="<?= $item['thumb_list'][0]?>" width="80"></a><?php else: ?>无<?php endif;?></td>
                                    <td class="am-text-middle"><a href="<?php echo url("store/goods.comment/infosave" , ['comment_id' => $item['comment_id'],'field' => 'status','value' => $item['status']==1?2:1])?>" menu="ajax_request" data-id="<?=$item['comment_id']?>" style="color:<?php if( $item['status'] == 2 ) :?>green<?php else: ?>#999<?php endif;?>"><?php echo $item['status'] == 2?'显示':($item['status']==1?'不显示':'未审核');?></a></td>
                                    <td class="am-text-middle"><?php echo date("Y-m-d H:i:s" , $item['create_time']);?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <a href="javascript:;" class="item-delete tpl-table-black-operation-del"
                                                data-id="<?= $item['comment_id'] ?>">
                                                 <i class="am-icon-trash"></i> 删除
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="9" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        // 删除元素
        var url = "<?= url('goods.comment/delete') ?>";
        $('.item-delete').delete('comment_id', url);

    });
</script>

