<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">出售中的商品</div>
                </div>
                <div class="widget-body am-fr">
                    <form action="<?=url("store/goods/index");?>" class="am-form tpl-form-line-form submit_forms" enctype="multipart/form-data" method="post">
                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-6">
                        <div class="am-form-group">
                            <div class="am-btn-toolbar">
                                <div class="am-btn-group am-btn-group-xs">
                                    <a class="am-btn am-btn-default am-btn-success am-radius"
                                       href="<?= url('goods/add') ?>">
                                        <span class="am-icon-plus"></span> 新增
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="am-u-sm-12 am-u-md-6 am-u-lg-6" style="text-align: right;">
                            <div class="am-btn-group am-btn-group-xs">
								<input type="text" class="tpl-form-input" name="title" value="<?=input('param.title')?>" placeholder="请输入检索名称">
							</div>
                            <div class="am-btn-group am-btn-group-xs">
                                <select name="goods_type"  data-am-selected="{searchBox: 1, btnSize: 'sm',  placeholder:'请选择类型'}">
                                    <option value="0">全部</option>
                                    <option value="1" <?php if( input('param.goods_type') == 1 ) echo "selected" ?>>商城商品</option>
                                    <option value="2" <?php if( input('param.goods_type') == 2 ) echo "selected" ?>>积分兑换</option>
                                </select>
                            </div>
                            <button type="submit" class="j-submit am-btn am-btn-secondary fontsize15rem">搜索</button>
                        </div>
					</form>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>商品ID</th>
                                <th>商品图片</th>
                                <th>商品名称</th>
                                <th>商品分类</th>
                                <th>实际销量</th>
                                <th>商品排序</th>
                                <th>商品状态</th>
                                <th>添加时间</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['goods_id'] ?></td>
                                    <td class="am-text-middle">
                                        <a href="<?= $item['thumb'] ?>"  title="点击查看大图" target="_blank">
                                            <img src="<?= $item['thumb'] ?>" width="50" height="50" alt="商品图片">
                                        </a>
                                    </td>
                                    <td class="am-text-middle">
                                        <p class="item-title"><?= $item['goods_name'] ?></p>
                                    </td>
                                    <td class="am-text-middle"><?= $item['category']['name'] ?></td>
                                    <td class="am-text-middle"><?= $item['sales_actual'] ?></td>
                                    <td class="am-text-middle"><?= $item['goods_sort'] ?></td>
                                    <td class="am-text-middle">
                                        <a href="<?php echo url("store/goods/infosave" , ['goods_id' => $item['goods_id'],'field' => 'goods_status','value' => $item['goods_status']['value']==10?20:10])?>" menu="ajax_request" data-id="<?=$item['goods_id']?>" style="color:<?php if( $item['goods_status']['value'] == 10 ) :?>green<?php else: ?>#999<?php endif;?>"><?= $item['goods_status']['text'] ?></a>
                                        / <a href="<?php echo url("store/goods/infosave" , ['goods_id' => $item['goods_id'],'field' => 'hot','value' => $item['hot']==1?0:1])?>" menu="ajax_request" data-id="<?=$item['goods_id']?>" style="color:<?php if( $item['hot'] == 1 ) :?>green<?php else: ?>#999<?php endif;?>">热门</a> 
                                        / <a href="<?php echo url("store/goods/infosave" , ['goods_id' => $item['goods_id'],'field' => 'rec','value' => $item['rec']==1?0:1])?>" menu="ajax_request" data-id="<?=$item['goods_id']?>" style="color:<?php if( $item['rec'] == 1 ) :?>green<?php else: ?>#999<?php endif;?>">推荐</a>
                                    </td>
                                    <td class="am-text-middle"><?= $item['create_time'] ?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <a href="<?= url('goods/edit',
                                                ['goods_id' => $item['goods_id']]) ?>">
                                                <i class="am-icon-pencil"></i> 编辑
                                            </a>
                                            <a href="javascript:;" class="item-delete tpl-table-black-operation-del"
                                               data-id="<?= $item['goods_id'] ?>">
                                                <i class="am-icon-trash"></i> 删除
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="9" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        // 删除元素
        var url = "<?= url('goods/delete') ?>";
        $('.item-delete').delete('goods_id', url);
    });
</script>

