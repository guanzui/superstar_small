<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">商城设置</div>
                            </div>
							
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">
                                    APP状态
                                </label>
                                <div class="am-u-sm-9">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="store[open]" value="1" data-am-ucheck required <?php if( !isset($values['open']) || $values['open'] == 1):?>checked<?php endif;?>>
                                        正常
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="store[open]" value="0" data-am-ucheck required <?php if( isset($values['open']) && $values['open'] == 0):?>checked<?php endif;?>>
                                            
                                        关闭
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">
                                    商城名称
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="store[name]"
                                           value="<?= isset($values['name']) ? $values['name'] : '' ?>" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    用户注册赠送积分
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="store[integral]" value="<?=$values['integral']?>">
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    邀请默认会员
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="store[manage_user_id]" value="<?=isset($values['manage_user_id'])?$values['manage_user_id']:''?>">
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    积分流通开关
                                </label>
                                <div class="am-u-sm-9">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="store[open_circulation]" value="1" data-am-ucheck <?php echo isset($values['open_circulation'])&&$values['open_circulation']==1?'checked':''?> required >开启
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="store[open_circulation]" value="0" data-am-ucheck <?php echo isset($values['open_circulation'])&&$values['open_circulation']==0?'checked':''?> required>关闭
                                    </label>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    最低提现积分
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="store[min_integral]" value="<?php echo isset($values['min_integral'])&&$values['min_integral']?$values['min_integral']:''?>">
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    提现购卡
                                </label>
                                <div class="am-u-sm-9">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="store[tx_open_cart]" value="1" data-am-ucheck <?php echo isset($values['tx_open_cart'])&&$values['tx_open_cart']==1?'checked':''?> required >开启
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="store[tx_open_cart]" value="0" data-am-ucheck <?php echo isset($values['tx_open_cart'])&&$values['tx_open_cart']==0?'checked':''?> required>关闭
                                    </label>
                                </div>
                            </div>
							
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    提现复购卡积分
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="store[tx_buy_price]" value="<?=isset($values['tx_buy_price'])?$values['tx_buy_price']:''?>">
									<div class="help-block am-u-sm-12">
										<small>当提现总积分度达到多少之后需要购买会员卡，提现购卡关闭时无效</small>
									</div>
								</div>
                            </div>
							
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    积分记录开关
                                </label>
                                <div class="am-u-sm-9">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="store[integral_log_open]" value="1" data-am-ucheck <?php echo isset($values['integral_log_open'])&&$values['integral_log_open']==1?'checked':''?> required >开启
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="store[integral_log_open]" value="0" data-am-ucheck <?php echo isset($values['integral_log_open'])&&$values['integral_log_open']==0?'checked':''?> required>关闭
                                    </label>
                                </div>
                            </div>
							
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    我的好友开关
                                </label>
                                <div class="am-u-sm-9">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="store[my_firend_open]" value="1" data-am-ucheck <?php echo isset($values['my_firend_open'])&&$values['my_firend_open']==1?'checked':''?> required >开启
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="store[my_firend_open]" value="0" data-am-ucheck <?php echo isset($values['my_firend_open'])&&$values['my_firend_open']==0?'checked':''?> required>关闭
                                    </label>
                                </div>
                            </div>
							
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    支付中心在线支付按钮
                                </label>
                                <div class="am-u-sm-9">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="store[paycenter_but]" value="1" data-am-ucheck <?php echo isset($values['paycenter_but'])&&$values['paycenter_but']==1?'checked':''?> required >开启
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="store[paycenter_but]" value="0" data-am-ucheck <?php echo isset($values['paycenter_but'])&&$values['paycenter_but']==0?'checked':''?> required>关闭
                                    </label>
                                </div>
                            </div>
							
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    在线开通会员开关
                                </label>
                                <div class="am-u-sm-9">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="store[online_open_user]" value="1" data-am-ucheck <?php echo isset($values['online_open_user'])&&$values['online_open_user']==1?'checked':''?> required >开启
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="store[online_open_user]" value="0" data-am-ucheck <?php echo isset($values['online_open_user'])&&$values['online_open_user']==0?'checked':''?> required>关闭
                                    </label>
                                </div>
                            </div>
							
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    延迟在线开通
                                </label>
                                <div class="am-u-sm-9">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="store[yc_online_open_user]" value="1" data-am-ucheck <?php echo isset($values['yc_online_open_user'])&&$values['yc_online_open_user']==1?'checked':''?> required >开启
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="store[yc_online_open_user]" value="0" data-am-ucheck <?php echo isset($values['yc_online_open_user'])&&$values['yc_online_open_user']==0?'checked':''?> required>关闭
                                    </label>
									<div class="help-block am-u-sm-12">
										<small>必须要 “在线开通会员开关” 打开的情况下 此开关打开才有效</small>
									</div>
                                </div>
                            </div>
							
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    延迟开通时间
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="store[yc_online_open_user_time]" value="<?php echo isset($values['yc_online_open_user_time'])?$values['yc_online_open_user_time']:''?>">
									<div class="help-block am-u-sm-12">
										<small>注意：单位为小时 不能有小数</small>
									</div>
								</div>
                            </div>
							
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    热门搜索
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="store[hot_search]" value="<?php echo isset($values['hot_search'])?$values['hot_search']:''?>">
									<div class="help-block am-u-sm-12">
										<small>热门搜索，可设置多个，已英文的","隔开</small>
									</div>
								</div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    平台电话
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="store[tel]" value="<?php echo isset($values['tel'])?$values['tel']:''?>">
								</div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    关于平台
                                </label>
                                <div class="am-u-sm-9">
                                    <textarea type="text" style="height:150px;" class="tpl-form-input" name="store[content]"><?php echo isset($values['content'])?$values['content']:''?></textarea>
								</div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    银行卡号
                                </label>
                                <div class="am-u-sm-9">
                                    <textarea type="text" style="height:150px;" class="tpl-form-input" name="store[blank]"><?php echo isset($values['blank'])?$values['blank']:''?></textarea>
									<div class="help-block am-u-sm-12">
										<small>可设置多个，一样一个信息，信息格式“开户人|大连市中国银行支行|5123321562232151”</small>
									</div>
								</div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    提现银行
                                </label>
                                <div class="am-u-sm-9">
                                    <textarea type="text" style="height:150px;" class="tpl-form-input" name="store[tx_blank]"><?php echo isset($values['tx_blank'])?$values['tx_blank']:''?></textarea>
									<div class="help-block am-u-sm-12">
										<small>可设置多个，一样一个信息</small>
									</div>
								</div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">支付宝收款码 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <button type="button" id="zhifubao_upload" class="upload-file am-btn am-btn-secondary am-radius"><i class="am-icon-cloud-upload"></i> 请选择图片</button>
                                        <div class="uploader-list am-cf">
											<?php if( isset( $values['alipay'] ) ) : ?>
                                                <div class="file-item">
                                                    <img src="<?= $values['alipay']?>">
                                                    <input type="hidden" name="store[alipay]"
                                                           value="<?= $values['alipay']?>">
                                                    <i class="iconfont icon-shanchu file-item-delete"></i>
                                                </div>
                                            <?php endif; ?>
										</div>
                                    </div>
                                    <div class="help-block am-margin-top-sm">
                                        <small>尺寸200x200像素以上，大小1M以下</small>
                                    </div>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">微信收款码 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <button type="button" id="weixin_upload" class="upload-file am-btn am-btn-secondary am-radius"><i class="am-icon-cloud-upload"></i> 请选择图片</button>
                                        <div class="uploader-list am-cf">
											<?php if( isset( $values['wechat'] ) ) : ?>
                                                <div class="file-item">
                                                    <img src="<?= $values['wechat']?>">
                                                    <input type="hidden" name="store[wechat]"
                                                           value="<?= $values['wechat']?>">
                                                    <i class="iconfont icon-shanchu file-item-delete"></i>
                                                </div>
                                            <?php endif; ?>
										</div>
                                    </div>
                                    <div class="help-block am-margin-top-sm">
                                        <small>尺寸200x200像素以上，大小1M以下</small>
                                    </div>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    版本号
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="store[version]" value="<?=isset($values['version'])?$values['version']:''?>">
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    安卓下载地址
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="store[android_down_url]" value="<?=isset($values['android_down_url'])?$values['android_down_url']:''?>">
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    苹果下载地址
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="store[ios_down_url]" value="<?=isset($values['ios_down_url'])?$values['ios_down_url']:''?>">
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- 图片文件列表模板 -->
{{include file="layouts/_template/tpl_file_item" /}}

<!-- 文件库弹窗 -->
{{include file="layouts/_template/file_library" /}}
<script>
    $(function () {

        /**
         * 表单验证提交
         * @type {*}
         */
        $('#my-form').superForm();
		 $('#zhifubao_upload').selectImages({
            name: 'store[alipay]',type:2
        });
        $('#weixin_upload').selectImages({
            name: 'store[wechat]',type:2
        });
    });
</script>
