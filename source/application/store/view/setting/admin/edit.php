<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">管理员修改</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">登录账户 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="admin[user_name]" value="<?php echo $save_data['user_name'];?>" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">登录密码 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="admin[password]" value="">
									<div class="help-block am-u-sm-12">
										<small>如果不需要修改密码，请留空。</small>
									</div>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">姓名 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="admin[name]" value="<?php echo $save_data['name'];?>" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">所属分组 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <select name="admin[role_id]" data-am-selected="{searchBox: 1, btnSize: 'sm'}">
                                        <?php if (isset($role_list)): foreach ($role_list as $key => $first): ?>
                                            <option value="<?= $first['role_id'] ?>" <?php if( $save_data['role_id'] == $first['role_id']) echo "selected"; ?>><?= $first['name'] ?></option>
                                        <?php endforeach; endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require">
                                    状态
                                </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="admin[status]" value="1" data-am-ucheck required <?php if( $save_data['status'] == 1 ) echo "checked";?>>正常
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="admin[status]" value="0" data-am-ucheck required <?php if( $save_data['status'] == 0 ) echo "checked";?>>禁用
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
								<input type="hidden" value="<?=$save_data['store_user_id']?>" name="id" /> 
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        /**
         * 表单验证提交
         * @type {*}
         */
        $('#my-form').superForm();
    });
</script>
