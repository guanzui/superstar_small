<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">添加菜单</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">菜单名称 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="menu[title]" value="" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">菜单图片 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <button type="button" class="upload-file am-btn am-btn-secondary am-radius">
                                            <i class="am-icon-cloud-upload"></i> 选择图片
                                        </button>
                                        <div class="uploader-list am-cf">
                                        </div>
                                    </div>
                                    <div class="help-block am-margin-top-sm">
                                        <small>尺寸100x100像素以上，大小1M以下</small>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require">
                                    链接跳转类型
                                </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="menu[target_type]" value="1" data-am-ucheck required checked>商品
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="menu[target_type]" value="2" data-am-ucheck required>分类
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="menu[target_type]" value="3" data-am-ucheck required>文章
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="menu[target_type]" value="4" data-am-ucheck required>外部链接
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group" id="goods">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">商品 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <select name="goods" data-am-selected="{searchBox: 1, btnSize: 'sm'}">
                                        <?php if (isset($goods_list)): foreach ($goods_list as $key => $first): ?>
                                            <option value="<?= $first['goods_id'] ?>">
                                                <?= $first['goods_name'] ?></option>
                                        <?php endforeach; endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="am-form-group" style="display: none;" id="cat">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">分类 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <select name="cat" data-am-selected="{searchBox: 1, btnSize: 'sm'}">
                                        <?php if (isset($cat_list)): foreach ($cat_list as $key => $first): ?>
                                            <option value="<?= $first['category_id'] ?>">
                                                <?= $first['name'] ?></option>
                                        <?php endforeach; endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="am-form-group" style="display: none;" id="article">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">文章 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <select name="article" data-am-selected="{searchBox: 1, btnSize: 'sm'}">
                                        <?php if (isset($article_list)): foreach ($article_list as $key => $first): ?>
                                            <option value="<?= $first['article_id'] ?>">
                                                <?= $first['title'] ?></option>
                                        <?php endforeach; endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="am-form-group" style="display: none;" id="url">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">外部链接 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="url" value="100" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">分类排序 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="number" class="tpl-form-input" name="menu[sort]"
                                           value="100" required>
                                    <small>数字越小越靠前</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require">
                                    菜单状态
                                </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="menu[status]" value="1" data-am-ucheck required chekced>正常
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="menu[status]" value="0" data-am-ucheck required>禁用
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- 图片文件列表模板 -->
{{include file="layouts/_template/tpl_file_item" /}}

<!-- 文件库弹窗 -->
{{include file="layouts/_template/file_library" /}}

<script>
    $(function () {

        // 选择图片
        $('.upload-file').selectImages({
            name: 'menu[thumb]',type:2
        });

        /**
         * 表单验证提交
         * @type {*}
         */
        $('#my-form').superForm();
        
        $("[name='menu[target_type]']").click(function(){
            $("#goods").hide();$("#cat").hide();$("#url").hide(); $("#article").hide();
            $(this).val()==1?$("#goods").show():($(this).val()==2?$("#cat").show():($(this).val()==3?$("#article").show():$("#url").show()));
        });
    });
</script>
