<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">编辑广告</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">广告名称 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="ads[title]"
                                           value="<?= $save_data['title'] ?>" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">所属分组 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <select name="ads[type]" data-am-selected="{searchBox: 1, btnSize: 'sm'}">
                                        <?php if (isset($type_list)): foreach ($type_list as $key => $first): ?>
                                            <option value="<?= $key ?>" <?php if( $save_data['type'] == $key):?>selected<?php endif; ?>><?= $first ?></option>
                                        <?php endforeach; endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">分类图片 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <button type="button"
                                                class="upload-file am-btn am-btn-secondary am-radius">
                                            <i class="am-icon-cloud-upload"></i> 选择图片
                                        </button>
                                        <div class="uploader-list am-cf">
                                            <?php if ($save_data['thumb']): ?>
                                                <div class="file-item">
                                                    <img src="<?= $save_data['thumb'] ?>">
                                                    <input type="hidden" name="ads[thumb]" value="<?= $save_data['thumb'] ?>">
                                                    <i class="iconfont icon-shanchu file-item-delete"></i>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="help-block am-margin-top-sm">
                                        <small>尺寸650x300像素以上，大小2M以下</small>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require">
                                    链接跳转类型
                                </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="ads[target_type]" value="1" data-am-ucheck required <?php if( $save_data['target_type'] == 1):?>checked<?php endif;?>>商品
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="ads[target_type]" value="2" data-am-ucheck required <?php if( $save_data['target_type'] == 2):?>checked<?php endif;?>>分类
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="ads[target_type]" value="4" data-am-ucheck required <?php if( $save_data['target_type'] == 4):?>checked<?php endif;?>>外部链接
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group" id="goods" style="display: <?php if( $save_data['target_type'] == 1):?>block<?php else: ?>none<?php endif;?>">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">商品 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <select name="goods" data-am-selected="{searchBox: 1, btnSize: 'sm'}">
                                        <?php if (isset($goods_list)): foreach ($goods_list as $key => $first): ?>
                                            <option value="<?= $first['goods_id'] ?>" <?php if( $first['goods_id'] == $save_data['target_data']):?>selected<?php endif;?>><?= $first['goods_name'] ?></option>
                                        <?php endforeach; endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="am-form-group" style="display: <?php if( $save_data['target_type'] == 2):?>block<?php else: ?>none<?php endif;?>;" id="cat">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">分类 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <select name="cat" data-am-selected="{searchBox: 1, btnSize: 'sm'}">
                                        <?php if (isset($cat_list)): foreach ($cat_list as $key => $first): ?>
                                            <option value="<?= $first['category_id'] ?>" <?php if( $first['category_id'] == $save_data['target_data']):?>selected<?php endif;?>><?= $first['name'] ?></option>
                                        <?php endforeach; endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="am-form-group" style="display: <?php if( $save_data['target_type'] == 3):?>block<?php else: ?>none<?php endif;?>;" id="article">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">文章 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <select name="article" data-am-selected="{searchBox: 1, btnSize: 'sm'}">
                                        <?php if (isset($article_list)): foreach ($article_list as $key => $first): ?>
                                            <option value="<?= $first['article_id'] ?>" <?php if( $first['article_id'] == $save_data['target_data']):?>selected<?php endif;?>><?= $first['title'] ?></option>
                                        <?php endforeach; endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="am-form-group" style="display: <?php if( $save_data['target_type'] == 4):?>block<?php else: ?>none<?php endif;?>;" id="url">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">外部链接 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="url" value="<?=$save_data['target_data']?>" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">分类排序 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="number" class="tpl-form-input" name="ads[sort]"
                                           value="<?= $save_data['sort'] ?>" required>
                                    <small>数字越小越靠前</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require">
                                    广告状态
                                </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="ads[status]" value="1" data-am-ucheck required <?php if( $save_data['status'] == 1):?>checked<?php endif;?>>正常
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="ads[status]" value="0" data-am-ucheck required <?php if( $save_data['status'] == 0):?>checked<?php endif;?>>禁用
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <input type="hidden" value="<?=$save_data['ads_id']?>" name="id" />
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- 图片文件列表模板 -->
{{include file="layouts/_template/tpl_file_item" /}}

<!-- 文件库弹窗 -->
{{include file="layouts/_template/file_library" /}}

<script>
    $(function () {

        // 选择图片
        $('.upload-file').selectImages({
            name: 'ads[thumb]',type:2
        });

        /**
         * 表单验证提交
         * @type {*}
         */
        $('#my-form').superForm();
        
        $("[name='ads[target_type]']").click(function(){
            $("#goods").hide();$("#cat").hide();$("#url").hide(); $("#article").hide();
            $(this).val()==1?$("#goods").show():($(this).val()==2?$("#cat").show():($(this).val()==3?$("#article").show():$("#url").show()));
        });

    });
</script>
