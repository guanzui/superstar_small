<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">分销设置</div>
                            </div>
							
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require"> 会员卡积分 </label>
                                <div class="am-u-sm-10">
                                    <input type="text" class="tpl-form-input" name="distribution[order_price]" value="<?=$values['order_price'] ?>" required>
                                    <small>前端用户购买会员卡的积分</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require"> 免费换油次数 </label>
                                <div class="am-u-sm-10">
                                    <input type="text" class="tpl-form-input" name="distribution[order_consumption_num]" value="<?=isset($values['order_consumption_num'])?$values['order_consumption_num']:'' ?>" required>
                                    <small>会员卡换油的次数，补油默认为无限次</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require">
                                    是否开启分销
                                </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="distribution[open]" value="1" data-am-ucheck required <?php if( $values['open'] == 1):?>checked<?php endif;?>>
                                        开启
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="distribution[open]" value="0" data-am-ucheck required <?php if( $values['open'] == 0):?>checked<?php endif;?>>
                                            
                                        关闭
                                    </label>
                                </div>
                            </div>
                            
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require"> 复购奖励 </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline" style="padding-left:0px;">
                                        <input type="text" class="tpl-form-input" name="distribution[repeat_buy][level]" value="<?=$values['repeat_buy']['level'] ?>" required>
                                        <small>奖励之上的关系层级</small>
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="text" class="tpl-form-input" name="distribution[repeat_buy][price]" value="<?=$values['repeat_buy']['price'] ?>" required>
                                        <small>获得的奖励积分</small>
                                    </label>
                                    分
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require"> 提现感恩奖 </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline" style="padding-left:0px;">
                                        <input type="text" class="tpl-form-input" name="distribution[cash_withdrawal][level]" value="<?=$values['cash_withdrawal']['level'] ?>" required>
                                        <small>提现感恩奖向上层级</small>
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="text" class="tpl-form-input" name="distribution[cash_withdrawal][price]" value="<?=$values['cash_withdrawal']['price'] ?>" required>
                                        <small>获得的奖励积分比例</small>
                                    </label>
                                    %
                                </div>
                            </div>
							<div class="widget-head am-cf">
                                <div class="widget-title am-fl">普通会员规则设置</div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require">
                                    是否开启普通会员模式
                                </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="distribution[member][open]" value="1" data-am-ucheck required <?php if( $values['member']['open'] == 1):?>checked<?php endif;?>>
                                        开启
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="distribution[member][open]" value="0" data-am-ucheck required <?php if( $values['member']['open'] == 0):?>checked<?php endif;?>>
                                        关闭
                                    </label>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require"> 直推抽成 </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="text" class="tpl-form-input" name="distribution[member][direct][value]" value="<?=$values['member']['direct']['value'] ?>" required>
                                    </label>
                                    <span>
                                        分
                                    </span>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require"> 间推抽成 </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="text" class="tpl-form-input" name="distribution[member][indirect][value]" value="<?=$values['member']['indirect']['value'] ?>" required>
                                    </label>
                                    <span>
                                        分
                                    </span>
                                </div>
                            </div>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">业务员规则设置</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require"> 直推抽成 </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="distribution[salesman][direct][type]" value="1" id="choucheng" data-am-ucheck required <?php if( $values['salesman']['direct']['type'] == 1 ):?>checked<?php endif;?>>
                                        积分
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="distribution[salesman][direct][type]" value="2" id="choucheng" data-am-ucheck required <?php if( $values['salesman']['direct']['type'] == 2 ):?>checked<?php endif;?>>
                                        比例
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="text" class="tpl-form-input" name="distribution[salesman][direct][value]" value="<?=$values['salesman']['direct']['value'] ?>" required>
                                    </label>
                                    <span>
                                        <?php if( $values['salesman']['direct']['type'] == 1 ):?>分<?php else: ?>%<?php endif;?>
                                    </span>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require"> 间推抽成 </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="distribution[salesman][indirect][type]" id="choucheng" value="1" data-am-ucheck required <?php if( $values['salesman']['indirect']['type'] == 1 ):?>checked<?php endif;?>>
                                        积分
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="distribution[salesman][indirect][type]" id="choucheng" value="2" data-am-ucheck required <?php if( $values['salesman']['indirect']['type'] == 2 ):?>checked<?php endif;?>>
                                        比例
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="text" class="tpl-form-input" name="distribution[salesman][indirect][value]" value="<?=$values['salesman']['indirect']['value'] ?>" required>
                                    </label>
                                    <span>
                                        <?php if( $values['salesman']['indirect']['type'] == 1 ):?>分<?php else: ?>%<?php endif;?>
                                    </span>
                                </div>
                            </div>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">销售经理规则</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require"> 直推升级经理 </label>
                                <div class="am-u-sm-10">
                                    <input type="text" class="tpl-form-input" name="distribution[salesmanager][direct_upgrade]" value="<?=$values['salesmanager']['direct_upgrade'] ?>" required>
                                    <small>这里设置直推达到多少单之后升级条件</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require"> 直推抽成 </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="distribution[salesmanager][direct][type]" value="1" id="choucheng" data-am-ucheck required <?php if( $values['salesmanager']['direct']['type'] == 1 ):?>checked<?php endif;?>>
                                        积分
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="distribution[salesmanager][direct][type]" value="2" id="choucheng" data-am-ucheck required <?php if( $values['salesmanager']['direct']['type'] == 2 ):?>checked<?php endif;?>>
                                        比例
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="text" class="tpl-form-input" name="distribution[salesmanager][direct][value]" value="<?=$values['salesmanager']['direct']['value'] ?>" required>
                                    </label>
                                    <span>
                                        <?php if( $values['salesmanager']['direct']['type'] == 1 ):?>分<?php else: ?>%<?php endif;?>
                                    </span>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require"> 间推抽成 </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="distribution[salesmanager][indirect][type]" id="choucheng" value="1" data-am-ucheck required <?php if( $values['salesmanager']['indirect']['type'] == 1 ):?>checked<?php endif;?>>
                                        积分
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="distribution[salesmanager][indirect][type]" id="choucheng" value="2" data-am-ucheck required <?php if( $values['salesmanager']['indirect']['type'] == 2 ):?>checked<?php endif;?>>
                                        比例
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="text" class="tpl-form-input" name="distribution[salesmanager][indirect][value]" value="<?=$values['salesmanager']['indirect']['value'] ?>" required>
                                    </label>
                                    <span>
                                        <?php if( $values['salesmanager']['indirect']['type'] == 1 ):?>分<?php else: ?>%<?php endif;?>
                                    </span>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require"> 旗下业务员每单抽成 </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="distribution[salesmanager][teammembers][type]" id="choucheng" value="1" data-am-ucheck required <?php if( $values['salesmanager']['teammembers']['type'] == 1 ):?>checked<?php endif;?>>
                                        积分
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="distribution[salesmanager][teammembers][type]" id="choucheng" value="2" data-am-ucheck required <?php if( $values['salesmanager']['teammembers']['type'] == 2 ):?>checked<?php endif;?>>
                                        比例
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="text" class="tpl-form-input" name="distribution[salesmanager][teammembers][value]" value="<?=$values['salesmanager']['teammembers']['value'] ?>" required>
                                    </label>
                                    <span>
                                        <?php if( $values['salesmanager']['teammembers']['type'] == 1 ):?>分<?php else: ?>%<?php endif;?>
                                    </span>
                                </div>
                            </div>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">总经理规则</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require"> 升级总经理 </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline" style="padding-left:0px;">
                                        <input type="text" class="tpl-form-input" name="distribution[generalmanager][my_direct_num]" value="<?=$values['generalmanager']['my_direct_num'] ?>" required>
                                        <small>自己直推订单数量</small>
                                    </label>
                                    <label class="am-radio-inline" style="padding-left:0px;">
                                        <input type="text" class="tpl-form-input" name="distribution[generalmanager][team_direct_num]" value="<?=$values['generalmanager']['team_direct_num'] ?>" required>
                                        <small>2级团队直推订单数量</small>
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require"> 直推抽成 </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="distribution[generalmanager][direct][type]" value="1" id="choucheng" data-am-ucheck required <?php if( $values['generalmanager']['direct']['type'] == 1 ):?>checked<?php endif;?>>
                                        积分
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="distribution[generalmanager][direct][type]" value="2" id="choucheng" data-am-ucheck required <?php if( $values['generalmanager']['direct']['type'] == 2 ):?>checked<?php endif;?>>
                                        比例
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="text" class="tpl-form-input" name="distribution[generalmanager][direct][value]" value="<?=$values['generalmanager']['direct']['value'] ?>" required>
                                    </label>
                                    <span>
                                        <?php if( $values['generalmanager']['direct']['type'] == 1 ):?>分<?php else: ?>%<?php endif;?>
                                    </span>
                                </div>
                            </div>
                            
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require"> 旗下经理抽成 </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="distribution[generalmanager][teammanager][type]" value="1" id="choucheng" data-am-ucheck required <?php if( $values['generalmanager']['teammanager']['type'] == 1 ):?>checked<?php endif;?>>
                                        积分
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="distribution[generalmanager][teammanager][type]" value="2" id="choucheng" data-am-ucheck required <?php if( $values['generalmanager']['teammanager']['type'] == 2 ):?>checked<?php endif;?>>
                                        比例
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="text" class="tpl-form-input" name="distribution[generalmanager][teammanager][value]" value="<?=$values['generalmanager']['teammanager']['value'] ?>" required>
                                    </label>
                                    <span>
                                        <?php if( $values['generalmanager']['teammanager']['type'] == 1 ):?>分<?php else: ?>%<?php endif;?>
                                    </span>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require"> 旗下业务员每单抽成 </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="distribution[generalmanager][teammembers][type]" id="choucheng" value="1" data-am-ucheck required <?php if( $values['generalmanager']['teammembers']['type'] == 1 ):?>checked<?php endif;?>>
                                        积分
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="distribution[generalmanager][teammembers][type]" id="choucheng" value="2" data-am-ucheck required <?php if( $values['generalmanager']['teammembers']['type'] == 2 ):?>checked<?php endif;?>>
                                        比例
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="text" class="tpl-form-input" name="distribution[generalmanager][teammembers][value]" value="<?=$values['generalmanager']['teammembers']['value'] ?>" required>
                                    </label>
                                    <span>
                                        <?php if( $values['generalmanager']['teammembers']['type'] == 1 ):?>分<?php else: ?>%<?php endif;?>
                                    </span>
                                </div>
                            </div>
							<div class="widget-head am-cf">
                                <div class="widget-title am-fl">团队长规则</div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require"> 申请团队长的条件 </label>
                                <div class="am-u-sm-10">
									<input type="text" class="tpl-form-input" name="distribution[item][condition]" value="<?=!isset($values['item'])?'':$values['item']['condition'] ?>" required>
									<small>所属下属团队总共达到的业绩单位/万元</small>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require"> 提成团队长的层级 </label>
                                <div class="am-u-sm-10">
									<input type="text" class="tpl-form-input" name="distribution[item][level]" value="<?=!isset($values['item'])?'':$values['item']['level'] ?>" required>
									<small>团队长提成的层级数量</small>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require"> 团队长提成比例 </label>
                                <div class="am-u-sm-10">
									<textarea class="tpl-form-input" name="distribution[item][proportion]" required><?=!isset($values['item'])?'':str_replace("," , "\r\n" , $values['item']['proportion']) ?></textarea>
									<small>一行一个，例如设置团队长层级位3级  则这里需要填写三个比例参数</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('#my-form').superForm();
    });
    $("input#choucheng").click(function(){
        $(this).closest("div").find("span:last").html($(this).val()==1?'元':'%');
    })
</script>
