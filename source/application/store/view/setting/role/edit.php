<link rel="stylesheet" href="/assets/store/tree/style.min.css"/>
<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">角色修改</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">角色名称 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="role[name]" value="<?php echo $save_data['name'];?>" required>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require">
                                    角色类型
                                </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="role[type]" value="1" data-am-ucheck required <?php if( $save_data['type'] == 1 ) echo "checked";?>>普通用户组
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="role[type]" value="2" data-am-ucheck required <?php if( $save_data['type'] == 2 ) echo "checked";?>>超级管理组
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">权限列表 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div id="jstree"></div>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-2 am-form-label form-require">
                                    状态
                                </label>
                                <div class="am-u-sm-10">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="role[status]" value="1" data-am-ucheck required <?php if( $save_data['status'] == 1 ) echo "checked";?>>正常
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="role[status]" value="0" data-am-ucheck required <?php if( $save_data['status'] == 0 ) echo "checked";?>>禁用
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
								<input type="hidden" name="id" value="<?php echo $save_data['role_id'];?>" />
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="/assets/store/tree/jstree.min.js"></script>
<script>
    $(function () {

        var $jstree = $('#jstree');
        $jstree.jstree({
            icon: false,
            plugins: ['checkbox'],
            core: {
                themes: {icons: false},
                checkbox: {
                    keep_selected_style: false
                },
                data: <?php echo json_encode($menu_data); ?>
			}
        });

        // 读取选中的条目
        $.jstree.core.prototype.get_all_checked = function (full) {
            var obj = this.get_selected(), i, j;
            for (i = 0, j = obj.length; i < j; i++) {
                obj = obj.concat(this.get_node(obj[i]).parents);
            }
            obj = $.grep(obj, function (v) {
                return v !== '#';
            });
            obj = obj.filter(function (itm, i, a) {
                return i === a.indexOf(itm);
            });
            return full ? $.map(obj, $.proxy(function (i) {
                return this.get_node(i);
            }, this)) : obj;
        };

        /**
         * 表单验证提交
         * @type {*}
         */
        $('#my-form').superForm({
            buildData: function () {
                return {
                    role: {
                        role_data: $jstree.jstree('get_all_checked')
                    }
                }
            }
        });

    });
</script>