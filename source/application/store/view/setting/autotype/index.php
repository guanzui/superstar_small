<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">汽车型号列表</div>
                </div>
                <div class="widget-body am-fr">
                    <form action="" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                        <div class="am-u-sm-12 am-u-md-6 am-u-lg-6">
                            <a class="am-btn am-btn-default am-btn-success am-radius fontsize15rem"href="<?= url('setting.autotype/add') ?>">
                                 <span class="am-icon-plus"></span> 新增
                             </a>
                        </div>
                    </form>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>型号名称</th>
                                <th>所属品牌</th>
                                <th>品牌图标</th>
                                <th>简介</th>
                                <th>创建时间</th>
                                <th width="160">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['id'] ?></td>
                                    <td class="am-text-middle"><?= $item['title'] ?></td>
                                    <td class="am-text-middle"><?= $item['brand_name'] ?></td>
                                    <td class="am-text-middle"><img src="<?= $item['thumb'] ?>" width="40" /></td>
                                    <td class="am-text-middle" ><?= $item['desc']?></td>
                                    <td class="am-text-middle"><?php echo date("Y-m-d H:i:s" , $item['create_time']);?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <a href="<?= url('setting.autotype/edit',
                                                ['id' => $item['id']]) ?>">
                                                <i class="am-icon-pencil"></i> 编辑
                                            </a>
                                            <a href="javascript:;" class="item-delete tpl-table-black-operation-del"
                                                data-id="<?= $item['id'] ?>">
                                                 <i class="am-icon-trash"></i> 删除
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="9" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        // 删除元素
        var url = "<?= url('setting.autotype/delete') ?>";
        $('.item-delete').delete('id', url);

    });
</script>

