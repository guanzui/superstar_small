<style>
	.tpl-content-wrapper{ margin-left:160px; }
	.count_ui_info { width:100%; margin-bottom:20px;}
	.count_ui_info_item{ width:calc(25% - 20px); float:left; margin-right:20px; border:1px solid #efefef; border-top:3px solid #efefef}
	.count_ui_info_item:last-child{ margin:0px; }
	.data_3_list{ width:calc(33.33% - 20px); }
	.list_2_item{ width:calc(20% - 20px);}
	.cui_data{ width:100%;text-align:center; display:block; font-size:3rem;  padding:20px 0px 0px }
	.cui_data_title{ width:100%; display:block; text-align:center; padding-bottom:20px; }
	.left_div_data,.right_div_data{ margin:10px 0px}
	.left_div_data{ width:calc(50% - 1px); float:left; border-right:1px dashed #ccc; }
	.right_div_data{ width:50%; float:right;}
</style>

<div class="row-content am-cf">
    <div class="widget am-cf">
        <div class="widget-body">
            <div class="tpl-page-state am-margin-top-xl" style="    margin-top: 1.5rem;">
				<div class="count_ui_info">
					<div class="count_ui_info_item">
					<a href="<?=url('store/shop.yuyueauditing/index');?>">
						<span class="cui_data"><?=$count['sh']?></span>
						<span class="cui_data_title">商家结算待审核</span>
					</a>
					</div>
					<div class="count_ui_info_item">
					<a href="<?=url('store/finance.cashwithdrawal/index');?>">
						<span class="cui_data"><?=$count['tx']?></span>
						<span class="cui_data_title">用户提现待审核</span>
					</a>
					</div>
					<div class="count_ui_info_item">
					<a href="<?=url('store/shop.replace/index');?>">
						<span class="cui_data"><?=$count['rjff']?></span>
						<span class="cui_data_title">今日服务</span>
					</a>
					</div>
					<div class="count_ui_info_item">
					<a href="<?=url('store/order/delivery_list');?>">
						<span class="cui_data"><?=$count['rjdd']?></span>
						<span class="cui_data_title">今日订单</span>
					</a>
					</div>
					<div style="clear:both"></div>
				</div>
				<div class="count_ui_info">
					<div class="count_ui_info_item list_2_item">
						<span class="cui_data"><?=$count['user_cart']?></span>
						<span class="cui_data_title">开通会员卡人数</span>
					</div>
					<div class="count_ui_info_item list_2_item">
						<span class="cui_data"><?=$count['fy']?></span>
						<span class="cui_data_title">分佣总额</span>
					</div>
					<div class="count_ui_info_item list_2_item">
						<span class="cui_data"><?=$count['yw']?></span>
						<span class="cui_data_title">业务员</span>
					</div>
					<div class="count_ui_info_item list_2_item">
						<span class="cui_data"><?=$count['xsjl']?></span>
						<span class="cui_data_title">销售经理</span>
					</div>
					<div class="count_ui_info_item list_2_item">
						<span class="cui_data"><?=$count['zjl']?></span>
						<span class="cui_data_title">总经理</span>
					</div>
					<div style="clear:both"></div>
				</div>
				<div class="widget-head am-cf" style="margin-top:20px;">
                    <div class="widget-title am-cf">订单数据统计</div>
                </div>
				<div class="count_ui_info">
					<div class="count_ui_info_item data_3_list">
						<div class="left_div_data">
							<span class="cui_data"><?=$order['day7']?></span>
							<span class="cui_data_title">7天以内订单</span>
						</div>
						<div class="right_div_data">
							<span class="cui_data"><?=sprintf("%1\$.2f",$order['day7_price'])?></span>
							<span class="cui_data_title">订单金额</span>
						</div>
					</div>
					<div class="count_ui_info_item data_3_list">
						<div class="left_div_data">
							<span class="cui_data"><?=$order['day15']?></span>
							<span class="cui_data_title">15天以内订单</span>
						</div>
						<div class="right_div_data">
							<span class="cui_data"><?=sprintf("%1\$.2f",$order['day15_price'])?></span>
							<span class="cui_data_title">订单金额</span>
						</div>
					</div>
					<div class="count_ui_info_item data_3_list">
						<div class="left_div_data">
							<span class="cui_data"><?=$order['all']?></span>
							<span class="cui_data_title">订单总量</span>
						</div>
						<div class="right_div_data">
							<span class="cui_data"><?=sprintf("%1\$.2f",$order['all_price'])?></span>
							<span class="cui_data_title">订单总金额</span>
						</div>
					</div>
					<div style="clear:both"></div>
				</div>
				<div class="widget-head am-cf" style="margin-top:20px;">
                    <div class="widget-title am-cf">预约数据统计</div>
                </div>
				<div class="count_ui_info">
					<div class="count_ui_info_item ">
						<span class="cui_data"><?=$yuyue['day7']?></span>
						<span class="cui_data_title">7天以内服务</span>
					</div>
					<div class="count_ui_info_item ">
						<span class="cui_data"><?=$yuyue['day15']?></span>
						<span class="cui_data_title">15天以内服务</span>
					</div>
					<div class="count_ui_info_item ">
						<span class="cui_data"><?=$yuyue['all']?></span>
						<span class="cui_data_title">预约服务总数</span>
					</div>
					<div class="count_ui_info_item ">
						<span class="cui_data"><?=$yuyue['js']?></span>
						<span class="cui_data_title">已结算总数</span>
					</div>
					<div style="clear:both"></div>
				</div>
                <!--<div class="tpl-page-state-title am-text-center"><?= $setting['store']['values']['name'] ?></div>
                <div class="tpl-error-title-info">Welcome To YoShop System</div>
                <div class="tpl-page-state-content tpl-error-content">
                    <p>
					商城管理后台
					<style>
						.system_icon{ width:500px; margin:0px auto;}
						.system_icon i{ display:inline-block; padding:10px 15px;}
					</style>
					<div class="system_icon">
						<h1>系统图标</h1>
						<i class="iconfont sidebar-nav-link-logo icon-bianji" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-add" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-account" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-add1" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-bulb" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-camera" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-caret" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-close" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-refresh" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-daohangpinlei" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-code" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-daohang" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-dingwei" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-gift" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-huadongdaohang" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-huodong" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-home" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-iconfonticonfontshan" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-image" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-goods" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-like" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-linechart" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-lunbotu" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-heart" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-marketing" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-minus" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-menu" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-order" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-menufold" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-outdent" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-qrcode" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-rise" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-redo" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-search" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-scan" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-search1" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-shanchu" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-set" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-shanchu1" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-search2" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-shangpin" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-setting" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-shopping" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-shop" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-star" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-shouye" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-tag" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-stock " style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-tuichu " style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-tuichu1" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-search " style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-user" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-wxapp" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-tixing" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-wxbsousuotuiguang" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-x" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-xiaochengxu" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-xiaoxi" style=""></i>
						<i class="iconfont sidebar-nav-link-logo icon-xiaoxi1" style=""></i>
					</div>
					</p>
					
                </div>-->
            </div>
        </div>
    </div>
</div>
