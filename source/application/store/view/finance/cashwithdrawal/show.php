<style>
    .layui-layer-content{ height: auto !important}
	.am-form-group{ margin-bottom:10px;}
</style>
<div class="row-content am-cf" style="padding:0px;">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12" style="padding:0px;margin:0px">
            <div class="widget am-cf" style="padding:0px;margin:0px">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body" style="padding:0px;">
                        <fieldset style="margin:0px">
							<div class="am-form-group">
                                <label class="am-u-sm-2 am-u-lg-2 am-form-label form-require">提现积分 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
									<?=$info['money']?> 分
                                    </label>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-2 am-u-lg-2 am-form-label form-require">实际到账积分 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
									<?=$info['user_money']?> 分
                                    </label>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-2 am-u-lg-2 am-form-label form-require">分佣积分 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
									<?php echo $info['reward_price'] * ( $info['reward_user'] ? count(json_decode( $info['reward_user'] , true) ) : 0 );?> 元
                                    </label>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-2 am-u-lg-2 am-form-label form-require">分佣比例 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
									<?=$info['reward_ratio']?> %
                                    </label>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-2 am-u-lg-2 am-form-label form-require">分佣人数 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
									<?php echo ( $info['reward_user'] ? count(json_decode( $info['reward_user'] , true) ) : 0 );?> 人
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-u-lg-2 am-form-label form-require">审核状态 </label>
                                <div class="am-u-sm-9 am-u-end">
									<?php if( $info['status'] ) : ?>
									<label class="am-radio-inline">
										<?php 
											if( $info['status'] == 0 ) {
												echo "未审核";
											} else if( $info['status'] == 1 ) {
												echo "审核未通过";
											} else if( $info['status'] == 2 ) {
												echo "已通过";
											}
										?>
                                    </label>
									<?php else : ?>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="status" value="2" data-am-ucheck required checked>审核通过
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="status" value="1" data-am-ucheck required>拒绝驳回
                                    </label>
									<?php endif; ?>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-2 am-u-lg-2 am-form-label">说明备注 </label>
                                <div class="am-u-sm-9 am-u-end">
									<?php if( $info['status'] ) : ?>
									<label class="am-radio-inline">
										<?=$info['note']?$info['note']:'--'?>
                                    </label>
									<?php else : ?>
                                    <textarea class="tpl-form-input  am-active" name="note" style="height:100px;" placeholder="请输入此次操作的原因"></textarea>
									<?php endif; ?>
                                </div>
                            </div>
							<?php if( !$info['status'] ) : ?>
                            <input type="hidden" value="<?=$info['id']?>" name="id" />
                            <div class="am-form-group" style="margin:0px">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary" level-data="parent">提交</button>
                                </div>
                            </div>
							<?php endif;?>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('#my-form').superForm();
    })
</script>