<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">提现管理</div>
                </div>
                <div class="widget-body am-fr">
					<form action="" style="height:50px;" class="am-form tpl-form-line-form submit_forms" enctype="multipart/form-data" method="get">
                    <div class="am-u-sm-12 am-u-md-2 am-u-lg-2"> </div>
					<div class="am-u-sm-12 am-u-md-10 am-u-lg-10" style="text-align: right;">
						<div class="am-btn-group am-btn-group-xs" >
							<input type="text" class="tpl-form-input" name="name" value="<?=input('param.name')?>" placeholder="用户昵称/用户ID">
						</div>
						<div class="am-btn-group am-btn-group-xs" >
							<select name="type"  class="tpl-form-input">
								<option value="">提现类型</option>
								<option value="银行卡" <?php if( input('param.type') == '银行卡' ) echo "selected" ?>>银行卡</option>
								<option value="支付宝" <?php if( input('param.type') == '支付宝' ) echo "selected" ?>>支付宝</option>
								<option value="微信" <?php if( input('param.type') == '微信' ) echo "selected" ?>>微信</option>
							</select>
						</div>
						<div class="am-btn-group am-btn-group-xs" >
							<select name="status"  class="tpl-form-input">
								<option value="">状态</option>
								<option value="0" <?php if( input('param.status') == "0" ) echo "selected" ?>>未审核</option>
								<option value="1" <?php if( input('param.status') == 1 ) echo "selected" ?>>审核未通过</option>
								<option value="2" <?php if( input('param.status') == 2 ) echo "selected" ?>>已通过</option>
							</select>
						</div>
						<button type="submit" class="j-submit am-btn am-btn-secondary fontsize15rem">搜索</button>
						<button type="button" class="export_submit am-btn am-btn-secondary fontsize15rem">导出</button>
					</div>
					</form>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>提现用户</th>
                                <th>联系方式</th>
                                <th>提现信息</th>
                                <th>账号/卡号</th>
                                <th>提现积分</th>
                                <th>实际到账积分</th>
                                <th>收款码</th>
                                <th>状态</th>
                                <th>提现时间</th>
                                <th width="160">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['id'] ?></td>
                                    <td class="am-text-middle"><?= $item['nickname'] ?></td>
                                    <td class="am-text-middle"><?= $item['mobile'] ?></td>
                                    <td class="am-text-middle">
										<?php if( $item['type'] == '银行卡' ) : ?>
										
										<?php $item_data = json_decode( $item['data'] , true ); $item = array_merge($item , ($item_data?$item_data:[])); ?>
										户主：<?= $item['name'] ?><br>提现银行：<?= $item['blank_type'] ?><br>银行全称：<?= $item['blank_name'] ?><br>
										<?php else :?>
										--
										<?php endif; ?>
									</td>
                                    <td class="am-text-middle" ><?= $item['cart_no']?></td>
                                    <td class="am-text-middle" ><?= $item['money']?> 分</td>
                                    <td class="am-text-middle" ><?= $item['user_money']?> 分</td>
                                    <td class="am-text-middle" >
										<?php if( $item['type'] != '银行卡') :?>
										<a href="<?php echo $item['account_img'];?>" target="_blank">查看收款码</a>
										<?php else: ?>
										--
										<?php endif;?>
									</td>
                                    <td class="am-text-middle" >
										<?php 
											if( $item['status'] == 0 ) {
												echo "未审核";
											} else if( $item['status'] == 1 ) {
												echo "审核未通过";
											} else if( $item['status'] == 2 ) {
												echo "已通过";
											}
										?>
									</td>
                                    <td class="am-text-middle"><?=$item['create_time']?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
											<?php if(!$item['status']) : ?>
                                            <a class="shenghe" height="550" href="<?= url('finance.cashwithdrawal/show',
                                                ['id' => $item['id']]) ?>" title="提现审核处理">
                                                <i class="am-icon-pencil"></i> 审核
                                            </a>
											<?php else : ?>
											<a class="shenghe" height="550" href="<?= url('finance.cashwithdrawal/show',
                                                ['id' => $item['id']]) ?>" title="提现审核消息查看">
                                                <i class="am-icon-pencil"></i> 查看
                                            </a>
											<a href="javascript:;" class="item-delete tpl-table-black-operation-del"
												data-id="<?= $item['id'] ?>">
												 <i class="am-icon-trash"></i> 删除
											</a>
											<?php endif; ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="9" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        // 删除元素
        var url = "<?= url('finance.cashwithdrawal/delete') ?>";
        $('.item-delete').delete('id', url);
		
		$("a.shenghe").openWindow();
    });
</script>


<script>
$(".export_submit").click(function(){
	var url = decodeURIComponent(window.location.href).split("?s=");
	var url_uri = url[1].substr(1).split('/');
	var url_data = $('form').serialize().split("&");
	$.each(url_data , function(k,v){
		var uri_data = v.split("=");
		var floag = false;
		$.each(url_uri , function(m,n){
			if( m > 2 && m % 2 == 1 && uri_data[0] == n ) {
				floag = true;
				url_uri[m + 1] = uri_data[1];
			}
		});
		if( floag === false ) {
			url_uri.push( uri_data[0] + '/' + uri_data[1] );
		}
		
	});
	location.href = url[0] + '?s=' + '/'+url_uri.join("/").replace("<?php echo request()->action();?>" , 'export');
	return false;
});
</script>