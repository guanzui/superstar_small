<style>
    .chongzhi_body{ padding:20px; width: 600px; height:300px;}
	.jiyouliebiao{ margin:20px; height:300px; overflow-y:scroll}
</style>
<script src="assets/layer/laydate/laydate.js"></script>
<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">用户列表</div>
                </div>
                <div class="widget-body am-fr">
					<form action="<?=url("store/finance.user/index");?>" style="height:50px;" class="am-form tpl-form-line-form submit_forms" enctype="multipart/form-data" method="get">
                    <div class="am-u-sm-12 am-u-md-2 am-u-lg-2"></div>
					<div class="am-u-sm-12 am-u-md-10 am-u-lg-10" style="text-align: right;">
						<div class="am-btn-group am-btn-group-xs" >
							<input type="text" class="tpl-form-input" name="string" value="<?=input('param.string')?>" placeholder="姓名/手机号/卡号">
						</div>
						<div class="am-btn-group am-btn-group-xs" style="width:180px;">
							<input type="text" class="tpl-form-input" name="datetime" id="datetime" value="<?=input('param.datetime')?>" placeholder="日期筛选">
						</div>
						<div class="am-btn-group am-btn-group-xs" style="width:100px;">
							<select name="is_cart"  class="tpl-form-input">
								<option value="">是否开卡</option>
								<option value="2" <?php if( input('param.is_cart') == 2 ) echo "selected" ?>>已开卡</option>
								<option value="1" <?php if( input('param.is_cart') == 1 ) echo "selected" ?>>未开卡</option>
							</select>
						</div>
						<button type="submit" class="j-submit am-btn am-btn-secondary fontsize15rem">搜索</button>
						<button type="button" class="export_submit am-btn am-btn-secondary fontsize15rem">导出</button>
					</div>
					</form>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>用户ID</th>
                                <th>手机号</th>
                                <th>头像</th>
                                <th>昵称</th>
                                <th>会员卡</th>
                                <th>开卡时间</th>
                                <th>余额</th>
                                <th>积分</th>
                                <th>状态</th>
                                <th width="80">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['user_id'] ?></td>
                                    <td class="am-text-middle"><?= $item['mobile'] ?></td>
                                    <td class="am-text-middle">
                                        <a href="<?= $item['avatar'] ?>" title="点击查看大图" target="_blank">
                                            <img src="<?= $item['avatar'] ?>" width="40" height="40" alt="">
                                        </a>
                                    </td>
                                    <td class="am-text-middle"><?= $item['nickname'] ?></td>
                                    <td class="am-text-middle"><?= $item['is_open_cart'] == 1 ? '已开通' : '未开通'?></td>
                                    <td class="am-text-middle"><?= $item['open_cart_time'] > 0 ? date("Y-m-d H:i" , $item['open_cart_time']) : '--' ?></td>
                                    <td class="am-text-middle"><?= $item['money'] ?> / <a href="javascript:;" data-value="<?=$item['money']?>" data-parent_nickname="<?=$item['parent_nickname']?>" class="chognzhi_but" data-id="<?=$item['user_id']?>" data-type="1" title="余额充值">充值</a></td>
                                    <td class="am-text-middle"><?= $item['integral'] ?> / <a href="javascript:;" data-value="<?=$item['integral']?>" data-parent_nickname="<?=$item['parent_nickname']?>" class="chognzhi_but" data-id="<?=$item['user_id']?>" data-type="2" title="积分充值">充值</a></td>
                                    <td class="am-text-middle"><?= $item['status'] == 2 ? "正常" : '禁用' ?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                          <?php if( $item['is_open_cart'] == 1 ) : ?>
                                          	<a href="<?= url('user/opencart' , array('user_id' => $item['user_id'])) ?>" title="确定是否要复购？" class="item-ajax-request">
                                                复购
                                            </a>
                                          <?php else : ?>
											<a href="<?= url('user/opencart' , array('user_id' => $item['user_id'])) ?>" title="是否确定要开通会员？" class="item-ajax-request">
                                                开通会员
                                            </a>
                                          <?php endif; ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="9" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script id="chongzhi_template">
<div class="chongzhi_body">
        <div class="am-form tpl-form-line-form">
			<div class="am-form-group">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label ">上级用户 </label>
                <div class="am-u-sm-9 am-u-end">
                    <span style="height:3rem; line-height:3rem; display: inline-block;font-size: 1.2rem;">{{parent_nickname}}</span>
                </div>
            </div>
            <div class="am-form-group">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label ">{{name}} </label>
                <div class="am-u-sm-9 am-u-end">
                    <span style="height:3rem; line-height:3rem; color:red; display: inline-block;font-size: 1.8rem;">{{value}}</span>
                </div>
            </div>
            <div class="am-form-group">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label form-require">充值数量 </label>
                <div class="am-u-sm-6 am-u-end">
                    <input type="text" class="tpl-form-input" id="num" value="" required="" pattern="^-?(?:\d+|\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$">
                </div>
            </div>
            <div class="am-form-group">
                <label class="am-u-sm-2 am-u-lg-2 am-form-label">备注 </label>
                <div class="am-u-sm-9 am-u-end">
                    <textarea class="tpl-form-input  am-active" id="note" required="" placeholder="请输入此次操作的原因"></textarea>
                </div>
            </div>
            <input type="hidden" value="1" name="id">
            <div class="am-form-group">
                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                    <button type="button" class="but-submit-data am-btn am-btn-secondary " data-type="{{type}}" data-id="{{id}}">确定充值</button>
                </div>
            </div>
        </div>
</div>
</script>
<script>
    $(function () {
        $('.item-ajax-request').request_ajax();
        $("a.chognzhi_but").click(function(){
            var value = $(this).data("value");
            var parent_nickname = $(this).data("parent_nickname");
            var type = $(this).data("type");
            var id = $(this).data("id");
            layer.open({
                type: 1,
                title : $(this).attr("title"),
                skin: 'layui-layer-rim',
                area: ['650px', '380px'],
                content: $("#chongzhi_template").html().replace("{{name}}" , (type == 1 ? '当前余额' : '当前积分')).replace("{{id}}" , id).replace("{{parent_nickname}}" , parent_nickname).replace("{{type}}" , type).replace("{{value}}" , ( type == 1 ? "￥" : '' ) + value)
            });
        })
        $(document).on( 'click',".but-submit-data"  , function(){
            var type = $(this).data("type");
            var id = $(this).data("id");
            var num = $("#num").val();
            var note = $("#note").val();
            if( num == '' ) {
                layer.msg("请输入需要充值的数量");
                return false;
            }
            $.post('<?=url('store/finance.user/recharge');?>' , { id :id , num :num , type:type , note:note} , function(data){
                if( data.code == 1 ) {
                    layer.alert(data.msg , function(){
                        window.location.reload();
                    })
                } else {
                    layer.msg(data.msg);
                }
            },'json');
        });
		
    });
	laydate.render({
	  elem: '#datetime',
	  type: 'date',
	  range: '~'
	});
</script>
<script>
$(".export_submit").click(function(){
	var url = decodeURIComponent(window.location.href).split("?s=");
	var url_uri = url[1].substr(1).split('/');
	var url_data = $('form').serialize().split("&");
	$.each(url_data , function(k,v){
		var uri_data = v.split("=");
		var floag = false;
		$.each(url_uri , function(m,n){
			if( m > 2 && m % 2 == 1 && uri_data[0] == n ) {
				floag = true;
				url_uri[m + 1] = uri_data[1];
			}
		});
		if( floag === false ) {
			url_uri.push( uri_data[0] + '/' + uri_data[1] );
		}
		
	});
	location.href = url[0] + '?s=' + '/'+url_uri.join("/").replace("<?php echo request()->action();?>" , 'export').replace("finance." , '');
	return false;
});
</script>