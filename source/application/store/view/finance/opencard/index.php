<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">开卡记录列表</div>
                </div>
                <div class="widget-body am-fr">
                    <form action="" class="am-form tpl-form-line-form submit_forms" enctype="multipart/form-data" method="post">
                        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12" style="text-align: right;">
							<div class="am-btn-group am-btn-group-xs" >
								<input type="text" class="tpl-form-input" name="name" value="<?=input('param.name')?>" placeholder="用户昵称/用户ID">
							</div>
							<div class="am-btn-group am-btn-group-xs">
                                <select name="restart" required  data-am-selected="{searchBox: 1, btnSize: 'sm',  placeholder:'开卡类型'}">
                                    <option value="-1">全部</option>
                                    <option value="0" <?php if( input('param.restart') == "0" ) echo "selected" ?>>首次开卡</option>
                                    <option value="1" <?php if( input('param.restart') == 1 ) echo "selected" ?>>复购</option>
                                </select>
                            </div>
                            <div class="am-btn-group am-btn-group-xs">
                                <select name="type" required  data-am-selected="{searchBox: 1, btnSize: 'sm',  placeholder:'支付类型'}">
                                    <option value="0">全部</option>
                                    <option value="2" <?php if( input('param.type') == 2 ) echo "selected" ?>>支付宝</option>
                                    <option value="1" <?php if( input('param.type') == 1 ) echo "selected" ?>>微信</option>
                                    <option value="3" <?php if( input('param.type') == 3 ) echo "selected" ?>>余额</option>
                                    <option value="4" <?php if( input('param.type') == 4 ) echo "selected" ?>>平台开通</option>
                                </select>
                            </div>
                            <button type="submit" class="j-submit am-btn am-btn-secondary fontsize15rem">搜索</button>
						<button type="button" class="export_submit am-btn am-btn-secondary fontsize15rem">导出</button>
                        </div>
                    </form>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>用户</th>
                                <th>订单编号</th>
                                <th>支付类型</th>
                                <th>支付金额</th>
                                <th>开卡类型</th>
                                <th>支付时间</th>
                                <th>支付单号</th>
                                <th>赠送服务次数</th>
                                <th>下单时间</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['id'] ?></td>
                                    <td class="am-text-middle"><?= $item['nickname'] ?></td>
                                    <td class="am-text-middle"><?= $item['ordersn'] ?></td>
                                    <td class="am-text-middle"><?= $item['pay_type']==1?'微信':($item['pay_type']==2?'支付宝':($item['pay_type']==3?'余额':'平台开通')) ?></td>
                                    <td class="am-text-middle"><?= $item['price'] ?></td>
                                    <td class="am-text-middle"><?= $item['restart']==1?'复购':'首次开卡' ?></td>
                                    <td class="am-text-middle"><?= $item['pay_time'] ?></td>
                                    <td class="am-text-middle"><?= $item['transaction_id'] ?></td>
                                    <td class="am-text-middle"><?= $item['service_num'] ?></td>
                                    <td class="am-text-middle"><?= $item['create_time'] ?></td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="11" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(".export_submit").click(function(){
	var url = decodeURIComponent(window.location.href).split("?s=");
	var url_uri = url[1].substr(1).split('/');
	var url_data = $('form').serialize().split("&");
	$.each(url_data , function(k,v){
		var uri_data = v.split("=");
		var floag = false;
		$.each(url_uri , function(m,n){
			if( m > 2 && m % 2 == 1 && uri_data[0] == n ) {
				floag = true;
				url_uri[m + 1] = uri_data[1];
			}
		});
		if( floag === false ) {
			url_uri.push( uri_data[0] + '/' + uri_data[1] );
		}
		
	});
	location.href = url[0] + '?s=' + '/'+url_uri.join("/").replace("<?php echo request()->action();?>" , 'export');
	return false;
});
</script>