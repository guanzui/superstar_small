<style>
    .chongzhi_body{ padding:20px; width: 600px; height:300px;}
	.jiyouliebiao{ margin:20px; height:300px; overflow-y:scroll}
</style>
<link type="text/css" rel="stylesheet" href="/assets/store/jedate/skin/jedate.css">
<script type="text/javascript" src="/assets/store/jedate/src/jedate.js"></script>
<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">奖金拨比查询</div>
                </div>
                <div class="widget-body am-fr">
					<form action="<?=url("store/finance.allocate/index");?>" style="height:50px;" class="am-form tpl-form-line-form submit_forms" enctype="multipart/form-data" method="get">
					<div class="am-u-sm-12 am-u-md-12 am-u-lg-12" style="text-align: center;">
						<div class="am-btn-group am-btn-group-xs">
							<select name="type" required  data-am-selected="{searchBox: 1, btnSize: 'sm',  placeholder:'请选择类型'}">
								<option value="0">全部类型</option>
								<option value="1" <?=$type == 1 ? 'selected' : '';?>>直推</option>
								<option value="2" <?=$type == 2 ? 'selected' : '';?>>间推</option>
								<option value="3" <?=$type == 3 ? 'selected' : '';?>>复购奖</option>
								<option value="4" <?=$type == 4 ? 'selected' : '';?>>团长提成</option>
							</select>
						</div>
						<div class="am-btn-group am-btn-group-xs" >
							<input type="text" class="tpl-form-input" id="start" name="start" value="<?=$start?>" placeholder="开始时间">
						</div>
						<div class="am-btn-group am-btn-group-xs" >
							<input type="text" class="tpl-form-input" name="end" id="end" value="<?=$end?>" placeholder="结束时间">
						</div>
						<button type="submit" class="j-submit am-btn am-btn-secondary fontsize15rem">搜索</button>
					</div>
					</form>
					<style>
					.user_data_count{ width:100%; height:120px; margin-top:50px;}
					.user_data_count_item{ width:calc(33.3% - 10px); height:120px; float:left;margin-right:10px; border:1px solid #efefef; border-bottom:3px solid #ccc}
					.user_data_count_item p{ margin:0px; text-align:center; font-size:14px;}
					.user_data_count_item p.item_ti{ margin:25px 0 5px 0; font-size:20px; font-weight:bold}
					</style>
					<div class="user_data_count">
						<div class="user_data_count_item">
							<p class="item_ti">￥<?=$data['sale_price']?></p>
							<p>销售总额</p>
						</div>
						<div class="user_data_count_item">
							<p class="item_ti">￥<?=$data['reward_price']?></p>
							<p>奖金</p>
						</div>
						<div class="user_data_count_item">
							<p class="item_ti"><?=$data['ratio']?></p>
							<p>拨比</p>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
jeDate("#start",{theme:{bgcolor:"#00A1CB",pnColor:"#00CCFF"},multiPane:false,format:"YYYY-MM-DD hh:mm:ss"});
jeDate("#end",{theme:{bgcolor:"#00A1CB",pnColor:"#00CCFF"},multiPane:false,format:"YYYY-MM-DD hh:mm:ss"});
</script>
