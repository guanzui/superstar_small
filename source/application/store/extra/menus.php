<?php
/**
 * 后台菜单配置
 *    'home' => [
 *       'name' => '首页',                // 菜单名称
 *       'icon' => 'icon-home',          // 图标 (class)
 *       'index' => 'index/index',         // 链接
 *     ],
 */
return [
    'index' => [
        'name' => '首页',
        'icon' => 'icon-home',
        'index' => 'index/index',
		'id' => 1,
    ],
    'goods' => [
        'name' => '商品管理',
        'icon' => 'icon-goods',
        'index' => 'goods/index',
		'id' => 2,
        'submenu' => [
            [
                'name' => '商品列表',
                'index' => 'goods/index',
				'id' => 3,
				'pid' => 2
            ],
            [
                'name' => '商品分类',
                'index' => 'goods.category/index',
				'id' => 4,
				'pid' => 2
            ],
            [
                'name' => '用户评论',
                'index' => 'goods.comment/index',
				'id' => 5,
				'pid' => 2
            ]
        ],
    ],
    'order' => [
        'name' => '订单管理',
        'icon' => 'icon-order',
        'index' => 'order/delivery_list',
		'id' => 6,
        'submenu' => [
            [
                'name' => '待发货',
                'index' => 'order/delivery_list',
				'id' => 7,
				'pid' => 6
            ],
            [
                'name' => '待收货',
                'index' => 'order/receipt_list',
				'id' => 8,
				'pid' => 6
            ],
            [
                'name' => '待付款',
                'index' => 'order/pay_list',
				'id' => 9,
				'pid' => 6
            ],
            [
                'name' => '已完成',
                'index' => 'order/complete_list',
				'id' => 10,
				'pid' => 6

            ],
            [
                'name' => '已取消',
                'index' => 'order/cancel_list',
				'id' => 11,
				'pid' => 6
            ],
            [
                'name' => '全部订单',
                'index' => 'order/all_list',
				'id' => 12,
				'pid' => 6
            ],
        ]
    ],
    'user' => [
        'name' => '用户管理',
        'icon' => 'icon-user',
        'index' => 'user/index',
		'id' => 13,
        'submenu' => [
            [
                'name' => '用户列表',
                'index' => 'user/index',
				'pid' => 13,
				'id' => 14
            ],
            [
                'name' => '车辆管理',
                'index' => 'user.autolist/index',
				'pid' => 13,
				'id' => 15
            ],
            [
                'name' => '团队长申请审核',
                'index' => 'user.items/index',
				'pid' => 13,
				'id' => 17
            ]
        ]
    ],
    'shop' => [
        'name' => '服务中心',
        'icon' => 'icon-shop',
        'index' => 'shop/index',
		'id' => 18,
        'submenu' => [
            [
                'name' => '服务中心列表',
                'index' => 'shop/index',
				'pid' => 18,
				'id' => 19
            ],
            [
                'name' => '机油管理',
                'index' => 'shop.engineoil/index',
				'pid' => 18,
				'id' => 20
            ],
            [
                'name' => '补油申请',
                'index' => 'shop.replenishment/index',
				'pid' => 18,
				'id' => 21
            ],
            [
                'name' => '换油记录',
                'index' => 'shop.replace/index',
				'pid' => 18,
				'id' => 22
            ],
            [
                'name' => '换油评价',
                'index' => 'shop.replacecomment/index',
				'pid' => 18,
				'id' => 23
            ],
            [
                'name' => '服务费结算',
                'index' => 'shop.yuyueauditing/index',
				'pid' => 18,
				'id' => 24
            ]
        ]
    ],
	'finance' => [
		'name' => '财务管理',
        'icon' => 'icon-rise',
        'index' => 'finance.user/index',
		'id' => 43,
        'submenu' => [
			[
                'name' => '用户充值',
                'index' => 'finance.user/index',
				'pid' => 43,
				'id' => 48,
            ],
			[
                'name' => '提现管理',
                'index' => 'finance.cashwithdrawal/index',
				'pid' => 43,
				'id' => 44,
            ],
			[
                'name' => '账户消费',
                'index' => 'finance.usermoney/index',
				'pid' => 43,
				'id' => 45,
            ],
			[
                'name' => '消费积分',
                'index' => 'finance.userintegral/index',
				'pid' => 43,
				'id' => 46,
            ],
			[
                'name' => '分佣奖金',
                'index' => 'finance.commission/index',
				'pid' => 43,
				'id' => 47,
            ],
			[
                'name' => '奖金拨比',
                'index' => 'finance.allocate/index',
				'pid' => 43,
				'id' => 49,
            ],
			[
                'name' => '开卡记录',
                'index' => 'finance.opencard/index',
				'pid' => 43,
				'id' => 52,
            ]
		]
	],
    'count' => [
        'name' => '数据统计',
        'icon' => 'icon-linechart',
        'index' => 'count.yuyuenum/index',
		'id' => 25,
        'submenu' => [
			[
                'name' => '预约次数',
                'index' => 'count.yuyuenum/index',
				'pid' => 25,
				'id' => 50,
            ],
            [
                'name' => '商家进销记录',
                'index' => 'count.shoppurchase/index',
				'pid' => 25,
				'id' => 29,
            ]
        ]
    ],
    'setting' => [
        'name' => '设置',
        'icon' => 'icon-setting',
        'index' => 'setting/store',
		'id' => 30,
        'submenu' => [
            [
                'name' => '商城设置',
                'index' => 'setting/store',
				'pid' => 30,
				'id' => 31
            ],
            [
                'name' => '交易设置',
                'index' => 'setting/trade',
				'pid' => 30,
				'id' => 32
            ],
            [
                'name' => '配送设置',
                'index' => 'setting.delivery/index',
				'pid' => 30,
				'id' => 33
            ],
            [
                'name' => '短信通知',
                'index' => 'setting/sms',
				'pid' => 30,
				'id' => 34
            ],
            [
                'name' => '分销设置',
                'index' => 'setting/distribution',
				'pid' => 30,
				'id' => 35
            ],
            [
                'name' => '汽车品牌',
                'index' => 'setting.autobrand/index',
				'pid' => 30,
				'id' => 36
            ],
            [
                'name' => '广告管理',
                'index' => 'setting.ads/index',
				'pid' => 30,
				'id' => 37
            ],
            [
                'name' => '帮助文档',
                'index' => 'setting.article/index',
				'pid' => 30,
				'id' => 51
            ],
            [
                'name' => '首页菜单',
                'index' => 'setting.menu/index',
				'pid' => 30,
				'id' => 38
            ],
			[
				'name' => '管理员列表',
				'index' => 'setting.admin/index',
				'pid' => 30,
				'id' => 39
			],
			[
				'name' => '角色列表',
				'index' => 'setting.role/index',
				'pid' => 30,
				'id' => 40
			],
			[
				'name' => '清理缓存',
				'index' => 'setting.cache/clear',
				'pid' => 30,
				'id' => 41
			],
			[
				'name' => '环境监测',
				'index' => 'setting.science/index',
				'pid' => 30,
				'id' => 42
			]
        ]
    ]
];
