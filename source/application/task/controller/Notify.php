<?php

namespace app\task\controller;

use app\common\extend\Payapi as PayModel;

use think\Controller as ThinkController;
/**
 * 支付成功异步通知接口
 * Class Notify
 * @package app\api\controller
 */
class Notify extends ThinkController
{
    /**
     * 支付成功异步通知
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    public function order()
    {
		$pay = new PayModel;
		$type = $this->request->param("type/s" , '');
		if( $type == '' ) exit('没有支付方式');
		$pay->notify( $type == 'wxpay' ? 'WxPay' : 'Alipay' );
    }

}
