<?php

namespace app\index\controller;

use think\Controller as ThinkController;
use app\common\model\Goods as GoodsModel;
use app\common\model\User as UserModel;
use app\store\model\Setting as SettingModel;

use app\common\library\sms\Driver as SmsDriver;
use think\Cache;
use think\Request;

/**
 * 我的好友
 * Class Index
 * @package app\index\controller
 */
class firend extends ThinkController {
    
    public function index() {
        $token = $this->request->param("token/s" , '');
        if( !$token ) exit("信息错误");
        $user_info = db("user")->where(['login_token' => $token,'status' => 2])->find();
        if( !$user_info ) $this->error("用户不存在");
		$user_data = []; 
        $list = $this->friend( $user_info['user_id'] , $user_data );
		$data = [
			[
				'name' => $user_info['nickname'] ? $user_info['nickname'] : '我自己',
				'code' => $user_info['user_id'],
				'icon' => "icon-th",
				'child' => $list
			]
		];
		$end = strtotime(date("Y-m-d 23:59:59" , time()));
		$start = strtotime(date("Y-m-d",time()));
		
		//统计
		$count = [
			'total' => 0,
			'total_price' => 0,
			'total_item' => 0,
			'total_cart' => 0,
			'total_1' => 0,
			'total_2' => 0,
			'total_3' => 0,
			'today' => 0,
			'day7' => 0,
			'day15' => 0,
		];
		if( $user_data ) {
			$count = [
				'total' => count($user_data),
				'total_price' => db("member_cart_order")->where(['user_id' => ['IN' , implode(',' , $user_data)],'status' => 1])->sum('price'),
				'total_item' => db("user")->where(['user_id' => ['IN' , implode(',' , $user_data)],'status' => 2 , 'is_item' => 1])->count(),
				'total_cart' => db("user")->where(['user_id' => ['IN' , implode(',' , $user_data)],'status' => 2 , 'is_open_cart' => 1])->count(),
				'total_1' => db("user")->where(['user_id' => ['IN' , implode(',' , $user_data)],'status' => 2 , 'identity' => 10])->count(),
				'total_2' => db("user")->where(['user_id' => ['IN' , implode(',' , $user_data)],'status' => 2 , 'identity' => 20])->count(),
				'total_3' => db("user")->where(['user_id' => ['IN' , implode(',' , $user_data)],'status' => 2 , 'identity' => 30])->count(),
				'today' => db("user")->where('`user_id` IN('. implode(',' , $user_data) .') AND `status` = 2 AND `create_time` >= ' . $start . ' AND `create_time` <= ' . $end)->count(),
				'day7' => db("user")->where('`user_id` IN('. implode(',' , $user_data) .') AND `status` = 2 AND `create_time` >= ' . ( $start - ( 15 * 3600 * 24 - 1 ) ) . ' AND `create_time` <= ' . $end)->count(),
				'day15' => db("user")->where('`user_id` IN('. implode(',' , $user_data) .') AND `status` = 2 AND `create_time` >= ' . ( $start - ( 30 * 3600 * 24 - 1 ) ) . ' AND `create_time` <= ' . $end)->count(),
			];
		}
        return $this->fetch('index' ,compact('data' , 'count'));
    }
	
	private function friend($id , &$data ) {
		if( !$id ) return '';
        $sf_field = [0 => '普通会员',10 => '业务员',20 => '销售经理',30 => '总经理'];
		$user = db("user")->where(['parent_id' => $id , 'status' => 2])->field("user_id AS code,nickname AS name,identity,parent_id AS parentCode,is_item")->select()->toArray();
		if( $user ) {
			foreach( $user as $k => $v) {
				$data[] = $v['code'];
				$user[$k]['name'] = $user[$k]['name'] . ' 等级：' . $sf_field[$v['identity']] . ( $v['is_item'] == 1 ? '团长已开通' : '' );
				unset( $user[$k]['identity'] );
				$list = $this->friend( $v['code'] , $data );
				$user[$k]['icon'] = '';
				$user[$k]['child'] = [];
				if( $list ) {
					$user[$k]['icon'] = 'icon-minus-sign';
					$user[$k]['child'] = $list;
				}
			}
		}
		return $user;
	}
    
}