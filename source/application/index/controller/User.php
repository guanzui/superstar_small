<?php

namespace app\index\controller;

use think\Controller as ThinkController;
use app\store\model\Setting as SettingModel;
use think\Request;

/**
 * 用户
 * Class Index
 * @package app\index\controller
 */
class User extends ThinkController {
    
    public function register() {
        $share_code = $this->request->param("share_code/d" , 0);
		$area = db("region")->field("id,pid,name as value,level")->select();
        $new_area = [];
        foreach( $area as $value ) {
            $new_area[$value['pid']][] = $value;
        }
        foreach ( $new_area[0] as $k => $v ) {
            if( isset( $new_area[$v['id']] ) ) {
                $level2 =  $new_area[$v['id']];
                foreach( $level2 as $k1 => $v1 ) {
                    if( isset( $new_area[$v1['id']] ) ) {
                        $level2[$k1]['childs'] = $new_area[$v1['id']];
                    }
                }
                $new_area[0][$k]['childs'] = $level2;
            }
        }
        $data['data'] = $new_area[0];
		$config = SettingModel::getItem('store');
		if( !$share_code ) {
			$share_code = '推荐码,可为空';//db("user")->where(['user_id' => $config['manage_user_id']])->value("share_code");
		}
        return $this->fetch('register' , compact('data' , 'share_code'));
    }
    
}