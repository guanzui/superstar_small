<?php

namespace app\index\controller;

use think\Controller as ThinkController;
use app\common\model\User as UserModel;
use app\store\model\Setting as SettingModel;
use think\Request;

/**
 * 公共
 * Class Index
 * @package app\index\controller
 */
class Index extends ThinkController {
    
    public function version() {
        $config_data = SettingModel::getItem('store');
        return $this->fetch('version' , compact('config_data'));
    }
    
	//执行脚本函数
	public function data() {
		set_time_limit(0);
		$list = db("member_cart_order_delay")->where('`open_time` <= ' . time() . ' AND `status` = 0')->order("id ASC")->select();
		if( $list ) {
			foreach( $list as $k => $v ) {
				//分销奖励
				$usermodel = new UserModel;
				$usermodel->builddistribution( $v['ordersn'] );
				//团队奖励
				//$usermodel->colonelReward($v['user_id'],$v['order_id']);
				//修改数据为已执行
				db("member_cart_order_delay")->where(['id' => $v['id']])->update(['status' => 1]);
			}
		}
	}
	
	//执行用户定时清除的脚本
	public function timerClear() {
		set_time_limit(0);
		$config_data = SettingModel::getItem('trade');
		$data = db("timer_clear")->value("last_time");
		if( ( $config_data['clear']['time']  * 3600 ) + $data <= time() ) {
			$user_list = db("user")->field("user_id,integral")->where('`is_open_cart` = 0 AND identity = 0 AND integral > 0')->select()->toArray();
			if( $user_list ) {
				foreach( $user_list as $k => $v ) {
					$res = db("user")->where(['user_id' => $v['user_id']])->update(["integral" => 0]);
					if( $res ) {
						db("user_integral_log")->insert([
							'user_id' => $v['user_id'],
							'num' => $v['integral'],
							'type' => 2,
							'content' => '未升级开通成会员每隔【'. $data .'】小时清理一次积分。',
							'create_time' => time()
						]);
					}
				}
			}
			//修改时间戳
			db("timer_clear")->where(['id' => 1])->update(["last_time" => time()]);
		}		
	}
	
	/*public function run() {
		set_time_limit(0);
		$list = db("shop_yuyue_service")->where('id >= 71')->select()->toArray();
		foreach( $list as $k => $v ) {
			$info = db("user_integral_log")->where("user_id = {$v['user_id']} and content = '服务费结算积分增加【100】' and type = 1")->order("id ASC")->find();
			$shop = db("shop")->where(['shop_id' => $v['shop_id']])->find();
			db("user_integral_log")->where(['id' => $info['id']])->update(['user_id' => $shop['user_id']]);
		}
	}*/
}