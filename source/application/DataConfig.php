<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [
    'express_name_list' => [
        'shentong' => '申通快递',
        'EMS' => 'ems',
        'shunfeng' => '顺丰速运',
        'yunda' => '韵达快递',
        'yuantong' => '圆通速递',
        'zhongtong' => '中通快递',
        'huitongkuaidi' => '百世汇通',
        'tiantian' => '天天快递'
    ]
];