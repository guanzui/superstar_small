<?php
namespace app\api\controller;

/**
 * 购物车控制器
 * Class Cart
 * @package app\api\controller
 */
class Cart extends Controller
{
    /**
	 * 购物车列表
	 */
	public function data() {
		$list = thumbImgUrl(db("goods_cart")->alias('gc')->join('yoshop_goods g' , 'g.goods_id = gc.goods_id')->join('yoshop_goods_spec gs','g.goods_id = gs.goods_id AND gc.goods_spec_id = gs.goods_spec_id')
				->field("gc.id,g.goods_id,g.goods_name,g.thumb,gc.num,gs.goods_price,gs.stock_num,gs.spec_sku_id,gc.is_checked")
				->where(['gc.user_id' => $this->user_info['user_id']])->select()->each(function($item , $key){
					if( $item['spec_sku_id'] ) {
						$value = db("spec_value")->where('spec_value_id IN('.str_replace('_' , ',' , $item['spec_sku_id']).')')->column("spec_value");
						$item['attr_value'] = implode(" + " , $value);
					}
					unset( $item['spec_sku_id'] );
					return $item;
				})->toArray());
		$lists = db("goods_cart")->alias("gc")->join("yoshop_goods_spec g",'g.goods_spec_id=gc.goods_spec_id')->where(['gc.user_id' => $this->user_info['user_id'] , 'gc.is_checked' => 1])->field("g.goods_price,gc.num")->select()->toArray();
		$total_price = 0;
		if( $lists ) {
			foreach( $lists as $v ) {
				$total_price += $v['goods_price'] * $v['num'];
			}
		}
		return $this->renderSuccess(compact('list' , 'total_price'));
	}
	
	/**
	 * 购物车修改
	 */
	public function savestock() {
		$cart_id = $this->request->param("cart_id/d" , 0);
		if( $cart_id <= 0 ) $this->error('数据异常，请重试');
		$stock = $this->request->param("stock/d" , 0);
		if( $stock <= 0 ) $this->error('数据异常，请重试');
		//判断数据是否合法
		$info = db("goods_cart")->alias('gc')->join('yoshop_goods g' , 'g.goods_id = gc.goods_id')->join('yoshop_goods_spec gs','g.goods_id = gs.goods_id AND gc.goods_spec_id = gs.goods_spec_id')
				->field('gs.stock_num')->where(['gc.id' => $cart_id , 'gc.user_id' => $this->user_info['user_id']])->find();
		if( !$info ) {
			$this->error("数据不存在，请重试");
		}
		if( $info['stock_num'] < $stock ) {
			$this->error("最多只能购买{$info['stock_num']}商品");
		}
		$result = db("goods_cart")->where(['id' => $cart_id])->update(['num' => $stock]);
		if( $result ) {
			$list = db("goods_cart")->alias("gc")->join("yoshop_goods_spec g",'g.goods_spec_id=gc.goods_spec_id')->where(['gc.user_id' => $this->user_info['user_id'] , 'gc.is_checked' => 1])->field("g.goods_price,gc.num")->select()->toArray();
			$total_price = 0;
			if( $list ) {
				foreach( $list as $v ) {
					$total_price += $v['goods_price'] * $v['num'];
				}
			}
			$this->success('更改成功' , $total_price);
		} else {
			$this->error('购物车数据更改失败');
		}
	}
	
	/**
	 * 增加购物车
	 */
	public function addcart() {
		$goodsid = $this->request->param("goodsid/d" , 0);
		$spec_id = $this->request->param("spec_id/d" , 0);
		$stock = $this->request->param("stock/d" , 1);
		if( $goodsid <= 0 && $spec_id <= 0 ) {
			$this->error('数据异常');
		}
		if( $goodsid > 0 ) {
			$goodsinfo = db("goods_spec")->where(['goods_id' => $goodsid])->find();
			$spec_id = $goodsinfo['goods_spec_id'];
		} else if( $spec_id > 0 ) {
			$goodsinfo = db("goods_spec")->where(['goods_spec_id' => $spec_id])->find();
			$goodsid = $goodsinfo['goods_id'];
		} else {
			$this->error('数据异常');
		}
		if( !$goodsinfo ) {
			$this->error('商品不存在或已下架');
		}
		
		//查询是否存在 购物车
		$data = ['user_id' => $this->user_info['user_id'] , 'goods_id' => $goodsid,'goods_spec_id' => $spec_id];
		$cart_info = db("goods_cart")->where($data)->find();
		if( $goodsinfo['stock_num'] < ($cart_info ? $cart_info['num'] + $stock : $stock)) {
			$this->error('最多只能购买' . $goodsinfo['stock_num']);
		}
		if( $cart_info ) {
			db("goods_cart")->where(['id' => $cart_info['id']])->update(['num' => $cart_info['num'] + $stock]);
		} else {
			$data['num'] = $stock;
			$data['creaet_time'] = time();
			db("goods_cart")->insert($data);
		}
		$this->success('购物车添加成功');
	}
	
	/**
	 * 删除购物车
	 */
	public function delcart() {
		$cart_id = $this->request->param("cart_id/d" , 0);
		if( $cart_id <= 0 ) $this->error('数据异常，请重试');
		//判断数据是否合法
		$info = db("goods_cart")->where(['id' => $cart_id , 'user_id' => $this->user_info['user_id']])->find();
		if( !$info ) {
			$this->error("数据不存在，请重试");
		}
		$result = db("goods_cart")->where(['id' => $cart_id , 'user_id' => $this->user_info['user_id']])->delete();
		if( $result ) {
			$list = db("goods_cart")->alias("gc")->join("yoshop_goods_spec g",'g.goods_spec_id=gc.goods_spec_id')->where(['gc.user_id' => $this->user_info['user_id'] , 'gc.is_checked' => 1])->field("g.goods_price,gc.num")->select()->toArray();
			$total_price = 0;
			if( $list ) {
				foreach( $list as $v ) {
					$total_price += $v['goods_price'] * $v['num'];
				}
			}
			$this->success("删除成功" , $total_price);
		} else {
			$this->error("删除失败");
		}
	}
	
	/**
	 * 购物车选中状态更新
	 */
	public function savechecked() {
		$cart_id = $this->request->param("cart_id" , '');
		$checked = $this->request->param("checked/d" , 0);
		$checked = !$checked ? 0 : 1;
		if( !$cart_id ) $this->error('数据异常，请重试');
		//判断数据是否合法
		$info = db("goods_cart")->where(['id' => ['IN' , $cart_id] , 'user_id' => $this->user_info['user_id']])->find();
		if( !$info ) {
			$this->error("数据不存在，请重试");
		}
		$result = db("goods_cart")->where(['id' => ['IN' , $cart_id] , 'user_id' => $this->user_info['user_id']])->update(['is_checked' => $checked]);
		if( $result ) {
			$list = db("goods_cart")->alias("gc")->join("yoshop_goods_spec g",'g.goods_spec_id=gc.goods_spec_id')->where(['gc.user_id' => $this->user_info['user_id'] , 'gc.is_checked' => 1])->field("g.goods_price,gc.num")->select()->toArray();
			$total_price = 0;
			if( $list ) {
				foreach( $list as $v ) {
					$total_price += $v['goods_price'] * $v['num'];
				}
			}
			$this->success("更新成功" , $total_price);
		} else {
			$this->error("更新失败");
		}
	}
	 
}