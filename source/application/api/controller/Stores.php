<?php
namespace app\api\controller;


use think\Request;
use think\Db; 
/**
 * 4s服务中心管理
 * Class Store
 * @package app\api\controller
 */
class Stores extends Controller
{
	private $shopinfo = [];
    public function _initialize()
    {
		parent::_initialize();
		//判断用户是否有权限进行访问服务中心
		$this->shopinfo = db("shop")->where(['user_id' => $this->user_info['user_id']])->find();
		if( !$this->shopinfo || !is_array( $this->shopinfo ) ) {
			exit(json_encode(array("code" => 423 , 'msg' => '非常抱歉，您没有访问权限')));
		}
		if( !$this->shopinfo['status'] ) {
			exit(json_encode(array("code" => 423 , 'msg' => '非常抱歉，您的账户已被平台冻结，详情请和平台联系。')));
		}
	}
	
	/**
	 * 工作台
	 */
	public function index() {
		$shop_info = thumbImgUrl([
			'shop_id' => $this->shopinfo['shop_id'],
			'shop_name' => $this->shopinfo['shop_name'],
			'logo' => $this->shopinfo['logo'],
			'type' => $this->shopinfo['type'],
			'address' => $this->shopinfo['province_name'] . '-' . $this->shopinfo['city_name'],
			'tel' => $this->shopinfo['tel'],
			//预约数据统计
			'yuyue_count' => [
				'wait_confirm' => db("shop_yuyue_service")->where(['status' => 0,'shop_id' => $this->shopinfo['shop_id']])->count(),
				'arrangeing' => db("shop_yuyue_service")->where(['status' => 1,'shop_id' => $this->shopinfo['shop_id']])->count(),
				'starting' => db("shop_yuyue_service")->where(['status' => 2,'shop_id' => $this->shopinfo['shop_id']])->count(),
				'complete' => db("shop_yuyue_service")->where(['status' => 3,'shop_id' => $this->shopinfo['shop_id']])->count()
			],
			//查询可结算的数量
			'settlement' =>  db("shop_yuyue_service")->where(['status' => 3,'shop_id' => $this->shopinfo['shop_id'] , 'is_settlement' => 1])->count(),
			//查询需要补油的产品数量
			'supplement' => db("engineoil_shop")->where('shop_id = ' . $this->shopinfo['shop_id'] . ' and stock_num <= 2')->count(),
			//查询数据的统计
			'data_count' => [
				'supplement_sq' => round(db("engineoil_replenishment")->where(['shop_id' => $this->shopinfo['shop_id'] , 'status' => 0])->count(),2),
				'supplement_cg' => round(db("engineoil_replenishment")->where('shop_id = '. $this->shopinfo['shop_id'] . ' and status > 1')->count(),2),
				'supplement_sb' => round(db("engineoil_replenishment")->where(['shop_id' => $this->shopinfo['shop_id'] , 'status' => 1])->count(),2),
				'service_num' => round(db("shop_yuyue_service")->where(['status' => ['in' , '2,3'],'shop_id' => $this->shopinfo['shop_id']])->count(),2),
				'service_huan_num' => round(db("shop_yuyue_service")->where(['status' => ['in' , '2,3'],'type' => 1,'shop_id' => $this->shopinfo['shop_id']])->count(),2),
				'service_bu_num' => round(db("shop_yuyue_service")->where(['status' => ['in' , '2,3'],'type' => 2,'shop_id' => $this->shopinfo['shop_id']])->count(),2),
				'service_bu_capacity' => round(db("shop_yuyue_service")->where(['status' => ['in' , '2,3'],'type' => 2,'shop_id' => $this->shopinfo['shop_id']])->sum('use_capacity'),2),
				'service_huan_capacity' => round(db("shop_yuyue_service")->where(['status' => ['in' , '2,3'],'type' => 1,'shop_id' => $this->shopinfo['shop_id']])->sum('use_capacity'),2),
			]
		]);
		$this->success("success" , $shop_info);
	}
	
	/**
	 * 资料修改
	 */
	public function setting() {
		if( request()->isPost() ) {
			$shop_name = $this->request->param("shop_name/s" , '');
			$shop_name ? $shop_name : $this->error('店面名称不能为空');
			
			$logo = $this->request->param("logo/s" , '');
			$logo ? $logo : $this->error('请上传店面LOGO');
			
			$province = $this->request->param("province/d" , 0);
			$province ? $province : $this->error('请选择省份');
			
			$city = $this->request->param("city/d" , 0);
			$city ? $city : $this->error('请选择城市');
			
			$area = $this->request->param("area/d" , 0);
			$area ? $area : $this->error('请选择市区');
			
			$address = $this->request->param("address/s" , '');
			$address ? $address : $this->error('请输入详细地址');
			
			$contact = $this->request->param("contact/s" , '');
			$contact ? $contact : $this->error('联系人不能为空');
			
			$tel = $this->request->param("tel/s" , '');
			$tel ? $tel : $this->error('联系电话不能为空');
			
			$saletime = $this->request->param("saletime/s" , '');
			$saletime ? $saletime : $this->error('营业时间不能为空');
			
			$info = $this->request->param("info/s" , '');
			$info ? $info : $this->error('店面介绍不能为空');
			
			$business_license = $this->request->param("business_license_path/s" , '');
			$business_license ? $business_license : $this->error('请上传营业执照');
			
			$transport_license = $this->request->param("transport_license_path/s" , '');
			$transport_license ? $transport_license : $this->error('请上传道路运输执照');
			
			$shop_image = $this->request->param("shop_image_path/s" , '');
			$shop_image ? $shop_image : $this->error('请上传店面照片');
			
			$workshop_image = $this->request->param("workshop_image_path/s" , '');
			$workshop_image ? $workshop_image : $this->error('请上传车间照片');
			
			$reception_image = $this->request->param("reception_image_path/s" , '');
			$reception_image ? $reception_image : $this->error('请上传接待室照片');
			
			$office_image = $this->request->param("office_image_path/s" , '');
			$office_image ? $office_image : $this->error('请上传办公室照片');
			
			//修改数据
			$result = db("shop")->where(['shop_id' => $this->shopinfo['shop_id']])->update( [
				'shop_name' => htmlspecialchars($shop_name),
				'logo' => $logo,
				'province' => intval($province),
				'city' => intval($city),
				'area' => intval($area),
				'province_name' => db("region")->where(['id' => intval($province) ] )->value("name"),
				'city_name' => db("region")->where(['id' => intval($city) ])->value("name"),
				'area_name' => db("region")->where(['id' => intval($area) ] )->value("name"),
				'address' => htmlspecialchars($address),
				'contact' => htmlspecialchars($contact),
				'tel' => htmlspecialchars($tel),
				'saletime' => htmlspecialchars($saletime),
				'info' => htmlspecialchars($info),
				'business_license' => json_encode(explode(',',$business_license)),
				'transport_license' => json_encode(explode(',',$transport_license)),
				'shop_image' => json_encode(explode(',',$shop_image)),
				'workshop_image' => json_encode(explode(',',$workshop_image)),
				'reception_image' => json_encode(explode(',',$reception_image)),
				'office_image' => json_encode(explode(',',$office_image))
			] );
			if( $result ) {
				$this->success("资料更新成功");
			} else {
				$this->error("资料更新失败");
			}
		} else {
			$shop_info =[
				'shop_name' => $this->shopinfo['shop_name'],
				'logo' => $this->shopinfo['logo'],
				'province' => $this->shopinfo['province'],
				'city' => $this->shopinfo['city'],
				'area' => $this->shopinfo['area'],
				'address_data' => $this->shopinfo['province_name'] . ' - ' . $this->shopinfo['city_name'] . ' - ' . $this->shopinfo['area_name'],
				'address' => $this->shopinfo['address'],
				'contact' => $this->shopinfo['contact'],
				'tel' => $this->shopinfo['tel'],
				'saletime' => $this->shopinfo['saletime'],
				'info' => $this->shopinfo['info'],
				'business_license' => $this->shopinfo['business_license'] ? json_decode($this->shopinfo['business_license'] , true) : '',
				'transport_license' => $this->shopinfo['transport_license'] ? json_decode($this->shopinfo['transport_license'] , true) : '',
				'shop_image' => $this->shopinfo['shop_image'] ? json_decode($this->shopinfo['shop_image'] , true) : '',
				'workshop_image' => $this->shopinfo['workshop_image'] ? json_decode($this->shopinfo['workshop_image'] , true) : '',
				'reception_image' => $this->shopinfo['reception_image'] ? json_decode($this->shopinfo['reception_image'] , true) : '',
				'office_image' => $this->shopinfo['office_image'] ? json_decode($this->shopinfo['office_image'] , true) : ''
			];
			
			$new_data['business_license_path'] = $shop_info['business_license'];
			$new_data['transport_license_path'] = $shop_info['transport_license'];
			$new_data['shop_image_path'] = $shop_info['shop_image'];
			$new_data['workshop_image_path'] = $shop_info['workshop_image'];
			$new_data['reception_image_path'] = $shop_info['reception_image'];
			$new_data['office_image_path'] = $shop_info['office_image'];
			
			$shop_info = array_merge( thumbImgUrl( $shop_info ) , $new_data );
			$this->success("success" , $shop_info);
		}
	}
	
	/**
	 * 预约中心
	 */
	public function yuyuelist() {
		$type = $this->request->param("type/d" , 0);
		$where = ['ys.shop_id' => $this->shopinfo['shop_id'] , 'shop_delete' => 1];
		if( $type > 0 ) {
			if( $type >= 5 ) {
				$where['ys.status'] = ['IN' , '4,5,6'];
			} else {
				$where['ys.status'] = $type - 1;
			}
		}
		$list = thumbImgUrl( db('shop_yuyue_service')->alias("ys")->join("yoshop_user u",'u.user_id = ys.user_id')->where($where)->field("ys.id,ys.cart_brand,ys.type,ys.contact,ys.mobile,ys.yuyue_time,ys.specific_time,ys.status,u.nickname,u.avatar")
				->order("ys.id desc")->paginate(10, false, [ 'query' => Request::instance()->request() ])->each(function($item , $key){
					$fields = [ 0 => '待确认' , 1 => '预约中' , 2 => '服务中' , 3 => '已完成' , 4 => '待确认取消' , 5 => '已确认取消' , 6 => '已拒绝'];
					$item['status_name'] = $fields[$item['status']];
					return $item;	
				})->toArray()['data'] );
		$this->success("success" , $list);
	}
	
	/**
	 * 预约拒绝
	 */
	public function yuyuerefuse() {
		$id = $this->request->param("id/d" , 0);
		if( $id <= 0 ) $this->error("数据异常");
		$refuse = $this->request->param("refuse/s" , '');
		if( !$refuse ) $this->error("请输入拒绝原因");
		$info = db('shop_yuyue_service')->where(['id' => $id ,'shop_delete' => 1, 'shop_id' => $this->shopinfo['shop_id']])->find();
		if( !$info ) $this->error("数据不存在");
		$result = db("shop_yuyue_service")->where(['id' => $id])->update(['status' => 6 , 'refuse_note' => $refuse]);
		if( $result ) {
			//预约次数回增
			//db("user")->where(['user_id' => $info['user_id']])->setInc('service_num',1);
			$member_info = db("user")->where(['user_id' => $info['user_id']])->find();
			if( $info['type'] == 1 && $member_info) {
				db("user")->where(['user_id' => $info['user_id']])->update(['service_num' => $member_info['service_num'] + 1]);
			}
			$this->getui->sendToClient($info['user_id'] , '您有新的预约消息反馈' , '您预约【'.$info['yuyue_time'].' ' . $info['specific_time'] . '】的服务商家已拒绝，详情请在个人中心查看。');
			$this->success("预约拒绝成功");
		} else {
			$this->error("操作失败");
		}
	}
	
	/**
	 * 预约删除
	 */
	public function yuyuedelete() {
		$id = $this->request->param("id/d" , 0);
		if( $id <= 0 ) $this->error("数据异常");
		$info = db('shop_yuyue_service')->where(['id' => $id ,'shop_delete' => 1, 'shop_id' => $this->shopinfo['shop_id']])->find();
		if( !$info ) $this->error("数据不存在");
		$result = db("shop_yuyue_service")->where(['id' => $id])->update(['shop_delete' => 0]);
		if( $result ) {
			$this->success("预约删除成功");
		} else {
			$this->error("操作失败");
		}
	}
	
	/**
	 * 预约更新资料
	 */
	public function yuyuesave() {
		$id = $this->request->param("id/d" , 0);
		if( $id <= 0 ) $this->error("数据异常");
		$yuyue_time = $this->request->param("yuyue_time/s" , '');
		if( !$yuyue_time ) $this->error("请选择预约时间");
		$specific_time = $this->request->param("specific_time/s" , '');
		if( !$specific_time ) $this->error("请选择时间区间");
		$info = db('shop_yuyue_service')->where(['id' => $id ,'shop_delete' => 1, 'shop_id' => $this->shopinfo['shop_id']])->find();
		if( !$info ) $this->error("数据不存在");
		$result = db("shop_yuyue_service")->where(['id' => $id])->update(['yuyue_time' => $yuyue_time , 'specific_time' => $specific_time]);
		if( $result ) {
			$this->getui->sendToClient($info['user_id'] , '您的预约信息有更新' , '您原来预约的【'.$info['yuyue_time'].' '.$info['specific_time'].'】服务商户更改为【'.$yuyue_time.' '.$specific_time.'】，请在个人中心查看' );
			$this->success("信息更新成功");
		} else {
			$this->error("操作失败");
		}
	}
	
	/**
	 * 确定预约
	 */
	public function yuyueok() {
		$id = $this->request->param("id/d" , 0);
		if( $id <= 0 ) $this->error("数据异常");
		$info = db('shop_yuyue_service')->where(['id' => $id ,'shop_delete' => 1, 'shop_id' => $this->shopinfo['shop_id']])->find();
		if( !$info ) $this->error("数据不存在");
		$result = db("shop_yuyue_service")->where(['id' => $id])->update(['status' => 1 , 'shop_confirm_time' => time()]);
		if( $result ) {
			$this->getui->sendToClient($info['user_id'] , '您预约的服务有新进展了' , '您预约的【'.$info['yuyue_time'].' '.$info['specific_time'].'】服务，商家已确认，详情请在个人中心查看' );
			$this->success("预约确定成功");
		} else {
			$this->error("操作失败");
		}
	}
	
	/**
	 * 确认取消
	 */
	public function yuyueokcancel() {
		$id = $this->request->param("id/d" , 0);
		if( $id <= 0 ) $this->error("数据异常");
		$info = db('shop_yuyue_service')->where(['id' => $id ,'shop_delete' => 1, 'shop_id' => $this->shopinfo['shop_id']])->find();
		if( !$info ) $this->error("数据不存在");
		$result = db("shop_yuyue_service")->where(['id' => $id])->update(['status' => 5]);
		if( $result ) {
			//预约次数回增
			$member_info = db("user")->where(['user_id' => $info['user_id']])->find();
			if( $info['type'] == 1 && $member_info) {
				db("user")->where(['user_id' => $info['user_id']])->update(['service_num' => $member_info['service_num'] + 1]);
			}
			$this->getui->sendToClient($info['user_id'],'您取消预约的服务有新进展了' , '您【'.$info['yuyue_time'].' '.$info['specific_time'].'】取消申请服务，商家已同意，详情请在个人中心查看' );
			$this->success("确认取消成功");
		} else {
			$this->error("操作失败");
		}
	}
	
	/**
	 * 预约拒绝取消
	 */
	public function yuyuecanceljujue() {
		$id = $this->request->param("id/d" , 0);
		if( $id <= 0 ) $this->error("数据异常");
		$info = db('shop_yuyue_service')->where(['id' => $id ,'shop_delete' => 1, 'shop_id' => $this->shopinfo['shop_id']])->find();
		if( !$info ) $this->error("数据不存在");
		$result = db("shop_yuyue_service")->where(['id' => $id])->update(['status' => 1]);
		if( $result ) {
			//预约次数回增
			$this->getui->sendToClient($info['user_id'],'您取消预约的服务有新进展了' , '您【'.$info['yuyue_time'].' '.$info['specific_time'].'】取消申请服务，商家拒绝了取消，详情请在个人中心查看' );
			//db("user")->where(['user_id' => $info['user_id']])->setInc('service_num',1);
			$this->success("拒绝取消成功");
		} else {
			$this->error("操作失败");
		}
	}
	
	/**
	 * 二维码核销
	 */
	public function qrcodewriteoff() {
		$id = $this->request->param("id/d" , 0);
		if( $id <= 0 ) $this->error("数据异常");
		$where = ['ys.id' => $id , 'ys.shop_id' => $this->shopinfo['shop_id'] , 'ys.status' => 1,'ys.user_delete' => 1,'ys.shop_delete' => 1];
		$info = thumbImgUrl( db('shop_yuyue_service')->alias("ys")->join("yoshop_user u",'u.user_id = ys.user_id')->where($where)->field("ys.id,u.user_id,ys.cart_brand,ys.type,ys.contact,ys.mobile,ys.yuyue_time,ys.specific_time,ys.status,u.nickname,u.avatar,u.baoyang_num")->find());
		if( !$info ) $this->error("数据不存在");
		if( request()->isPost() ) {
			/*$stock = $this->request->param("stock/d" , 0);
			if( $stock <= 0 ) {
				$this->error("请输入使用的库存");
			}*/
			$stock = $this->request->param("capacity" , 0);
			if( $stock <= 0 ) $this->error("请输入此次使用的容量（升）");
			$engineoil_shop_id = $this->request->param("engineoil_shop_id/d" , 0);
			if( !$engineoil_info = db("engineoil_shop")->where(['id' => $engineoil_shop_id , 'shop_id' => $this->shopinfo['shop_id']])->find() ) {
				$this->error("机油产品信息错误");
			}
			//$info['type'] == 1 ? $stock = $engineoil_info['capacity'] : '';
			if( $engineoil_info['stock_num'] < $stock ) $this->error("当前库存容量【".$engineoil_info['stock_num']."】升,不够支持此次服务。");
			$result = db("shop_yuyue_service")->where(['id' => $id])->update([
				'engineoil_shop_id' => $engineoil_shop_id,
				'engineoil_id' => $engineoil_info['engineoil_id'],
				'use_capacity' => $stock,
				'status' => 2,
				'shop_qrcode_time' => time()
			]);
			if( $result ) {
				//写入日志
				if( $stock > 0 ) {
					db("engineoil_shop_log")->insert([
						'shop_id' => $this->shopinfo['shop_id'],
						'engineoil_id' => $engineoil_info['engineoil_id'],
						'type' => 2,
						'use_type' => $info['type'],
						'num' => $stock,
						'content' => '用户【'.$info['nickname'].'】' . ($info['type'] == 1 ? '换油' : '补油') . '使用'. $stock . '升，品牌：' .$info['cart_brand'],
						'create_time' => time()
					]);
					//扣除库存
					db("engineoil_shop")->where(['id' => $engineoil_shop_id])->update(['stock_num' => $engineoil_info['stock_num'] - $stock]);
				}
				//写入用户换油记录次数
				if( $info['type'] == 1 ) {
					db("user")->where(['user_id' => $info['user_id']])->update(['baoyang_num' => $info['baoyang_num'] + 1]);
				}
				$this->getui->sendToClient($info['user_id'],'您预约的服务现有新进展' , '您【'.$info['yuyue_time'].' '.$info['specific_time'].'】的服务商家已核销并且服务开始，详情请在个人中心查看' );
				$this->success("确认成功，服务已开始");
			} else {
				$this->error("操作失败，请重试");
			}
		} else {
			//获得产品信息
			$info['goods'] = db("engineoil_shop")->alias("es")->join("yoshop_engineoil e",'e.id = es.engineoil_id')->field("es.id,es.stock_num,e.title")
							 ->where(['es.shop_id' => $this->shopinfo['shop_id']])->select()->toArray();
			$this->success( 'success' ,  $info );
		}
	}
	
	/**
	 * 预约详情
	 */
	public function yuyueinfo() {
		$id = $this->request->param("id/d" , 0);
		if( $id <= 0 ) $this->error("数据错误");
		$info = thumbImgUrl(db("shop_yuyue_service")->alias("ys")->join("yoshop_user u",'u.user_id = ys.user_id')->where(['ys.shop_id' => $this->shopinfo['shop_id'] ,'ys.shop_delete' => 1 , 'ys.id' => $id])
				->field("ys.*,u.nickname,u.avatar")->find());
		if( !$info ) $this->error("数据不存在！");
		$info['province_name'] = $this->shopinfo['province_name'];
		$info['city_name'] = $this->shopinfo['city_name'];
		$info['area_name'] = $this->shopinfo['area_name'];
		$info['address'] = $this->shopinfo['address'];
		$info['tel'] = $this->shopinfo['tel'];
		$info['create_time'] = date("Y-m-d H:i" , $info['create_time']);
		$info['address'] = $info['province_name'].$info['city_name'].$info['area_name'].$info['address'];
		//获得预约车辆的数据
		$auto_info = db("member_auto_info")->where(['id' => $info['auto_info_id']])->field("plate_num,type as cart_type,frame_number")->find();
		$info = array_merge( $info , $auto_info );
		unset( $info['province_name'], $info['city_name'], $info['area_name'], $info['user_id'], $info['shop_id'],$info['auto_info_id'] );
		$fields = [ 0 => '待确认' , 1 => '预约中' , 2 => '服务中' , 3 => '已完成' , 4 => '待确认取消' , 5 => '已确认取消' , 6 => '已拒绝'];
		$info['status_name'] = $fields[$info['status']];
		return $this->renderSuccess(compact('info'));
	}
	
	/**
	 * 4s店库存管理
	 */
	public function engineoil() {
		$type = $this->request->param("type/d" , 0);
		$where = 'es.shop_id = ' . $this->shopinfo['shop_id'];
		if( $type == 1 ) {
			$where .= ' and es.stock_num <= 40 and es.stock_num >= 1';
		} else if( $type == 2 ) {
			$where .= ' and es.stock_num <= 0 and e.status = 0';
		} else {
			$where .= ' and es.stock_num > 40';
		}
		$list = thumbImgUrl( db('engineoil_shop')->alias("es")->join("yoshop_engineoil e" , 'e.id = es.engineoil_id')->where($where)->field("es.stock_num,es.id,e.title,e.thumb,e.brand,e.model_number,e.status")
				->paginate(10, false, [ 'query' => Request::instance()->request() ])->each(function($item , $key){
					$item['is_supplement'] = 0;
					if( $item['stock_num'] <= 40 && $item['status'] ) {
						$item['is_supplement'] = 1;
					}
					$item['type'] = $item['stock_num'] <= 0 && !$item['status'] ? 1 : 0;
					return $item;
				})->toArray()['data'] );
		return $this->renderSuccess(compact('list'));
	}
	
	/**
	 * 删除换油商品
	 */
	public function delengineoil() {
		$id = $this->request->param("id/d" , 0);
		if( $id <= 0 ) $this->error("数据异常");
		$info = db('engineoil_shop')->where(['id' => $id , 'shop_id' => $this->shopinfo['shop_id']])->find();
		if( !$info ) $this->error("数据不存在");
		$result = db("engineoil_shop")->where(['id' => $id])->delete();
		if( $result ) {
			$this->success("删除成功");
		} else {
			$this->error("操作失败");
		}
	}
	
	/**
	 * 库存申请
	 */
	public function supplement() {
		$id = $this->request->param("id/d" , 0);
		if( $id <= 0 ) $this->error("信息错误");
		$where = 'es.shop_id = ' . $this->shopinfo['shop_id'] . ' and es.id = ' . intval( $id );
		$info = thumbImgUrl( db('engineoil_shop')->alias("es")->join("yoshop_engineoil e" , 'e.id = es.engineoil_id')->where($where)->field("es.stock_num,es.id,e.title,e.thumb,e.brand,e.model_number,e.capacity,e.status,es.engineoil_id")->find() );
		if( !$info ) $this->error("信息不存在");
		if( !$info['status'] ) $this->error("您申请的机油产品平台已下架，详情请咨询平台。");
		unset( $info['status'] );
		if( request()->isPost() ) {
			$stock = $this->request->param("stock/d" , 0);
			if( $stock <= 0 ) $this->error("请输入需要补充的库存数量");
			$content = $this->request->param("content/s" , '');
			if( $content == '' ) $this->error('请输入这次补油的说明');
			$result = db("engineoil_replenishment")->insert([
				'eid' => $info['engineoil_id'],
				'sn' => date("YmdHis") . rand(100,999),
				'content' => $content,
				'stock' => $stock,
				'shop_id' => $this->shopinfo['shop_id'],
				'create_time' => time()
			]);
			if( $result ) {
				$this->success("申请成功");
			} else {
				$this->error("申请失败");
			}
		} else {
			//统计
			$info['supplement_stock'] = db("engineoil_replenishment")->where('status >= 2')->sum('stock');
			$info['supplement_capacity'] = $info['capacity'] * $info['supplement_stock'];
			return $this->renderSuccess(compact('info'));
		}
	}
	
	/**
	 * 出入库日志
	 */
	public function stocklog() {
		$where = ['es.shop_id' => $this->shopinfo['shop_id']];
		$list = thumbImgUrl( db('engineoil_shop_log')->alias("es")->join("yoshop_engineoil e" , 'e.id = es.engineoil_id')->where($where)->field("es.*,e.thumb,e.title,e.brand,e.model_number")
				->order("es.id desc")->paginate(10, false, [ 'query' => Request::instance()->request() ])->each(function($item , $key){
					$item['create_time'] = date("Y-m-d H:i:s" , $item['create_time']);
					unset( $item['shop_id'] , $item['engineoil_id'] );
					return $item;
				})->toArray()['data']);
		return $this->renderSuccess(compact('list'));
	}
	
	/**
	 * 补油申请列表
	 */
	public function supplementapply() {
		$type = $this->request->param("type/d" , 0);
		$where = ['er.shop_id' => $this->shopinfo['shop_id']];
		if( $type == 1 ) {
			$where['er.status'] = ['IN' , '2,3,4'];
		} else if( $type == 2 ) {
			$where['er.status'] = 1;
		} else {
			$where['er.status'] = 0;
		}
		$list = thumbImgUrl( db('engineoil_replenishment')->alias("er")->join("yoshop_engineoil e" , 'e.id = er.eid')->where($where)->field("er.*,e.title,e.brand,e.thumb,e.model_number")
				->paginate(10, false, [ 'query' => Request::instance()->request() ])->each(function($item , $key){
					$item['create_time'] = date("Y-m-d H:i:s" , $item['create_time']);
					unset( $item['shop_id'] , $item['eid'] );
					return $item;
				})->toArray()['data'] );
		return $this->renderSuccess(compact('list'));
	}
	
	/**
	 * 确认申请收货
	 */
	public function supplementget() {
		$id = $this->request->param("id/d" , 0);
		if( $id <= 0 ) $this->error("信息错误");
		$info = db('engineoil_replenishment')->where(['shop_id' => $this->shopinfo['shop_id'] , 'id' => $id])->find();
		if( !$info ) {
			$this->error("信息不存在");
		}
		if( $info['status'] != 3 ) {
			$this->error("不能收货");
		}
		$result = db('engineoil_replenishment')->where(['shop_id' => $this->shopinfo['shop_id'] , 'id' => $id])->update(['status' => 4]);
		if( $result ) {
			$data = db("engineoil_shop")->where(['engineoil_id' => $info['eid'] , 'shop_id' => $this->shopinfo['shop_id']])->find();
			if( $data ) {
				$engineoil =  db("engineoil")->where(['id' => $info['eid']])->find();
				//写入4s店库存
				db("engineoil_shop")->where(['engineoil_id' => $info['eid'] , 'shop_id' => $this->shopinfo['shop_id']])->update(['stock_num' => $data['stock_num'] + $info['stock'] * $engineoil['capacity'] , 'last_instock_time' => time(), 'total_stock_num' => $data['total_stock_num'] + $info['stock'] * $engineoil['capacity']]);
				//写入日志
				db("engineoil_shop_log")->insert([
					'shop_id' => $this->shopinfo['shop_id'],
					'engineoil_id' => $info['eid'],
					'type' => 1,
					'num' => $info['stock'] * $engineoil['capacity'],
					'content' => '编号【' . $info['sn'] . '】补油到货，入库：' . $info['stock'] * $engineoil['capacity'] .'升',
					'create_time' => time()
				]);
			}
			$this->success("确认收货成功");
		} else {
			$this->error("操作失败");
		}
	}
	
	/**
	 * 查看申请详情
	 */
	public function supplementinfo() {
		$id = $this->request->param("id/d" , 0);
		if( $id <= 0 ) $this->error("信息错误");
		$info = thumbImgUrl( db('engineoil_replenishment')->alias("er")->join("yoshop_engineoil e" , 'e.id = er.eid')->where(['er.shop_id' => $this->shopinfo['shop_id'] , 'er.id' => $id])
				->field("er.*,e.title,e.brand,e.thumb,e.model_number")->find() );
		if( !$info ) {
			$this->error("信息不存在");
		}
		try{
			$info['express_name'] =config("express_name_list")[ $info['express_name']];
			$info['express_list'] = [];
			if( $info['is_deliver'] == 1 ) {
				//获得快递信息
				/*$curl = curl_init();
				curl_setopt($curl, CURLOPT_URL, "http://www.kuaidi100.com/query?type=".$info['express_name']."&postid=" . $info['express_no']);
				curl_setopt($curl, CURLOPT_HEADER, 0);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
				curl_setopt($curl,CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)");
				$result = curl_exec($curl);
				curl_close($curl);
				$result = json_decode( $result , true);*/
				$result = getExpress($info['express_no']);
				if( $result && $result['code'] == 'OK' ) {
					$express_list = [];
					if( $result['list'] ) {
						foreach( $result['list'] as $k1 => $v1 ) {
							$express_list[] = ['context' => $v1['content'] , 'time' => $v1['time']];
						}
					}
					$info['express_list'] = $express_list;
				}
			}
			$this->success("success" , $info);
		} catch (Exception $e) {
			$this->error("快递数据获取异常，请联系平台");
		}
	}
	
	/**
	 * 删除申请信息
	 */
	public function supplementdel(){
		$id = $this->request->param("id/d" , 0);
		if( $id <= 0 ) $this->error("信息错误");
		$info = db('engineoil_replenishment')->where(['shop_id' => $this->shopinfo['shop_id'] , 'id' => $id])->find();
		if( !$info ) {
			$this->error("信息不存在");
		}
		if( !in_array($info['status'] , array(1,4) ) ) $this->error("信息不能删除");
		$result = db('engineoil_replenishment')->where(['shop_id' => $this->shopinfo['shop_id'] , 'id' => $id])->delete();
		if( $result ) {
			$this->success("数据删除成功");
		} else {
			$this->error("操作失败");
		}
	}
	
	/**
	 * 工时申请结算
	 */
	public function settlement() {
		$type = $this->request->param("type/d" , 0);
		$where = ['ys.shop_id' => $this->shopinfo['shop_id'] , 'ys.status' => 3 , 'ys.is_settlement' => 1];
		if( $type == 1 ) {
			$where['settlement_status'] = 1;
		} else if( $type == 2 ) {
			$where['settlement_status'] = 3;
		} else if( $type == 3 ) {
			$where['settlement_status'] = 2;
		} else {
			$where['settlement_status'] = 0;
		}
		$list = thumbImgUrl( db('shop_yuyue_service')->alias("ys")->join("yoshop_user u",'u.user_id = ys.user_id')->where($where)->field("ys.id,ys.cart_brand,ys.user_complete_time,ys.type,ys.contact,ys.mobile,ys.yuyue_time,ys.specific_time,ys.settlement_status,u.nickname,u.avatar")
				->paginate(10, false, [ 'query' => Request::instance()->request() ])->each(function($item , $key){
					$fields = [0 => '待结算' , 1 => '已申请' , 2 => '驳回申请' , 3 => '同意申请'];
					$item['status_name'] = $fields[$item['settlement_status']];
					$item['user_complete_time'] = date("Y-m-d H:i:s" , $item['user_complete_time']);
					return $item;	
				})->toArray()['data'] );
		$this->success("success" , $list);
	}
	
	/**
	 * 申请结算
	 */
	public function settlementapply() {
		$id = $this->request->param("id/d" , 0);
		if( $id <= 0 ) $this->error("信息错误");
		$info = db('shop_yuyue_service')->where(['shop_id' => $this->shopinfo['shop_id'] , 'status' => 3 , 'is_settlement' => 1 , 'settlement_status' => ['IN' , '0,2'] , 'id' => $id])->find();
		if( !$info ) $this->error("信息不存在");
		$result = db('shop_yuyue_service')->where(['id' => $id])->update(['settlement_status' => 1]);
		if( $result ) {
			$this->success("结算申请成功");
		} else{
			$this->error("操作失败");
		}
	}
	
	
	
	/**
	 * 批量提交核算
	 */
	public function allsettlementapply() {
		$total = db("shop_yuyue_service")->where(['is_settlement' => 1 , 'settlement_status' => ['in' , '0,2'],'shop_id' => $this->shopinfo['shop_id']])->count();
		if( $total <= 0 ) {
			$this->error("您暂无需要核算的服务");
		}
		$result = db("shop_yuyue_service")->where(['is_settlement' => 1 , 'settlement_status' => ['in' , '0,2'],'shop_id' => $this->shopinfo['shop_id']])->update(['settlement_status' => 1]);
		if( $result ) {
			$this->success("申请成功，共申请【".$total."】笔服务");
		} else {
			$this->error("核算申请失败");
		}
	}
}