<?php
namespace app\api\controller;

use app\common\exception\BaseException;
use think\Controller as ThinkController;
use app\common\model\GetTui as GetTuiModel;
use think\Request;

define('REQUEST_MODULE', 'API');
/**
 * API控制器基类
 * Class BaseController
 * @package app\store\controller
 */
class Controller extends ThinkController
{
    const JSON_SUCCESS_STATUS = 1;
    const JSON_ERROR_STATUS = 0;
    
	protected $getui = null;
    protected $user_info = [];
	protected $sf_field = [0 => '普通会员',10 => '业务员',20 => '销售经理',30 => '总经理'];
    
    //验证的模块
    private $_rule_module = [
        'Cart' => '*',
        'Order' => '*',
		'User' => '*',
		'Stores' => '*'
    ];
    /**
     * 基类初始化
     * @throws BaseException
     */
    public function _initialize()
    {
		parent::_initialize();
        $request = Request::instance();
		$is_checked_login = false;
        //判断是否是需要认
		$token = $this->request->param("token") ? $this->request->param("token") : ( isset( $_SERVER['HTTP_TOKEN'] ) ? $_SERVER['HTTP_TOKEN'] : '' );
        if( ( isset( $this->_rule_module[$request->controller()] ) &&  $this->_rule_module[$request->controller()] == '*' ) || ( isset( $this->_rule_module[$request->controller()] ) && strpos($this->_rule_module[$request->controller()] , ','. $request->action()  .',') ) ){
            $is_checked_login = true;
			if( !$token ) {
                exit(json_encode($this->renderJson( 413 , '登录信息已过期，请重新登录', []) , JSON_UNESCAPED_UNICODE));
            }
        }
		
		if( $token ) {
			$this->user_info = thumbImgUrl( db("user")->where(['status' => 2 , 'login_token' => $token])->find() );
            if( !$this->user_info && $is_checked_login ) {
                exit(json_encode($this->renderJson( 413 , '登录信息已过期，请重新登录', []) , JSON_UNESCAPED_UNICODE));
            }
			if( $this->user_info['status'] != 2  && $is_checked_login ) {
				db("user")->where(['user_id' => $this->user_info['user_id']])->update(['login_token' => '']);
				exit(json_encode($this->renderJson( 413 , '您已被平台禁止登录', []) , JSON_UNESCAPED_UNICODE));
			}
			if( $this->user_info ) {
				$this->user_info['identity_name'] = $this->sf_field[$this->user_info['identity']];
				$this->user_info = thumbImgUrl( $this->user_info );
			}
		}
		$this->getui = new GetTuiModel;
    }

    /**
     * 返回封装后的 API 数据到客户端
     * @param int $code
     * @param string $msg
     * @param array $data
     * @return array
     */
    protected function renderJson($code = self::JSON_SUCCESS_STATUS, $msg = '', $data = [])
    {
        return compact('code', 'msg', 'url', 'data');
    }

    /**
     * 返回操作成功json
     * @param string $msg
     * @param array $data
     * @return array
     */
    protected function renderSuccess($data = [], $msg = 'success')
    {
        return $this->renderJson(self::JSON_SUCCESS_STATUS, $msg, $data);
    }

    /**
     * 返回操作失败json
     * @param string $msg
     * @param array $data
     * @return array
     */
    protected function renderError($msg = 'error', $data = [])
    {
        return $this->renderJson(self::JSON_ERROR_STATUS, $msg, $data);
    }

    /**
     * 获取post数据 (数组)
     * @param $key
     * @return mixed
     */
    protected function postData($key)
    {
        return $this->request->post($key . '/a');
    }
	
    public function _empty(){
        return $this->renderJson( 404 , '访问的方法不存在', []);
    }

}
