<?php
/**
 * 接口集成API
 * @author wyb
 */
namespace app\common\extend;
class Baseapi {
    
    protected $_error = '出现未知错误';   //错误信息提示
    
    public function getError() {
        return $this->_error;
    }
}